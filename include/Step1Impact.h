// Dear emacs, this is -*- c++ -*-
// $Id: CycleCreators.py 344 2012-12-13 13:10:53Z krasznaa $
#ifndef Step1Impact_H
#define Step1Impact_H

// SFrame include(s):
#include "core/include/SCycleBase.h"

/**
 *   @short Put short description of class here
 *
 *          Put a longer description over here...
 *
 *  @author Put your name here
 * @version $Revision: 344 $
 */
class Step1Impact : public SCycleBase {

public:
   /// Default constructor
   Step1Impact();
   /// Default destructor
   ~Step1Impact();

   /// Function called at the beginning of the cycle
   virtual void BeginCycle() throw( SError );
   /// Function called at the end of the cycle
   virtual void EndCycle() throw( SError );

   /// Function called at the beginning of a new input data
   virtual void BeginInputData( const SInputData& ) throw( SError );
   /// Function called after finishing to process an input data
   virtual void EndInputData  ( const SInputData& ) throw( SError );

   /// Function called after opening each new input file
   virtual void BeginInputFile( const SInputData& ) throw( SError );

   /// Function called for every event
   virtual void ExecuteEvent( const SInputData&, Double_t ) throw( SError );

private:
   Double_t fRadiansToDegrees;
   //
   // Put all your private variables here
   //
   std::string     TreeName;

   Int_t           NtelInShower;
   Double_t        MCTrueEnergy;
   Double_t        Energy;
   Double_t        CameraXEvent;
   Double_t        CameraYEvent;
   Double_t        MCTrueNominalX;
   Double_t        MCTrueNominalY;
   Double_t        MCThetaSqr;

   Double_t        CoreXGround;
   Double_t        CoreYGround;
   Double_t        MCTrueCoreGroundX;
   Double_t        MCTrueCoreGroundY;

   Double_t        MSCW;
   Double_t        MSCL;

   Int_t           out_NtelInShower;
   Double_t        out_TrueEnergy;
   Double_t        out_RecoEnergy;
   Double_t        out_RecoCameraX;
   Double_t        out_RecoCameraY;
   Double_t        out_TrueCameraX;
   Double_t        out_TrueCameraY;
   Double_t        out_ThetaSqDegreesSq;
   Double_t         out_ThetaDegrees;
   Double_t        out_RecoShowerCoreX;
   Double_t        out_RecoShowerCoreY;
   Double_t        out_TrueShowerCoreX;
   Double_t        out_TrueShowerCoreY;

   Double_t        out_MSCW;
   Double_t        out_MSCL;

   // Macro adding the functions for dictionary generation
   ClassDef( Step1Impact, 0 );

}; // class Step1Impact

#endif // Step1Impact_H

