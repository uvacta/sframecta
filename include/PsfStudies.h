// Dear emacs, this is -*- c++ -*-
// $Id: CycleCreators.py 344 2012-12-13 13:10:53Z krasznaa $
#ifndef PsfStudies_H
#define PsfStudies_H

#include "TH1.h"
#include "TH2.h"
#include "TH3.h"

// SFrame include(s):
#include "core/include/SCycleBase.h"

/**
 *   @short Psf Studies
 *
 *          Put a longer description over here...
 *
 *  @author Cosette Molijn, 6 Dec. 2016
 * @version $Revision: 344 $
 */
class PsfStudies : public SCycleBase {

public:
   /// Default constructor
   PsfStudies();
   /// Default destructor
   ~PsfStudies();

   /// Function called at the beginning of the cycle
   virtual void BeginCycle() throw( SError );
   /// Function called at the end of the cycle
   virtual void EndCycle() throw( SError );

   /// Function called at the beginning of a new input data
   virtual void BeginInputData( const SInputData& ) throw( SError );
   /// Function called after finishing to process an input data
   virtual void EndInputData  ( const SInputData& ) throw( SError );

   /// Function called after opening each new input file
   virtual void BeginInputFile( const SInputData& ) throw( SError );

   /// Function called for every event
   virtual void ExecuteEvent( const SInputData&, Double_t ) throw( SError );

private:
   //
   // Put all your private variables here
   //

     std::string TreeName;
   TTree* tree;
   TFile* outputFile;
   Double_t TrueEnergy;
   Double_t RecoEnergy;
   Double_t RecoCameraX;
   Double_t RecoCameraY;
   Double_t TrueCameraX;
   Double_t TrueCameraY;
   Double_t ThetaSqDegreesSq;
   Double_t ThetaDegrees;
   Int_t    NtelInShower;
   Double_t TrueShowerCoreX;  
   Double_t TrueShowerCoreY;
   Double_t RecoShowerCoreX;
   Double_t RecoShowerCoreY;

   Double_t ARsq;
   Double_t trueEn;
   Double_t trueImp;
   Int_t    Ntelshower;
   Double_t theta;
  

   TH1F* hTheta;
   TH1F* hEnergyResolution;
   TH1F* hAngularResolution;
   TH1F* hPSFContainment;
   TH1F* hNtelInShower;
   TH2F* hARvsNtel;
  // TH2F* hARvsEn;
   TH2F* hARvslogEn;
   TH3F* hARlogEnNtel;
   TH3F* hARNtelImp;

   TH1F* hlogEn;
   TH1F* hTrueMCEnergy;
   TH1F* hTrueShowerCore;
   TH1F* hShowerCoreResolution;
   TH2F* hlogEnvsEnres;
   TH2F* hImpactvsAR;


   // Macro adding the functions for dictionary generation
   ClassDef( PsfStudies, 0 );

}; // class PsfStudies

#endif // PsfStudies_H

