// Dear emacs, this is -*- c++ -*-
// $Id: CycleCreators.py 344 2012-12-13 13:10:53Z krasznaa $
#ifndef ExampleEventLoop_H
#define ExampleEventLoop_H

#include "TH1.h"
#include "TH2.h"
#include "TH3.h"

// SFrame include(s):
#include "core/include/SCycleBase.h"

/**
 *   @short Put short description of class here
 *
 *          Put a longer description over here...
 *
 *  @author Put your name here
 * @version $Revision: 344 $
 */
class ExampleEventLoop : public SCycleBase {

public:
   /// Default constructor
   ExampleEventLoop();
   /// Default destructor
   ~ExampleEventLoop();

   /// Function called at the beginning of the cycle
   virtual void BeginCycle() throw( SError );
   /// Function called at the end of the cycle
   virtual void EndCycle() throw( SError );

   /// Function called at the beginning of a new input data
   virtual void BeginInputData( const SInputData& ) throw( SError );
   /// Function called after finishing to process an input data
   virtual void EndInputData  ( const SInputData& ) throw( SError );

   /// Function called after opening each new input file
   virtual void BeginInputFile( const SInputData& ) throw( SError );

   /// Function called for every event
   virtual void ExecuteEvent( const SInputData&, Double_t ) throw( SError );

private:
   //
   // Put all your private variables here
   //
   std::string TreeName;
   Double_t TrueEnergy;
   Double_t RecoEnergy;
   Double_t RecoCameraX;
   Double_t RecoCameraY;
   Double_t TrueCameraX;
   Double_t TrueCameraY;
   Double_t ThetaSqDegreesSq;
   Int_t    NtelInShower;

   TH1F* hEnergyResolution;
   TH1F* hAngularResolution;
   TH1F* hPSFContainment;
   TH1F* hNtelInShower;
   TH2F* hARvsNtel;
   TH2F* hNtelvsEn;
   TH1F* hTrueMCEnergy;



   // Macro adding the functions for dictionary generation
   ClassDef( ExampleEventLoop, 0 );

}; // class ExampleEventLoop

#endif // ExampleEventLoop_H

