// $Id: CycleCreators.py 344 2012-12-13 13:10:53Z krasznaa $
#include "TMath.h"
#include "TArrayF.h"
// Local include(s):
#include <iostream>
#include "../include/ExampleEventLoop.h"

ClassImp( ExampleEventLoop );

ExampleEventLoop::ExampleEventLoop()
   : SCycleBase() {

   SetLogName( GetName() );
   DeclareProperty( "TreeString", TreeName );
}

ExampleEventLoop::~ExampleEventLoop() {

}

void ExampleEventLoop::BeginCycle() throw( SError ) {

   return;

}

void ExampleEventLoop::EndCycle() throw( SError ) {

   return;

}

void ExampleEventLoop::BeginInputData( const SInputData& ) throw( SError ) {
  // before loop


   hEnergyResolution  = Book( TH1F("EnergyResolution",";#Delta E ( rec-true ) / true;Events",100,-2.0,5) );
   hAngularResolution = Book(TH1F("AngularResolution","",100,0.,0.05));
   hNtelInShower      = Book(TH1F("NtelInShower","",55,0.5,55.5));
   hTrueMCEnergy        = Book(TH1F("TrueMCEnergy","",100,0.,100));

   // book 2D hist
   hARvsNtel          = Book(TH2F("ARvsNtel",";#Theta^2;# telescopes",100,0,0.05, 55,0,55));
   //  hNtelvsEn          = Book(TH2F("NtelvsEn",";#Delta E;# telescopes",100,-2.0,5.0,55,0,55));

   std::cout<< "Here we make the histograms " << std::endl ;

   return;

}

void ExampleEventLoop::EndInputData( const SInputData& ) throw( SError ) {
  // after loop

   Int_t nbins = Hist("AngularResolution")->GetNbinsX();

   hPSFContainment = Book(TH1F("PSFContainment","; #Theta^2; Containment ",100,0.,0.03));
   Double_t total = Hist("AngularResolution")->Integral(1,nbins+1);
   Double_t soFar = 0.;
   for(Int_t i = 1; i<= nbins + 1;++i) {
   
      soFar += Hist("AngularResolution")->GetBinContent(i);
      Hist("PSFContainment")->SetBinContent(i,soFar/total);
   }

   std::cout<< "Here we make the PSFContainment hist" << std::endl ;


   return;
}

void ExampleEventLoop::BeginInputFile( const SInputData& ) throw( SError ) {

   ConnectVariable( TreeName.c_str(), "RecoEnergy", RecoEnergy );
   ConnectVariable( TreeName.c_str(), "TrueEnergy", TrueEnergy );
   ConnectVariable( TreeName.c_str(), "RecoCameraX", RecoCameraX );
   ConnectVariable( TreeName.c_str(), "RecoCameraY", RecoCameraY );
   ConnectVariable( TreeName.c_str(), "TrueCameraX", TrueCameraX );
   ConnectVariable( TreeName.c_str(), "TrueCameraY", TrueCameraY );
   ConnectVariable( TreeName.c_str(), "ThetaSqDegreesSq", ThetaSqDegreesSq );
   ConnectVariable( TreeName.c_str(), "NtelInShower", NtelInShower );

   std::cout<< "Here we read in the variables:" << std::endl ;

   return;
}



void ExampleEventLoop::ExecuteEvent( const SInputData&, Double_t ) throw( SError ) {


  //Calulate stuff: in this case the energy resolution
   Double_t delta = (RecoEnergy - TrueEnergy) / TrueEnergy;
   //  std::cout<< "delta: " << delta << std::endl ;


   //And fill the histograms
   hEnergyResolution->Fill(delta);

   hAngularResolution->Fill(ThetaSqDegreesSq);

   hNtelInShower->Fill(NtelInShower);

   hTrueMCEnergy->Fill(TrueEnergy);

   hARvsNtel->Fill(ThetaSqDegreesSq,NtelInShower);


   return;
}

