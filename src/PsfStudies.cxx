// $Id: CycleCreators.py 344 2012-12-13 13:10:53Z krasznaa $

// Local include(s):
#include "../include/PsfStudies.h"
#include "TFile.h"
#include "TMath.h"
#include "TArrayF.h"
#include <iostream>

ClassImp( PsfStudies );

PsfStudies::PsfStudies()
   : SCycleBase() {

   SetLogName( GetName() );
   DeclareProperty( "TreeString", TreeName );
}


PsfStudies::~PsfStudies() {

}

void PsfStudies::BeginCycle() throw( SError ) {

   return;

}

void PsfStudies::EndCycle() throw( SError ) {

 

   return;

}

void PsfStudies::BeginInputData( const SInputData& ) throw( SError ) {
 // before loop
  // ==========
  // Here we make a new tree, for the plot scripts
  // ==========
  //you need to open the outputfile here, keep the file pointer as a member of your class/struct
  outputFile = Book(TFile("Test.root", "RECREATE"));
  //create the tree in the directory of the file, keep the tree pointer as a member of your class/struct
  tree =  Book(TTree("tree", "tree"));
  //add the branches, be careful: you need Pointers to the variables! 
  //Your code compiled because of implicit casts in C/C++
  //e.g. C/C++ converted your variable to a pointer, e.g. 0 to a NULL pointer
  tree->Branch("ThetaSq", &ARsq);
  tree->Branch("TrueEnergy", &trueEn);
  tree->Branch("TrueImpact", &trueImp);
  tree->Branch("Ntelshower", & Ntelshower);
  tree->Branch("Theta", &theta);
  //leave the file directory
  gROOT->cd();


  // ========== 
  // Make the histograms
  // ==========
  // 1D hists
  // impctpar = impact parameter
 
  hAngularResolution = Book(TH1F("AngularResolution","",100,0.,1.0));
  hNtelInShower      = Book(TH1F("NtelInShower","",55,0.5,55.5));
  hTrueMCEnergy      = Book(TH1F("TrueMCEnergy","",100,0.,100));
  hTrueShowerCore    = Book(TH1F("TrueShowerCore","",1500,0.,1500));
  hShowerCoreResolution =  Book(TH1F("ShowerCoreResolution","",100,-1.5,2.5));
  hTheta             = Book(TH1F("Theta","",1000,0.,0.5));
  
  // book 2D hist
  // book energy vs AR histogram with logarithmic energy (xaxis)  binning

  // first make an array for the energy bins
  Double_t En_bins[45];
  Int_t   el1 = 0;
  Int_t   plist[5] = {-2,-1,0,1,2};

  for(Int_t i=0 ; i<= 4; i++){
    Int_t j = plist[i];
    
    for(Int_t k = 1; k<=9; k++ ){
      Double_t num = k * pow(10,j);
      En_bins[el1] = num;
 
      el1++;
    }
  }

  // Since TH3 does not like it when some parameters have an array as bin range, and some don't (variable bin width or not),
  // we also need arrays for the bin range of Ntel and AR:

  // AR [0,0.5]
  Double_t AR_bins[1001];
  Int_t el2 = 0;
  Double_t AR = 0;
  for(Int_t i = 0; i <= 1000; i++){
    AR_bins[el2] = AR;
    
    el2++;
    AR += 0.0005;
  }
 
  // Ntel [0,55]
  Double_t Ntel_bins[56];
  Int_t el3 = 0;
  Int_t Ntel = 0;
  for(Int_t i = 0; i <= 55; i++){
    Ntel_bins[el3] = Ntel;
    
    el3++;
    Ntel++;
  }
  
  // And book the histograms
  hlogEn              = Book( TH1F("logEn",";Energy;#events",44,En_bins));
 
  hARvslogEn          = Book( TH2F("ARvslogEn",";#Theta^2; Energy",1000,0.,0.5, 44, En_bins ));
  hlogEnvsEnres       = Book( TH2F("hlogEnvsEnres",";Energy (Tev); Resolution",44, En_bins, 100, -2., 5. )) ;
  hARlogEnNtel        = Book( TH3F("ARlogEnNtel",";#Theta^2; Ntel; Energy",1000, AR_bins, 55,  Ntel_bins,  44,  En_bins  ));
  hImpactvsAR        = Book(TH2F("hImpactvsAR",";Max. Impact parameter (m); #Theta^2",1500,0.,1500, 1000,0., 0.5 ));
  hARvsNtel          = Book(TH2F("ARvsNtel",";#Theta^2; Ntel",1000, 0., 0.5, 55, 0, 55));

  hARNtelImp         = Book( TH3F( "ARNtelImp", ";#Theta^2; Ntel; Max. impact par", 1000, 0., 0.5, 55, 0, 55, 1500 ,0. , 1500.  )  ); 

   std::cout<< "Here we make the histograms " << std::endl ;

   return;

}

void PsfStudies::EndInputData( const SInputData& ) throw( SError ) {
 // after loop

   Int_t nbins = Hist("AngularResolution")->GetNbinsX();

   hPSFContainment = Book(TH1F("PSFContainment","; #Theta^2; Containment ",100,0.,0.05));
   Double_t total = Hist("AngularResolution")->Integral(1,nbins+1);
   Double_t soFar = 0.;
   for(Int_t i = 1; i<= nbins + 1;++i) {
   
      soFar += Hist("AngularResolution")->GetBinContent(i);
      Hist("PSFContainment")->SetBinContent(i,soFar/total);
   }

   //the next line forces the TTree to flush its buffer to the file
   outputFile->Write();
   outputFile->Close();

   std::cout<< "End of the loop" << std::endl ;

   return;

}

void PsfStudies::BeginInputFile( const SInputData& ) throw( SError ) {
   ConnectVariable( TreeName.c_str(), "RecoEnergy", RecoEnergy );
   ConnectVariable( TreeName.c_str(), "TrueEnergy", TrueEnergy );
   ConnectVariable( TreeName.c_str(), "RecoCameraX", RecoCameraX );
   ConnectVariable( TreeName.c_str(), "RecoCameraY", RecoCameraY );
   ConnectVariable( TreeName.c_str(), "TrueCameraX", TrueCameraX );
   ConnectVariable( TreeName.c_str(), "TrueCameraY", TrueCameraY );
   ConnectVariable( TreeName.c_str(), "ThetaSqDegreesSq", ThetaSqDegreesSq );
   ConnectVariable( TreeName.c_str(), "ThetaDegrees", ThetaDegrees );
   ConnectVariable( TreeName.c_str(), "NtelInShower", NtelInShower );
   ConnectVariable( TreeName.c_str(), "TrueShowerCoreX", TrueShowerCoreX );
   ConnectVariable( TreeName.c_str(), "TrueShowerCoreY", TrueShowerCoreY );
   ConnectVariable( TreeName.c_str(), "RecoShowerCoreX", RecoShowerCoreX );
   ConnectVariable( TreeName.c_str(), "RecoShowerCoreY", RecoShowerCoreY );


   std::cout<< "Here we read in the variables:" << std::endl ;

   return;

}

void PsfStudies::ExecuteEvent( const SInputData&, Double_t ) throw( SError ) {
  //Calulate stuff: 
  // Energy resolution
   Double_t deltaE  = (RecoEnergy - TrueEnergy) / TrueEnergy;

   // Impact parameter 
   Double_t TrueImpact =  pow ( pow(TrueShowerCoreX,2) + pow(TrueShowerCoreY,2), 0.5);
   Double_t RecoImpact =  pow ( pow(RecoShowerCoreX,2) + pow(RecoShowerCoreY,2), 0.5);
   Double_t deltaSC = (RecoImpact - TrueImpact) / TrueImpact; 

   // Fill the branches
   ARsq   = ThetaSqDegreesSq;
   trueEn = TrueEnergy;
   trueImp= TrueImpact;
   Ntelshower   = NtelInShower;
   theta = ThetaDegrees;
   tree->Fill();

   //And fill the histograms


   hTheta->Fill(ThetaDegrees);

   hlogEnvsEnres->Fill(TrueEnergy, deltaE);

   hAngularResolution->Fill(ThetaSqDegreesSq);

   hNtelInShower->Fill(NtelInShower);

   hTrueMCEnergy->Fill(TrueEnergy);

   hlogEn->Fill(TrueEnergy);

   hARvslogEn->Fill(ThetaSqDegreesSq,TrueEnergy);

   hARlogEnNtel->Fill(ThetaSqDegreesSq,NtelInShower,TrueEnergy);

   hARvsNtel->Fill(ThetaSqDegreesSq, NtelInShower);
   
   hTrueShowerCore->Fill(TrueImpact);

   hShowerCoreResolution->Fill(deltaSC);

   hImpactvsAR->Fill(TrueImpact, ThetaSqDegreesSq);

   hARNtelImp->Fill(ThetaSqDegreesSq, NtelInShower, TrueImpact);
			     

   return;

}

