// $Id: CycleCreators.py 344 2012-12-13 13:10:53Z krasznaa $
#include "TMath.h"
// Local include(s):
#include "../include/Step1EventDisplay.h"

ClassImp( Step1EventDisplay );

Step1EventDisplay::Step1EventDisplay()
   : SCycleBase() {

   SetLogName( GetName() );
   DeclareProperty( "TreeString", TreeName );
   fRadiansToDegrees = 180./TMath::Pi();
}

Step1EventDisplay::~Step1EventDisplay() {

}

void Step1EventDisplay::BeginCycle() throw( SError ) {

   return;

}

void Step1EventDisplay::EndCycle() throw( SError ) {

   return;

}

void Step1EventDisplay::BeginInputData( const SInputData& ) throw( SError ) {

   DeclareVariable(out_NtelWData,"NtelWData");
   DeclareVariable(out_NtelInShower,"NtelInShower");
   DeclareVariable(out_TrueEnergy,"TrueEnergy");
   DeclareVariable(out_RecoEnergy,"RecoEnergy");

   DeclareVariable(out_RecoCameraX,"RecoCameraX");
   DeclareVariable(out_RecoCameraY,"RecoCameraY");
   DeclareVariable(out_TrueCameraX,"TrueCameraX");
   DeclareVariable(out_TrueCameraY,"TrueCameraY");
   DeclareVariable(out_ThetaSqDegreesSq,"ThetaSqDegreesSq");
   DeclareVariable(out_ThetaDegrees, "ThetaDegrees");

   DeclareVariable(out_RecoShowerCoreX,"RecoShowerCoreX");
   DeclareVariable(out_RecoShowerCoreY,"RecoShowerCoreY");
   DeclareVariable(out_TrueShowerCoreX,"TrueShowerCoreX");
   DeclareVariable(out_TrueShowerCoreY,"TrueShowerCoreY");

   DeclareVariable(out_MSCW,"MSCW");
   DeclareVariable(out_MSCL,"MSCL");
   return;

}

void Step1EventDisplay::EndInputData( const SInputData& ) throw( SError ) {

   return;

}

void Step1EventDisplay::BeginInputFile( const SInputData& ) throw( SError ) {


   ConnectVariable( TreeName.c_str(), "ErecS", Energy );
   ConnectVariable( TreeName.c_str(), "MCe0", MCTrueEnergy );
   ConnectVariable( TreeName.c_str(), "MCxoff", MCTrueNominalX );
   ConnectVariable( TreeName.c_str(), "MCyoff", MCTrueNominalY );
   ConnectVariable( TreeName.c_str(), "Xoff", CameraXEvent );
   ConnectVariable( TreeName.c_str(), "Yoff", CameraYEvent );
   ConnectVariable( TreeName.c_str(), "MCxcore", MCTrueCoreGroundX );
   ConnectVariable( TreeName.c_str(), "MCycore", MCTrueCoreGroundY );
   ConnectVariable( TreeName.c_str(), "Xcore", CoreXGround );
   ConnectVariable( TreeName.c_str(), "Ycore", CoreYGround );
   ConnectVariable( TreeName.c_str(), "NImages", NtelInShower );
   ConnectVariable( TreeName.c_str(), "MSCW", MSCW );
   ConnectVariable( TreeName.c_str(), "MSCL", MSCL );

   return;

}

void Step1EventDisplay::ExecuteEvent( const SInputData&, Double_t ) throw( SError ) {

   // copy data
   out_NtelInShower = NtelInShower;
   out_TrueEnergy = MCTrueEnergy;
   out_RecoEnergy = Energy;
   out_RecoCameraX = CameraXEvent;
   out_RecoCameraY = CameraYEvent;
   out_TrueCameraX = MCTrueNominalX;
   out_TrueCameraY = MCTrueNominalY;
   out_MSCW = MSCW;
   out_MSCL = MSCL;
   out_RecoShowerCoreX = CoreXGround;
   out_RecoShowerCoreY = CoreYGround;
   out_TrueShowerCoreX = MCTrueCoreGroundX;
   out_TrueShowerCoreY = MCTrueCoreGroundY;

   out_ThetaSqDegreesSq =   pow(out_RecoCameraX - out_TrueCameraX ,2.) + pow(out_RecoCameraY - out_TrueCameraY,2.) ;

   return;

}

