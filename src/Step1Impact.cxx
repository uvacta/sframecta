// $Id: CycleCreators.py 344 2012-12-13 13:10:53Z krasznaa $
#include "TMath.h"
// Local include(s):
#include "../include/Step1Impact.h"

ClassImp( Step1Impact );

Step1Impact::Step1Impact()
   : SCycleBase() {

   SetLogName( GetName() );
   DeclareProperty( "TreeString", TreeName );
   fRadiansToDegrees = 180./TMath::Pi();
}

Step1Impact::~Step1Impact() {

}

void Step1Impact::BeginCycle() throw( SError ) {

   return;

}

void Step1Impact::EndCycle() throw( SError ) {

   return;

}

void Step1Impact::BeginInputData( const SInputData& ) throw( SError ) {

   DeclareVariable(out_NtelInShower,"NtelInShower");
   DeclareVariable(out_TrueEnergy,"TrueEnergy");
   DeclareVariable(out_RecoEnergy,"RecoEnergy");

   DeclareVariable(out_RecoCameraX,"RecoCameraX");
   DeclareVariable(out_RecoCameraY,"RecoCameraY");
   DeclareVariable(out_TrueCameraX,"TrueCameraX");
   DeclareVariable(out_TrueCameraY,"TrueCameraY");
   DeclareVariable(out_ThetaSqDegreesSq,"ThetaSqDegreesSq");
   DeclareVariable(out_ThetaDegrees, "ThetaDegrees");

   DeclareVariable(out_RecoShowerCoreX,"RecoShowerCoreX");
   DeclareVariable(out_RecoShowerCoreY,"RecoShowerCoreY");
   DeclareVariable(out_TrueShowerCoreX,"TrueShowerCoreX");
   DeclareVariable(out_TrueShowerCoreY,"TrueShowerCoreY");

   DeclareVariable(out_MSCW,"MSCW");
   DeclareVariable(out_MSCL,"MSCL");
   return;

}

void Step1Impact::EndInputData( const SInputData& ) throw( SError ) {

   return;

}

void Step1Impact::BeginInputFile( const SInputData& ) throw( SError ) {

   ConnectVariable( TreeName.c_str(), "NtelInShower", NtelInShower );
   ConnectVariable( TreeName.c_str(), "MCTrueEnergy", MCTrueEnergy );
   ConnectVariable( TreeName.c_str(), "CorrEnergy", Energy );
   ConnectVariable( TreeName.c_str(), "MCTrueNominalX", MCTrueNominalX );
   ConnectVariable( TreeName.c_str(), "MCTrueNominalY", MCTrueNominalY );
   ConnectVariable( TreeName.c_str(), "CameraXEvent", CameraXEvent );
   ConnectVariable( TreeName.c_str(), "CameraYEvent", CameraYEvent );
   ConnectVariable( TreeName.c_str(), "MCThetaSqr", MCThetaSqr );

   ConnectVariable( TreeName.c_str(), "CoreXGround", CoreXGround );
   ConnectVariable( TreeName.c_str(), "CoreYGround", CoreYGround );
   ConnectVariable( TreeName.c_str(), "MCTrueCoreGroundX", MCTrueCoreGroundX );
   ConnectVariable( TreeName.c_str(), "MCTrueCoreGroundY", MCTrueCoreGroundY );

   ConnectVariable( TreeName.c_str(), "MSCW", MSCW  );
   ConnectVariable( TreeName.c_str(), "MSCL", MSCL  );
   return;

}

void Step1Impact::ExecuteEvent( const SInputData&, Double_t ) throw( SError ) {

   // copy data
   out_NtelInShower = NtelInShower;
   out_TrueEnergy = MCTrueEnergy;
   out_RecoEnergy = Energy;
   // convert from radians to degrees
   out_RecoCameraX = CameraXEvent * fRadiansToDegrees;
   out_RecoCameraY = CameraYEvent * fRadiansToDegrees;
   out_TrueCameraX = MCTrueNominalX * fRadiansToDegrees;
   out_TrueCameraY = MCTrueNominalY * fRadiansToDegrees;
   out_MSCW = MSCW;
   out_MSCL = MSCL;
   out_ThetaSqDegreesSq = MCThetaSqr;
   out_RecoShowerCoreX = CoreXGround;
   out_RecoShowerCoreY = CoreYGround;
   out_TrueShowerCoreX = MCTrueCoreGroundX;
   out_TrueShowerCoreY = MCTrueCoreGroundY;

   return;

}

