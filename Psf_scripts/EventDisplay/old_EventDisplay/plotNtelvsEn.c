//==========
//STL (standard C library)
#include <iostream>
//#include <ofstream>
//=============
//ROOT
#include <TLatex.h>
#include <TFile.h>
#include <TH1F.h>
#include <TH1D.h>
//graphic output:
#include <TCanvas.h>
//nice legend:
#include <TLegend.h>
//colors and stuff:
#include <TStyle.h>
#include "TMath.h"
#include "TGraph.h"
#include "TGraphPainter.h"
#include <sstream>
//
//This script visualises the relation of the minimum number of telescopes in a shower versus the loss in percentage per energy bin.
//
// --------------
// Author: Felicia Krauss - 17 October 2016
// --------------
// Modification history:
// 19 Oct 2016: Cosette Molijn (cmolijn): Add other histograms/containment radius
// 01 Nov 2016: cmolijn                 : Replace GetEntries() with Integral()
// 16 Nov 2016: cmolijn                 : Extend script with 2d histogram, add Ncuts and graphs

void TH1F2ascii(TH1F* hist,std::string filename, TString folder, TString separator);

//==============

// Don't define as cstring here, unclear why
//define default value for variable
//several variable separated by ","
void plotNtelvsEn(std::string FName="../../output/PsfStudies.MC.EventDisplay.prod3.root")
{
  gROOT->SetStyle("Plain");

  //Define fonts:
  gStyle->SetStatFontSize(0.04); 
  gStyle->SetLabelFont(22,"X");
  gStyle->SetStatFont(22);
  gStyle->SetTitleFont(22,"xyz");
  gStyle->SetTitleFont(22,"h");
  gStyle->SetLegendFont(2);
    
  // * means pointer
  // convert string file name to c string, unclear why not before
  // Alternative: WRITE, RECREATE (=overwrite)

  // -----------------------
  // Read output MC File and define the histogram
  // -----------------------
  TFile* MCfile = new TFile (FName.c_str(),"READ");
    if(!MCfile)
      {
	std::cerr << "File not found: "<< FName << "Check if it exists." << std::endl;
      }
  // Define new histogram that points to histogram in file
  TH3F* H_3par = (TH3F*)MCfile->Get("ARlogEnNtel");
  H_3par->GetYaxis()->Print("all");


  // -----------------------
  // Here we will project the 3D histogram onto 2D histograms, by slicing along the X and Y axis
  // This way we get a 2D histogram for each energy bin. 
  // -----------------------
  // First bin of the histogram is the underflow bin, so start with b = 1
  // Get total number of x and y bins (E cuts) and check this with bin centers
  Int_t n_binx   = H_3par->GetXaxis()->GetNbins(); // AR bins
  Int_t n_biny   = H_3par->GetYaxis()->GetNbins(); // Ntel bins
  Int_t n_binz   = H_3par->GetZaxis()->GetNbins(); // Energy bins

  std::cout << "Number of X (#Theta^2) bins   = "<< n_binx  << std::endl;
  std::cout << "Number of Y (Ntel, # ) bins   = "<< n_biny  << std::endl;
  std::cout << "Number of Z (Energy, TeV) bins= "<< n_binz  << std::endl;

  //Get total Number of Events:
  Int_t tot_entr=H_3par->GetEntries(); // You want to count the overflow bin of course
  std::cout << "Total number of entries in simulation:" << tot_entr << std::endl;

  //Start the loop here, slicing to 2D histograms

  Double_t L_ev_loss[n_binz];
  Double_t L_En_bins[n_binz];
  Double_t L_En_entr[n_binz];
  Int_t el1 = 0;

  // TProfile* H_Ncut = new TProfile("H_Ncut","Ncut", n_binx, 0., 0.5);


  for (Int_t b=1 ; b<=n_binz ; ++b ){

    // Project the 3d histogram onto a 2D
    H_3par->GetZaxis()->SetRange(b,b); // Select the energy bin
    TProfile2D* H_Enbin = H_3par->Project3DProfile("yx");
  
    // Get the center of the energy bin
    Double_t En_center = H_3par->GetZaxis()->GetBinCenter(b);
    //  std::cout << "Ebin center = "  << En_center << std::endl;

    // Count entries in this energy bin
    Double_t Enbin_entr=H_Enbin->GetEntries();
    // std::cout << "Enbin entries = " << Enbin_entr << std::endl;

    // Set the Ntel cut (start the loop here later for more Ncuts)
    Int_t Ncut = 10;
    
    // std::cout << ". # x (AR)bins = "  << H_Enbin->GetXaxis()->GetNbins() << std::endl;
    // std::cout << ". # y (Ntel)bins = "  << H_Enbin->GetYaxis()->GetNbins() << std::endl;


    // Project the 2d histogram onto a 1D
    // TProfile* H_Ncut = H_Enbin->ProfileX("",Ncut , n_biny);
    TProfile* H_Ncut = H_Enbin->ProfileX(Form("h%d",b),Ncut , n_biny);

    //   std::cout << "2. # x (AR)bins = "  << H_Ncut->GetXaxis()->GetNbins() << std::endl;
    //  H_Ncut->Print();

    // Calculate event loss
    Double_t   n_entr  = H_Ncut->GetEntries();
    //  std::cout << "entries after cut = " << n_entr << std::endl;
    
    if(n_entr > 0){
      Double_t   ev_loss = (1.-n_entr/Enbin_entr )*100.; // percentage
      L_ev_loss[el1] = ev_loss;
      //  std::cout << "For element " << el1 << "; event loss % = " << ev_loss  << std::endl;

    } else{
      Double_t ev_loss = 0.0; // Might need some other value.. N/A
      L_ev_loss[el1] = ev_loss;
      // std::cout << "For element " << el1 << "; event loss % = " << ev_loss  << std::endl;
    }
    
    L_En_bins[el1] = En_center;
    L_En_entr[el1] = n_entr;

    el1++;

    H_Ncut->Reset();

  }

  const Int_t n_el = el1;

   // PLOT FroM HERE
  TCanvas* canv = new TCanvas("c1","Event loss after Ncut per energy bin");
  canv->SetFillColor(0);
  // canv->Divide(2,1);
  
  // Plot Energy bin vs event loss
  //  canv->cd(1);
  gPad->SetLogx();

  TGraph* gr_loss = new TGraph(n_el,  L_En_bins,  L_ev_loss);
  gr_loss-> SetTitle("Energy vs event loss for Ncut = 10 ; Energy (TeV); loss (%)");
  gr_loss->SetMarkerStyle(20);
  gr_loss->SetMarkerColor(2);
  gr_loss->SetMarkerSize(1.);
  gr_loss->Draw("AP");
  gr_loss->GetYaxis()->SetTitleOffset(1.3);
  gr_loss->GetXaxis()->SetTitleOffset(1.3);

  // Plot Energy bin vs total entries
  // canv->cd(2);
  gPad->SetLogx();
  gPad->SetLogy();

  TGraph* gr_ev_tot = new TGraph(n_el,  L_En_bins,  L_En_entr);
  gr_ev_tot-> SetTitle("Energy vs # events for Ncut = 3 ; Energy (TeV); # events");
  gr_ev_tot->SetMarkerStyle(20);
  gr_ev_tot->SetMarkerColor(4);
  gr_ev_tot->SetMarkerSize(1.);
  // gr_ev_tot->Draw("AP");
  gr_ev_tot->GetYaxis()->SetTitleOffset(1.3);
  gr_ev_tot->GetXaxis()->SetTitleOffset(1.3);

  canv->SaveAs("plots/EventDisplay_Ncut3_En_bin500.pdf"); // Form("h%d",b)


   
  // ===============
}

