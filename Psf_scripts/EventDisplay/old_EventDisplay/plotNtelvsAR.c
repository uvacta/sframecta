//==========
//STL (standard C library)
#include <iostream>
//#include <ofstream>
//=============
//ROOT
#include <TLatex.h>
#include <TFile.h>
#include <TH1F.h>
#include <TH1D.h>
//graphic output:
#include <TCanvas.h>
//nice legend:
#include <TLegend.h>
//colors and stuff:
#include <TStyle.h>
#include "TMath.h"
#include "TGraph.h"
#include "TGraphPainter.h"
#include <sstream>
//
//This script visualises the relation of the number of telescopes in a shower 
// versus the angular resolution.
//
// --------------
// Author: Felicia Krauss - 17 October 2016
// --------------
// Modification history:
// 19 Oct 2016: Cosette Molijn (cmolijn): Add other histograms/containment radius
// 01 Nov 2016: cmolijn                 : Replace GetEntries() with Integral()
// 16 Nov 2016: cmolijn                 : Extend script with 2d histogram, add Ncuts and graphs

void TH1F2ascii(TH1F* hist,std::string filename, TString folder, TString separator);

//==============

// Don't define as cstring here, unclear why
//define default value for variable
//several variable separated by ","
void plotNtelvsAR(std::string FName="../../output/PsfStudies.MC.EventDisplay.prod3.root")
{
  gROOT->SetStyle("Plain");

  //Define fonts:
  gStyle->SetStatFontSize(0.04); 
  gStyle->SetLabelFont(22,"X");
  gStyle->SetStatFont(22);
  gStyle->SetTitleFont(22,"xyz");
  gStyle->SetTitleFont(22,"h");
  gStyle->SetLegendFont(2);
    
  // * means pointer
  // convert string file name to c string, unclear why not before
  // Alternative: WRITE, RECREATE (=overwrite)

  // -----------------------
  // Read output MC File and define the histogram
  // -----------------------
  TFile* MCfile = new TFile (FName.c_str(),"READ");
    if(!MCfile)
      {
	std::cerr << "File not found: "<< FName << "Check if it exists." << std::endl;
      }
  // Define new histogram that points to histogram in file
  TH2F* H_ARvsNtel = (TH2F*)MCfile->Get("ARvsNtel");
  H_ARvsNtel->GetYaxis()->Print("all");


  // -----------------------
  // Here we will project the 2D histogram onto 1D histograms, by making cuts on the 
  // number of telescopes
  // -----------------------
  // First bin of the histogram is the underflow bin, so start with b = 1
  // Get total number of x and y bins (Ntel cuts) and check this with bin centers
  int n_biny   = H_ARvsNtel->GetYaxis()->GetNbins();
  int n_binx   = H_ARvsNtel->GetXaxis()->GetNbins();
  float Ntel_tot = H_ARvsNtel->GetYaxis()->GetBinCenter(n_biny+1);

  std::cout << "NbinsY total and total num telescopes = "<< n_biny <<"; " << Ntel_tot << std::endl;
  std::cout << "Number of X (#Theta^2) bins:" << n_binx << std::endl;

  //Get total Number of Events:
  int tot_entr=H_ARvsNtel->Integral(1,n_binx + 1); // You want to count the overflow bin of course
  std::cout << "Total number of entries in simulation:" << tot_entr << std::endl;


  //============================
  // Define your variable parameters here
  //============================

  int Ntel_max = 20 ; // Define your max Ntel_cut: atm it is 52, since there are no events with 
                      // with more than 52 telescopes
                      // The minimum number of telescopes of an event is 3, so the lists with radii
                      // and so on will have Ntel_max - 2 elements

  //============================
  //============================

  //Define the lists with the radii and Ncut 
   
  float L_r68[Ntel_max -2];
  float L_r80[Ntel_max -2];
  float L_r90[Ntel_max -2];
  float L_Ncut[Ntel_max -2];
  float L_entries[Ntel_max -2];
  float L_ev_loss[Ntel_max - 2];
  
  // 'el' will define the nth element for the lists to fill later in the loop
   int el = 0; 

  for (int b=3; b<=Ntel_max ; ++b ){
    // This for-loop is for making the Ntel cuts. For each cut we calculate the r68 and r80
    // from the projected 1D histogram
  
     // Project the 2d histogram onto a 1D
     TH1D* H_Ncut = H_ARvsNtel->ProjectionX("",b , Ntel_max);
    
     // The first bin in the histogram is the underflow. Since there is no underflow
     // here with the AR,
     // we must start with j = 1 instead of j=0.     

     // Calculate the number of entries and the total entry loss
     float   n_entr  = H_Ncut->Integral(1,n_binx + 1);
     float ev_loss = (1-n_entr/tot_entr )*100; // percentage


     // The first bin in the histogram is the underflow. Since there is no underflow
     // here with the AR, we must start with j = 1 instead of j=0.     
     // Set the limits:
     double limit68 = 0.68*n_entr;
     double i68=0;
     int j68=1;

     double limit80 = 0.80*n_entr;
     double i80=0;
     int j80=1;

    double limit90 = 0.90*n_entr;
     double i90=0;
     int j90=1;

     // And count entries up and until the set limits
     while (i68 <= limit68)
       {
         i68+=H_Ncut->GetBinContent(j68);
         j68++;
       }

     while (i80 <= limit80)
       {
         i80+=H_Ncut->GetBinContent(j80);
         j80++;
       }

     while (i90 <= limit90)
       {
         i90+=H_Ncut->GetBinContent(j90);
         j90++;
       }


     //Get the r68 value of the histogram
     float r68 = H_Ncut->GetBinCenter(j68);
     float r80 = H_Ncut->GetBinCenter(j80);
     float r90 = H_Ncut->GetBinCenter(j90);

     // Let's print some info:
     std::cout << "Ncut (via bin center) = "<< H_ARvsNtel->GetYaxis()->GetBinCenter(b) << std::endl;
     std::cout << "Ncut = " << b << std::endl;
     std::cout << "Number of entries = " << n_entr << " and event loss = "<< ev_loss << "%" << std::endl;
     std::cout << "r68, r80 & r90 = "<< r68 << " ; "<< r80 << " ; " << r90 << std::endl;

     // Finally add all the variables to the corresponding arrays
     L_r68[el]     = r68;
     L_r80[el]     = r80; 
     L_r90[el]     = r90;
     L_Ncut[el]    = b; 
     L_entries[el] = n_entr;
     L_ev_loss[el] = ev_loss;

     ++el; // And +1 the nth element

   }

 const int n_el = el;

 // -----------------------
 // PLOT FroM HERE
 TCanvas* canv = new TCanvas("c1","Angular Resolution vs Number of telescopes in shower,bins");
  canv->SetFillColor(0);
  canv->Divide(2,2);
  
  // Plot Ntelcut vs r68
  canv->cd(1);
  TGraph* gr_r68 = new TGraph(n_el,  L_Ncut,  L_r68);
  gr_r68-> SetTitle("AR r68 (deg^2) vs minimum number of tel per event; Ncut; r68 (deg^2)");
  gr_r68->SetMarkerStyle(20);
  gr_r68->SetMarkerColor(2);
  gr_r68->SetMarkerSize(1.);
  gr_r68->Draw("AP");
  gr_r68->GetYaxis()->SetTitleOffset(1.3);

  // Plot Ntelcut vs r80
  canv->cd(2);
  TGraph* gr_r80 = new TGraph(n_el,  L_Ncut,  L_r80);
  gr_r80-> SetTitle("AR r80 (deg^2) vs minimum number of tel per event; Ncut; r80 (deg^2)");
  gr_r80->SetMarkerStyle(20);
  gr_r80->SetMarkerColor(2);
  gr_r80->SetMarkerSize(1.);
  gr_r80->Draw("AP");
  gr_r80->GetYaxis()->SetTitleOffset(1.3);

  // Plot Ntelcut vs r90
  canv->cd(3);
  TGraph* gr_r90 = new TGraph(n_el,  L_Ncut,  L_r90);
  gr_r90-> SetTitle("AR r90 (deg^2) vs minimum number of tel per event; Ncut; r90 (deg^2)");
  gr_r90->SetMarkerStyle(20);
  gr_r90->SetMarkerColor(2);
  gr_r90->SetMarkerSize(1.);
  gr_r90->Draw("AP");
  gr_r90->GetYaxis()->SetTitleOffset(1.3);

  // Plot Ntelcut vs number of remaining events
  canv->cd(4);
  TGraph* gr_entries = new TGraph(n_el,  L_Ncut,  L_entries);
  gr_entries->SetTitle("Ncut vs number of entries/events; Ncut ; # entries");
  gr_entries->SetMarkerStyle(20);
  gr_entries->SetMarkerColor(2);
  gr_entries->SetMarkerSize(1.);
  gr_entries->Draw("AP");
  gr_entries->GetYaxis()->SetTitleOffset(1.3);

  //Make new canvas, one is not enough
  TCanvas* canv2 = new TCanvas("c2","Angular Resolution vs Number of telescopes in shower");
  canv2->SetFillColor(0);
  // canv2->Divide(2,2);

  // canv2->cd(1);
  TGraph* gr_loss = new TGraph(n_el,  L_Ncut,  L_ev_loss);
  gr_loss->SetTitle("Ncut vs event loss; Ncut ; %");
  gr_loss->SetMarkerStyle(20);
  gr_loss->SetMarkerColor(2);
  gr_loss->SetMarkerSize(1.);
  gr_loss->Draw("AP");
  gr_loss->GetYaxis()->SetTitleOffset(0.8);


  //  TGraph2ascii(L_Ncut,L_ev_loss,"plots/gr_bin500_loss","./","\t");

  canv->SaveAs("plots/EventDisplay_Ntel_AR_bin500.pdf"); // Form("h%d",b)
  canv2->SaveAs("plots/EventDisplay_Ntel_AR_bin500_loss.pdf");
    
}


