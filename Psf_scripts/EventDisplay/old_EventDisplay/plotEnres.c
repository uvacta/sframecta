//==========
//STL (standard C library)
#include <iostream>
//#include <ofstream>
//=============
//ROOT
#include <TLatex.h>
#include <TFile.h>
#include <TH1F.h>
//graphic output:
#include <TCanvas.h>
//nice legend:
#include <TLegend.h>
//colors and stuff:
#include <TStyle.h>
#include "TMath.h"


// --------------
// Author: Felicia Krauss - 17 October 2016
// --------------
// Modification history:
// 19 Oct 2016: Cosette Molijn (cmolijn): Add other histograms/containment radius
// 01 Nov 2016: cmolijn                 : Replace GetEntries() with Integral()


void TH1F2ascii(TH1F* hist,std::string filename, TString folder, TString separator);

//==============

// Don't define as cstring here, unclear why
//define default value for variable
//several variable separated by ","
void plotEnres(std::string FName="../../output/PsfStudies.MC.EventDisplay.prod3.root")
{
  gROOT->SetStyle("Plain");

  //Define fonts:
  gStyle->SetStatFontSize(0.04); 
  gStyle->SetLabelFont(22,"X");
  gStyle->SetStatFont(22);
  gStyle->SetTitleFont(22,"xyz");
  gStyle->SetTitleFont(22,"h");
  gStyle->SetLegendFont(2);
  gStyle->SetOptStat(111111);
    
  // * means pointer
  // convert string file name to c string, unclear why not before
  // Alternative: WRITE, RECREATE (=overwrite)

  // Read output MC File
  TFile* MCfile = new TFile (FName.c_str(),"READ");
    if(!MCfile)
      {
	std::cerr << "File not found: "<< FName << "Check if it exists." << std::endl;
      }
  // Define new histogram that points to histogram in file
  TH1F* H_en_reso = (TH1F*)MCfile->Get("EnergyResolution");

  H_en_reso->Print();

  // Get Number of Bins on x axis:
  int numx=H_en_reso->GetXaxis()->GetNbins();
  std::cout << "Number of x bins:" << numx << std::endl;

  //Get total Number of Events:
  int num=H_en_reso->Integral(1,101);
  std::cout << "Number of entries:" << num << std::endl;
   

  //===============
  // Plotting 
  //===============
  TCanvas* canv = new TCanvas("Energy Resolution");
  canv->SetFillColor(0);
  canv->SetLogy();

  // Draw the histogram
  H_en_reso->Draw("HIST");

  //Finish setting up histogram

   H_en_reso->SetTitle("EnergyResolution; #Delta E; Number of events ");
   H_en_reso->SetTitleSize(0.05,"XY");
   H_en_reso->SetTitleOffset(1.0,"XY");
   H_en_reso->SetLabelOffset(0.01,"XY");
   H_en_reso->SetLabelSize(0.05,"XY");


  //  TH1F2ascii(H_en_reso,"engreso","./","\t");

  // canv->SaveAs("name.pdf");
    
}


