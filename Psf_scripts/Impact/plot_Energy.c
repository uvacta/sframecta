//==========
//STL (standard C library)
#include <iostream>
//#include <ofstream>
//=============
//ROOT
#include <TLatex.h>
#include <TFile.h>
#include <TH1F.h>
#include <TH2F.h>
#include <TH1D.h>
//graphic output:
#include <TCanvas.h>
//nice legend:
#include <TLegend.h>
//colors and stuff:
#include <TStyle.h>
#include "TMath.h"
#include "TROOT.h"
#include "TGraph.h"
#include "TGraphPainter.h"
#include <sstream>
//
// This script visualises relation between a minimum cut in number of telescopes,
// and the loss - as a consequence of this cut - of events within each energy band (i.e. NOT 
// relative to total number of events over entire energy range)
//
// --------------
// Author: Felicia Krauss - 17 October 2016
// --------------
// Modification history:
// 19 Oct 2016: Cosette Molijn (cmolijn): Add other histograms/containment radius
// 01 Nov 2016: cmolijn                 : Replace GetEntries() with Integral()
// 16 Nov 2016: cmolijn                 : Extend script with 2d histogram, add Ncuts and graphs
// ......................
// February 2017 cmolijn -
// July 2017 cmolijn




//===========================
//Function to export histogram to ASCII File to plot with other programs


void TH1F2ascii(TH1F* hist,std::string filename, TString folder, TString separator)
{
  /**
   * \brief Export Single Histogram into ASCII file
   */
  Int_t i,j;
  Double_t xcenter, xwidth;
  Bool_t success=kFALSE;
  filename = folder + hist->GetName() + ".txt";
  ofstream file_out(filename);
  file_out << "# Output " << hist->ClassName() << ": " << hist->GetName() << " (" <<
 hist->GetTitle() << ")\n";
  

  
  if (hist->GetDimension()==1)
    {
      file_out << "# BinCenter" << separator << "Content" << separator << "BinHalfWidth" << separator << "Error\n";
      for (i=1; i<=hist->GetNbinsX(); i++)
        file_out << hist->GetBinCenter(i) << separator << hist->GetBinContent(i) << separator << hist->GetBinWidth(i)/2 << separator << hist->GetBinError(i) << endl;
      if (i>1)
        success=kTRUE;
    }
    file_out.close();
    if (success == kTRUE)
      cout << "*** Histogram " << hist->GetName() << " written to " << filename << endl;
    //return success;

  
}
  



void plot_Energy(Double_t En_min=0.01, Double_t En_max=300.){

  gROOT->SetStyle("Plain");
  gStyle->SetOptStat(1000000001);  

 // first make an array for the energy bins
  Double_t En_bins [25] = {  5.01187234e-02, 7.07945784e-02, 1.00000000e-01,1.41253754e-01, 1.99526231e-01, 2.81838293e-01, 3.98107171e-01, 5.62341325e-01, 7.94328235e-01, 1.12201845e+00, 1.58489319e+00, 2.23872114e+00, 3.16227766e+00, 4.46683592e+00, 6.30957344e+00, 8.91250938e+00, 1.25892541e+01, 1.77827941e+01, 2.51188643e+01, 3.54813389e+01, 5.01187234e+01, 7.07945784e+01, 1.00000000e+02, 1.41253754e+02, 1.99526231e+02};

  // Declare 2D hist
  TH1F* H_En = new TH1F( "H_En", "; Energy",  24, En_bins  ) ;
  // TH1F* H_En_cut = new TH1F( "H_En_cut", "; Energy",  26, En_bins  ) ;   


  TFile * inputFile = new TFile("../../output/PsfStudies.MC.Impact.prod3.root", "READ");
  if(!inputFile || inputFile->IsZombie()) return;
    TTree * tree = dynamic_cast<TTree *>(inputFile->Get("tree"));
    if(!tree) return;
      Double_t AR = 0;
      Double_t trueEn = 0;
      Double_t trueImp = 0;
      Int_t Ntelshower = 0;
      //again, pointer to the variable needed
      tree->SetBranchAddress("Theta", &AR);
      tree->SetBranchAddress("TrueEnergy", &trueEn);
      tree->SetBranchAddress("TrueImpact", &trueImp);
      tree->SetBranchAddress("Ntelshower", &Ntelshower);

 
      //Loop over the tree with the set cuts on the entries: 
      // ======= Energy + Ntel =======
      for(Int_t entry = 0, end = tree->GetEntries(); entry<end; ++entry ) {
        tree->GetEntry(entry);
	if(Ntelshower < 10)
	  continue;

        H_En->Fill(trueEn);	  

      }

      H_En->Draw("hist");

      TH1F2ascii(H_En,"H_En","entire_distributions10July/Energy_range_Ncut10","\t");


} // End of main 




