//==========
//STL (standard C library)
#include <iostream>
//#include <ofstream>
//=============
//ROOT
#include <TLatex.h>
#include <TFile.h>
#include <TH1F.h>
#include <TH2F.h>
#include <TH1D.h>
//graphic output:
#include <TCanvas.h>
//nice legend:
#include <TLegend.h>
//colors and stuff:
#include <TStyle.h>
#include "TMath.h"
#include "TROOT.h"
#include "TGraph.h"
#include "TGraphPainter.h"
#include <sstream>
//
// This script visualises 3 containment radii of angular resolution 
// vs the minimum number of telescopes, and the event loss for this minimum cut
// on the number of telescopes. It is possible to do this for an energy range as well.
//
// --------------
// Author: Felicia Krauss - 17 October 2016
// --------------
// Modification history:
// 19 Oct 2016: Cosette Molijn (cmolijn): Add other histograms/containment radius
// 01 Nov 2016: cmolijn                 : Replace GetEntries() with Integral()
// 16 Nov 2016: cmolijn                 : Extend script with 2d histogram, add Ncuts and graphs

Double_t GetContainmentBinCenter(const TH1D* pInput, Double_t pRatio) {
  Double_t limit = pInput->Integral(0, pInput->GetNbinsX() + 1) * pRatio;
  Double_t sum = 0;
  for(Int_t bin = 1, end = pInput->GetNbinsX(); bin<end;++bin ) {
    sum += pInput->GetBinContent(bin);
    if(sum  >= limit)
      return pInput->GetXaxis()->GetBinCenter(bin);
  }
  return pInput->GetXaxis()->GetXmax();
}


TH1F * Mkplot_Ntel(const TH2F* pInput, Double_t pRatio, Int_t pNtelMax,  const std::string & pName, const std::string & pTitle) {

  std::cout << "Impact =========== " << std::endl;
  pInput->Print("base");
  
  //Get 1D histogram - Impact parameter loop
  TH1F * hist = new TH1F(pName.c_str(), pTitle.c_str(), pNtelMax, 0, pNtelMax);
  for(Int_t bin = 1, end = pInput->GetNbinsX() ; bin<=end; ++bin) {

    const TH1D* projection = pInput->ProjectionX("proc", pInput->GetYaxis()->FindBin(bin),pNtelMax); 
     hist->SetBinContent(bin, GetContainmentBinCenter(projection, pRatio));

     delete projection;

   }// End of impact paramaeter loop
  // hist->Print("base");
  return hist;
}


TH1F * Mkplot_Ntel_loss(const TH2F* pInput, Double_t tot_entries, Int_t pNtelMax, const std::string & pName, const std::string & pTitle) {

  //Get 1D histogram - Impact parameter loop
  TH1F * hist = new TH1F(pName.c_str(), pTitle.c_str(),pNtelMax, 0, pNtelMax);
  for(Int_t bin = 0, end =  pNtelMax; bin<=end; ++bin) {

    const TH1D* projection = pInput->ProjectionX("proc", pInput->GetYaxis()->FindBin(bin), pNtelMax); 
     Double_t entries = projection->GetEntries();
     Double_t loss = (1 - (entries/tot_entries)) * 100; 
     hist->SetBinContent(bin,loss );

     delete projection;

   }// End of impact paramaeter loop
  return hist;
}




void plotNtelvsAR(Double_t En_min=0.07, Double_t En_max=0.1, Int_t pNtelMax=10){


  gROOT->SetStyle("Plain");

  //Define fonts:
  gStyle->SetLabelFont(22,"X");
  gStyle->SetStatFont(22);
  gStyle->SetTitleFont(22,"xyz");
  gStyle->SetTitleFont(22,"h");
  gStyle->SetLegendFont(2);
  gStyle->SetOptStat(1000000001);

  
  // Declare 3D hist

  TH2F* H_2var_Ntel = new TH2F( "2var_Ntel", ";#Theta^2; Ntel min", 
			   50000, 0., 0.5, 55 ,0. , 55  ) ;   


  TFile * inputFile = new TFile("../../output/PsfStudies.MC.Impact.prod3.root", "READ");
  if(!inputFile || inputFile->IsZombie()) return;
    TTree * tree = dynamic_cast<TTree *>(inputFile->Get("tree"));
    if(!tree) return;
      Double_t ARsq = 0;
      Double_t trueEn = 0;
      Double_t trueImp = 0;
      Int_t Ntelshower = 0;
      //again, pointer to the variable needed
      tree->SetBranchAddress("ThetaSq", &ARsq);
      tree->SetBranchAddress("TrueEnergy", &trueEn);
      tree->SetBranchAddress("TrueImpact", &trueImp);
      tree->SetBranchAddress("Ntelshower", &Ntelshower);

	
      //Loop over the tree with the set cuts on the entries: 
      // ======= Energy + Ntel =======
      for(Int_t entry = 0, end = tree->GetEntries(); entry<end; ++entry ) {
        tree->GetEntry(entry);
	if( trueEn < En_min)
          continue; //we cut on the energy here
        if(trueEn > En_max)
          continue;

        H_2var_Ntel->Fill(ARsq, Ntelshower);	
      }
      std::cout << " # entries = " << H_2var_Ntel->GetEntries() << std::endl;
      Double_t entries_tot = H_2var_Ntel->GetEntries();  
      // The total number of entries of the Impact reco method

	
  // ============= Construct canvas =============
  // ============================================
  TCanvas* canv = new TCanvas("c1","Cut on # telescopes (minimum) vs Angular Resolution");
  canv->SetFillColor(0);
  canv->Divide(2,2);
  canv->cd(1);

  TH1F* hist = Mkplot_Ntel(H_2var_Ntel, 0.68, pNtelMax,
  			    "","Impact method - Ntel cut vs r68 - SST - 0.02 - 0.1 TeV; min. Ntel; r68 (deg^2)  ");
 TH1F* hist80 = Mkplot_Ntel(H_2var_Ntel, 0.80, pNtelMax,
  			    "","Impact method - Ntel cut vs r80 - SST - 0.02 - 0.1 TeV; min. Ntel; r80 (deg^2)  ");
 TH1F* hist90 = Mkplot_Ntel(H_2var_Ntel, 0.90, pNtelMax,
  			    "","Impact method - Ntel cut vs r90 - SST - 0.02 - 0.1 TeV ; min. Ntel; r90 (deg^2)  ");
  
  TH1F* histloss = Mkplot_Ntel_loss(H_2var_Ntel, entries_tot, pNtelMax,
  	    ""," Impact method - Ntel cut vs  Event loss - SST - 0.02 - 0.1 TeV; min. Ntel; %  ");


  hist->SetMarkerStyle(20);
  hist->SetMarkerColor(2);
  //  hist->SetMarkerSize(1.2);
  //  hist->GetXaxis()->SetRangeUser(0,20);
  hist->GetYaxis()->SetTitleOffset(1.3);
  hist->Draw("PM");

  canv->cd(2);
  hist80->SetMarkerStyle(20);
  hist80->SetMarkerColor(2);
  //  hist80->SetMarkerSize(1.2);
  //  hist80->GetXaxis()->SetRangeUser(0,20);
  hist80->GetYaxis()->SetTitleOffset(1.3);
  hist80->Draw("PM");

  canv->cd(3);
  hist90->SetMarkerStyle(20);
  hist90->SetMarkerColor(2);
  //  hist90->SetMarkerSize(1.2);
  //  hist90->GetXaxis()->SetRangeUser(0,20);
  hist90->GetYaxis()->SetTitleOffset(1.3);
  hist90->Draw("PM");

  canv->cd(4);
  histloss->SetMarkerStyle(20);
  histloss->SetMarkerColor(2);
  // histloss->SetMarkerSize(1.2);
  //  histloss->GetXaxis()->SetRangeUser(0,20);
  histloss->Draw("PM");


  
  canv->SaveAs("plots_Impact_LST/NtelvsAR_LST_v2.pdf");


} // End of main 




