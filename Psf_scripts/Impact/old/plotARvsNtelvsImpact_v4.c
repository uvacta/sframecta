//==========
//STL (standard C library)
#include <iostream>
//#include <ofstream>
//=============
//ROOT
#include <TLatex.h>
#include <TFile.h>
#include <TH1F.h>
#include <TH2F.h>
#include <TH1D.h>
//graphic output:
#include <TCanvas.h>
//nice legend:
#include <TLegend.h>
//colors and stuff:
#include <TStyle.h>
#include "TMath.h"
#include "TROOT.h"
#include "TGraph.h"
#include "TGraphPainter.h"
#include <sstream>
//
//This script visualises the relation of the number of telescopes in a shower 
// versus the angular resolution.
//
// --------------
// Author: Felicia Krauss - 17 October 2016
// --------------
// Modification history:
// 19 Oct 2016: Cosette Molijn (cmolijn): Add other histograms/containment radius
// 01 Nov 2016: cmolijn                 : Replace GetEntries() with Integral()
// 16 Nov 2016: cmolijn                 : Extend script with 2d histogram, add Ncuts and graphs

Double_t GetContainmentBinCenter(const TH1D* pInput, Double_t pRatio) {
  Double_t limit = pInput->Integral(0, pInput->GetNbinsX() + 1) * pRatio;
  Double_t sum = 0;
  for(Int_t bin = 0, end = pInput->GetNbinsX(); bin<end;++bin ) {
    sum += pInput->GetBinContent(bin);
    if(sum  >= limit)
      return pInput->GetXaxis()->GetBinCenter(bin);
  }
  return pInput->GetXaxis()->GetXmax();
}


TH1F * Mkplot_Impact(const TH2F* pInput, Double_t pRatio, Double_t pMaxImpact, Double_t pImpactStep, const std::string & pName, const std::string & pTitle) {

  std::cout << "Impact =========== " << std::endl;
  pInput->Print("base");
  
  //Get 1D histogram - Impact parameter loop
  TH1F * hist = new TH1F(pName.c_str(), pTitle.c_str(), pMaxImpact/pImpactStep, 0, pMaxImpact);
  for(Int_t bin = 1, end =  pMaxImpact/pImpactStep; bin<=end; ++bin) {

     const TH1D* projection = pInput->ProjectionX("proc", 0, pInput->GetYaxis()->FindBin(bin*pImpactStep)); 
     hist->SetBinContent(bin, GetContainmentBinCenter(projection, pRatio));

     delete projection;

   }// End of impact paramaeter loop
  // hist->Print("base");
  return hist;
}


TH1F * Mkplot_Impact_loss(const TH2F* pInput, Double_t tot_entries, Double_t pRatio, Double_t pMaxImpact, Double_t pImpactStep, const std::string & pName, const std::string & pTitle) {

  //Get 1D histogram - Impact parameter loop
  TH1F * hist = new TH1F(pName.c_str(), pTitle.c_str(), pMaxImpact/pImpactStep, 0, pMaxImpact);
  for(Int_t bin = 1, end =  pMaxImpact/pImpactStep; bin<=end; ++bin) {

     const TH1D* projection = pInput->ProjectionX("proc", 0, pInput->GetYaxis()->FindBin(bin*pImpactStep)); 
     Double_t entries = projection->GetEntries();
     Double_t loss = (1 - (entries/tot_entries)) * 100; 
     hist->SetBinContent(bin,loss );

     delete projection;

   }// End of impact paramaeter loop
  return hist;
}



/* TH1F * Mkplot_Ntel(const TH2F* pInput, Double_t pRatio, Int_t pMaxNtel, Int_t pMinNtel, Double_t pMaxImpact, Double_t pImpactStep, const std::string & pName, const std::string & pTitle) { */

/*   std::cout << "Ntel =========== " << std::endl; */
/*   pInput->Print("base"); */

/*   //Get 1D histogram - Ntel loop */
/*   TH1F * hist = new TH1F(pName.c_str(), pTitle.c_str(), pMaxNtel, 0, pMaxNtel); */
/*   for(Int_t bin = pMinNtel, end =  pMaxNtel; bin<=end; ++bin) { */

/*     const TH1D* projection = pInput->ProjectionX("proc", pInput->GetYaxis()->FindBin(bin),pMaxNtel+1); */
/*     hist->SetBinContent(bin, GetContainmentBinCenter(projection, pRatio)); */

/*      delete projection; */

/*    }// End of Ntel loop */
/*   hist->Print("base"); */
/*   return hist; */
/* } */



void plotARvsNtelvsImpact_v4(Double_t En_min=1.0, Double_t En_max=5, Int_t MaxNtel_plot=15){


  gROOT->SetStyle("Plain");

  //Define fonts:
  gStyle->SetLabelFont(22,"X");
  gStyle->SetStatFont(22);
  gStyle->SetTitleFont(22,"xyz");
  gStyle->SetTitleFont(22,"h");
  gStyle->SetLegendFont(2);
  gStyle->SetOptStat(1000000001);


  Double_t entries_tot = 1165308;  // The total number of entries of the Impact reco method
  
  // Declare 3D hist
  TH2F* H_2var_Imp = new TH2F( "2var_Imp", ";#Theta^2; Max. impact par", 
			   50000, 0., 0.5, 2000 ,0. , 2000  ) ;
  TH2F* H_2var_Ntel = new TH2F( "2var_Ntel", ";#Theta^2; Ntel min", 
			   50000, 0., 0.5, 55 ,0. , 55  ) ;   


  TFile * inputFile = new TFile("../../output/PsfStudies.MC.Impact.prod3.root", "READ");
  if(!inputFile || inputFile->IsZombie()) return;
    TTree * tree = dynamic_cast<TTree *>(inputFile->Get("tree"));
    if(!tree) return;
      Double_t ARsq = 0;
      Double_t trueEn = 0;
      Double_t trueImp = 0;
      Int_t Ntelshower = 0;
      //again, pointer to the variable needed
      tree->SetBranchAddress("ThetaSq", &ARsq);
      tree->SetBranchAddress("TrueEnergy", &trueEn);
      tree->SetBranchAddress("TrueImpact", &trueImp);
      tree->SetBranchAddress("Ntelshower", &Ntelshower);

	
      //Loop over the tree with the set cuts on the entries: 
      // ======= Energy + Ntel =======
      for(Int_t entry = 0, end = tree->GetEntries(); entry<end; ++entry ) {
        tree->GetEntry(entry);
	  
        if( trueEn < En_min)
          continue; //we cut on the energy here
	if(trueEn > En_max)
	  continue;
	if(Ntelshower < 6)
	  continue;
        //This is only executed if the energy is above the min.cut and under the max. cut
        //=============== Do fancy stuff here =========================
        //
        //Fill the 3D histogram:
        H_2var_Imp->Fill(ARsq, trueImp);	
      }
        std::cout << " # entries = " << H_2var_Imp->GetEntries() << std::endl;
	 
	/* // ======= Energy + Impact parameter ======= */
	/* for(Int_t entry = 0, end = tree->GetEntries(); entry<end; ++entry ) { */
	/*   tree->GetEntry(entry); */
	  
	/*   if( trueEn < En_min && trueEn > En_max)// && trueImp > pMaxImpact ) */
	/*     continue; //we cut on the energy(and other par) here */
	    
	/*   //This is only executed if the energy is above the min.cut and under the max. cut */
	/*   //=============== Do fancy stuff here ========================= */
	/*   // */
	/*   //Fill the 3D histogram: */
	/*   H_2var_Ntel->Fill(ARsq, Ntelshower);	 */
	/* } */
	/*  std::cout << " # entries = " << H_2var_Ntel->GetEntries() << std::endl; */

	
  // ============= Construct canvas =============
  // ============================================
  TCanvas* canv = new TCanvas("c1","Cut on # telescopes (minimum) vs Angular Resolution");
  canv->SetFillColor(0);
  //  canv->Divide(1,2);
  //  canv->cd(1);

  TH1F* hist = Mkplot_Impact(H_2var_Imp, 0.68, 1500, 50,
  			    "r68 vs Impact parameter","1 - 5 TeV, Ncut=6 ; max. Impact.; r68 (deg^2)  ");
  // TH1F* histloss = Mkplot_Impact_loss(H_2var_Imp, entries_tot, 0.68, 1500, 50,
  //	    "Event loss","0.5 - 1 TeV, Ncut=6 ; max. Impact.; %  ");
  hist->SetMarkerStyle(20);
  hist->SetMarkerColor(2);
  hist->Draw("PM");
//  canv->cd(2);
//  histloss->SetMarkerStyle(20);
//  histloss->SetMarkerColor(2);
//  histloss->Draw("SAME PM");



  /* canv->cd(2); */
  /* TH1F* histb = Mkplot_Impact(H_2var_Imp, 0.68, MaxNtel_plot, 3, 0, 1500, */
  /* 			      std::string & ,"Whole En-range, Ncut=1 ; max. Impact.; r68  "); */
  /* histb->SetMarkerStyle(20); */
  /* histb->SetMarkerColor(2); */
  /* histb->Draw("PM"); */

  /* TH1F* histb = Mkplot_Ntel(H_2var_Ntel, 0.68, pMaxNtel, pMinNtel, pMaxImpact, pImpactStep, */
  /* 				"Cont68_b","Whole En-range, Max Impact=2000 ;  Min Ntel; r68  "); */
  /* histb->SetMarkerStyle(20); */
  /* histb->SetMarkerColor(2); */
  /* histb->Draw("PM"); */
  /* histb->GetXaxis()->SetRangeUser(0,15); */


} // End of main 




