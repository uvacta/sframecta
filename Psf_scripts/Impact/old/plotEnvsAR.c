//==========
//STL (standard C library)
#include <iostream>
//#include <ofstream>
//=============
//ROOT
#include <TLatex.h>
#include <TFile.h>
#include <TH1F.h>
#include <TH1D.h>
//graphic output:
#include <TCanvas.h>
//nice legend:
#include <TLegend.h>
//colors and stuff:
#include <TStyle.h>
#include "TMath.h"
#include "TGraph.h"
#include <sstream>
//
//This script visualises the relation of the number of telescopes in a shower 
// versus the angular resolution.
//
// --------------
// Author: Felicia Krauss - 17 October 2016
// --------------
// Modification history:
// 19 Oct 2016: Cosette Molijn (cmolijn): Add other histograms/containment radius
// 01 Nov 2016: cmolijn                 : Replace GetEntries() with Integral()
// 16 Nov 2016: cmolijn                 : Extend script with 2d histogram, add Ncuts and graphs

void TH1F2ascii(TH1F* hist,std::string filename, TString folder, TString separator);

//==============

// Don't define as cstring here, unclear why
//define default value for variable
//several variable separated by ","
void plotEnvsAR(std::string FName="../../output/PsfStudies.MC.Impact.prod3.root")
{
  gROOT->SetStyle("Plain");

  //Define fonts:
  gStyle->SetStatFontSize(0.04); 
  gStyle->SetLabelFont(22,"X");
  gStyle->SetStatFont(22);
  gStyle->SetTitleFont(22,"xyz");
  gStyle->SetTitleFont(22,"h");
  gStyle->SetLegendFont(2);
    
  // * means pointer
  // convert string file name to c string, unclear why not before
  // Alternative: WRITE, RECREATE (=overwrite)

  // -----------------------
  // Read output MC File and define the histogram
  // -----------------------
  TFile* MCfile = new TFile (FName.c_str(),"READ");
    if(!MCfile)
      {
	std::cerr << "File not found: "<< FName << "Check if it exists." << std::endl;
      }
  // Define new histogram that points to histogram in file
  TH2F* H_ARvsEn = (TH2F*)MCfile->Get("ARvslogEn");
  H_ARvsEn->GetYaxis()->Print("all");


  // -----------------------
  // Here we will project the 2D histogram onto 1D histograms, by slicing the 2D histogram
  // into 1D histograms
  // -----------------------
  // First bin of the histogram is the underflow bin, so start with b = 1
  // Get total number of x and y bins (E cuts) and check this with bin centers
  Int_t n_biny   = H_ARvsEn->GetYaxis()->GetNbins(); // AR bins
  Int_t n_binx   = H_ARvsEn->GetXaxis()->GetNbins(); // Energy bins
  //  float Ntel_tot = H_ARvsNtel->GetYaxis()->GetBinCenter(n_biny+1);

  std::cout << "Number of Y (Energy, TeV) bins  = "<< n_biny << std::endl;
  std::cout << "Number of X (#Theta^2) bins = " << n_binx << std::endl;

  //Get total Number of Events:
  Int_t tot_entr=H_ARvsEn->Integral(1,n_binx + 1); // You want to count the overflow bin of course
  std::cout << "Total number of entries in simulation:" << tot_entr << std::endl;

  //Define the lists with the radii and Ncut 
   
  Double_t L_r68[n_binx];
  Double_t L_r80[n_binx];
  Double_t L_r90[n_binx];
  Double_t L_Enbin[n_binx];
  Double_t L_entries[n_binx];
  Double_t L_ev_loss[n_binx];
  
  // 'el' will define the nth element for the lists to fill later in the loop
   Int_t el = 0; 

  for (Int_t b=1 ; b<=n_biny ; ++b ){
    // This for-loop is for making the Ntel cuts. For each cut we calculate the r68 and r80
    // from the projected 1D histogram
  
     // Project the 2d histogram onto a 1D
     TH1D* H_Enbin = H_ARvsEn->ProjectionX("",1 ,b );
    
     // The first bin in the histogram is the underflow. Since there is no underflow
     // here with the AR,
     // we must start with j = 1 instead of j=0.

     // Calculate the number of entries and the total entry loss
     Double_t   n_entr  = H_Enbin->Integral(1,n_binx + 1);
     Double_t   ev_loss = (1-n_entr/tot_entr )*100; // percentage

     // The first bin in the histogram is the underflow. Since there is no underflow
     // here with the AR, we must start with j = 1 instead of j=0.
     // Set the limits:
     Double_t limit68 = 0.68*n_entr;
     Double_t i68=0;
     Int_t j68=1;

     Double_t limit80 = 0.80*n_entr;
     Double_t i80=0;
     Int_t j80=1;

     Double_t limit90 = 0.90*n_entr;
     Double_t i90=0;
     Int_t j90=1;

     // And count entries up and until the set limits
     while (i68 <= limit68)
       {
         i68+=H_Enbin->GetBinContent(j68);
         j68++;
       }

     while (i80 <= limit80)
       {
         i80+=H_Enbin->GetBinContent(j80);
         j80++;
       }

     while (i90 <= limit90)
       {
         i90+=H_Enbin->GetBinContent(j90);
         j90++;
       }


     //Get the r68 value of the histogram
     Double_t r68 = H_Enbin->GetBinCenter(j68);
     Double_t r80 = H_Enbin->GetBinCenter(j80);
     Double_t r90 = H_Enbin->GetBinCenter(j90);

     // Let's print some info:
     std::cout << "Ebin (via bin center) = "<< H_ARvsEn->GetYaxis()->GetBinCenter(b) << std::endl;
     std::cout << "r68, r80 & r90 = "<< r68 << " ; "<< r80 << " ; " << r90 << std::endl;

     // Finally add all the variables to the corresponding arrays
     L_r68[el]     = r68;
     L_r80[el]     = r80;
     L_r90[el]     = r90;
     L_Enbin[el]   = H_ARvsEn->GetYaxis()->GetBinCenter(b) ;
     L_entries[el] = n_entr;
     L_ev_loss[el] = ev_loss;

     ++el; // And +1 the nth element
     std::cout << "bin = " << el << std::endl; 

   }

  const Int_t n_el = el;
  std::cout << "Loop works yehhhh" << std::endl;

  
   // PLOT FroM HERE
  TCanvas* canv = new TCanvas("c1","Angular Resolution vs Energy bins in shower");
  canv->SetFillColor(0);
  canv->Divide(2,2);
  
  // Plot Ntelcut vs r68
  canv->cd(1);
  TGraph* gr_r68 = new TGraph(n_el,  L_Enbin,  L_r68);
  gr_r68-> SetTitle("AR r68 (deg^2) vs Maximum Energy bin; Energy (TeV); r68 (deg^2)");
  gr_r68->Draw("A*");
  gr_r68->GetYaxis()->SetTitleOffset(1.4);

  // Plot Ntelcut vs r80
  canv->cd(2);
  TGraph* gr_r80 = new TGraph(n_el,  L_Enbin,  L_r80);
  gr_r80-> SetTitle("AR r80 (deg^2) vs Maximum Energy bin;  Energy (TeV); r80 (deg^2)");
  gr_r80->Draw("A*");
  gr_r80->GetYaxis()->SetTitleOffset(1.4);

  // Plot Ntelcut vs r90
  canv->cd(3);
  TGraph* gr_r90 = new TGraph(n_el,  L_Enbin,  L_r90);
  gr_r90-> SetTitle("AR r90 (deg^2) vs Maximum Energy bin;  Energy (TeV); r90 (deg^2)");
  gr_r90->Draw("A*");
  gr_r90->GetYaxis()->SetTitleOffset(1.4);

  // Plot Ntelcut vs number of remaining events
  canv->cd(4);
  TGraph* gr_entries = new TGraph(n_el,  L_Enbin,  L_entries);
  gr_entries->SetTitle("Minimum Ebin vs number of entries/events;  Energy (TeV) ; # entries");
  gr_entries->Draw("A*");
  gr_entries->GetYaxis()->SetTitleOffset(1.4);



  // ===============
  // =============== Just to check how the TH1F histogram looks like for e.g. Ncut = 15

  TCanvas* canv2 = new TCanvas("c2","Angular Resolution vs Energy bins in shower");
  canv2->SetFillColor(0);
  canv2->Divide(2,1);

  TGraph* gr_loss = new TGraph(n_el, L_Enbin, L_ev_loss);
  gr_loss->SetTitle("Maximum Ebin vs percentage event loss;  Energy (TeV) ; %");
  gr_loss->Draw("AC*");
  gr_loss->GetYaxis()->SetTitleOffset(1.4);

  canv2->cd(1);
  // TH1D* H_Enbin1 = H_ARvsEn->ProjectionX("",2 ,2 );
  // H_Enbin1->SetTitle("AR distribution for Ebin = 2  case; #Theta^2 (deg^2); # entries");
  // H_Enbin1->SetTitleOffset(1.5,"Y");
  // H_Enbin1->Draw("Hist");

  // canv2->cd(2);
  TH1D* H_Enbin2 = H_ARvsEn->ProjectionX("",10 ,10 );
  H_Enbin2->SetTitle("AR distribution for Ebin =  10 case; #Theta^2 (deg^2); # entries");
  H_Enbin2->SetTitleOffset(1.5,"Y");
  // H_Enbin2->Draw("Hist");




  /* float xbins[36]; */
  /* int   el1 = 0; */
  /* int   plist[7] = {-3,-2,-1,0,1,2,3}; */

  /* for(int i=0 ; i<= 6; i++){ */
  /*   int j = plist[i]; */
    
  /*   for(int k = 1; k<=9; k++ ){ */
      
  /*     float num = k * pow(10,j); */
  /*     xbins[el1] = num; */

  /*     el1++;  */

  /*     std::cout<< "power = " << num << std::endl; */

  /*   } */

  /* } */



  


   
  // ===============
}

