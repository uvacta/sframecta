//==========
//STL (standard C library)
#include <iostream>
//#include <ofstream>
//=============
//ROOT
#include <TLatex.h>
#include <TFile.h>
#include <TH1F.h>
//graphic output:
#include <TCanvas.h>
//nice legend:
#include <TLegend.h>
//colors and stuff:
#include <TStyle.h>
#include "TMath.h"


// --------------
// Author: Felicia Krauss - 17 October 2016
// --------------
// Modification history:
// 19 Oct 2016: Cosette Molijn (cmolijn): Add other histograms/containment radius
// 01 Nov 2016: cmolijn                 : Replace GetEntries() with Integral()


void TH1F2ascii(TH1F* hist,std::string filename, TString folder, TString separator);

//==============

// Don't define as cstring here, unclear why
//define default value for variable
//several variable separated by ","
void plotAR_Impact(std::string FName="../../output/PsfStudies.MC.Impact.prod3.root")
{
  gROOT->SetStyle("Plain");

  //Define fonts:
  gStyle->SetStatFontSize(0.04); 
  gStyle->SetLabelFont(22,"X");
  gStyle->SetStatFont(22);
  gStyle->SetTitleFont(22,"xyz");
  gStyle->SetTitleFont(22,"h");
  gStyle->SetLegendFont(2);
  gStyle->SetOptStat(111111);
    
  // * means pointer
  // convert string file name to c string, unclear why not before
  // Alternative: WRITE, RECREATE (=overwrite)

  // Read output MC File
  TFile* MCfile = new TFile (FName.c_str(),"READ");
    if(!MCfile)
      {
	std::cerr << "File not found: "<< FName << "Check if it exists." << std::endl;
      }
  // Define new histogram that points to histogram in file
  TH1F* H_ang_reso = (TH1F*)MCfile->Get("AngularResolution");

  H_ang_reso->Print();

  // Get Number of Bins on x axis:
  int numx=H_ang_reso->GetXaxis()->GetNbins();
  std::cout << "Number of x bins:" << numx << std::endl;

  //Get total Number of Events:
  int num=H_ang_reso->Integral(1,101);
  std::cout << "Number of entries:" << num << std::endl;
   

 // The first bin in the histogram is the underflow. Since there is no underflow here with the AR,
  // we must start with j = 1 instead of j=0.
  
  //SET LIMIT
  double limit = 0.68*num;
  double i=0;
  int j=0;

  double limit80 = 0.80*num;
  double i80=0;
  int j80=1; 

  std::cout << i << "=i; j="<<j << std::endl;
  std::cout << i80 << "=i80; j80="<<j80 << std::endl;  


  while (i <= limit)
    {
      i+=H_ang_reso->GetBinContent(j);
      j++;
    }

 while (i80 <= limit80)
    {
      i80+=H_ang_reso->GetBinContent(j80);
      j80++;
    }

  // j = x index of 68%
  std::cout << i << "=i; j="<<j << " = "<< H_ang_reso->GetBinCenter(j) << "deg^2" << std::endl;
  std::cout << i80 << "=i80; j80="<<j80 << " = "<<  H_ang_reso->GetBinCenter(j80) << "deg^2"   <<std::endl;


  /* //Get x value of histogram */
  /* //int yval = H_ang_reso->GetBinContent(j); */
  float numfinal = H_ang_reso->GetBinCenter(j);
  float numfinal80 = H_ang_reso->GetBinCenter(j80);

  //===============
  // Plotting 
  //===============
  TCanvas* canv = new TCanvas("Angular Resolution");
  canv->SetFillColor(0);
  canv->SetLogy();

  //canv->Divide (2,1)  //(horizontal,vertikal)
  //switch plots with cd (number)

  //Fill color histogram
  H_ang_reso->SetFillColor(42);
  // H_ang_reso->GetXaxis()->SetRangeUser(0,0.035);

  // Add lines for the containment regions
  TLine* line = new TLine(numfinal,H_ang_reso->GetMinimum(),numfinal,H_ang_reso->GetBinContent(j));
  line->SetLineWidth(2);
  TLine* line80 = new TLine(numfinal80,H_ang_reso->GetMinimum(),numfinal80,H_ang_reso->GetBinContent(j80));
  line80->SetLineWidth(2);

  // Draw the histogram
  H_ang_reso->Draw("HIST");

  //And the regions
  TH1F *h1c = (TH1F*)H_ang_reso->Clone();
  h1c->SetFillColor(30);
  h1c->GetXaxis()->SetRangeUser(0,numfinal);
  h1c->Draw("HIST,same");

  TH1F *h1c80 = (TH1F*)H_ang_reso->Clone();
  h1c80->SetFillColor(40);
  h1c80->GetXaxis()->SetRangeUser(numfinal,numfinal80);
  h1c80->Draw("HIST,same");

  //Finish setting up histogram

   H_ang_reso->SetTitle("AngularResolution; #Theta^2; Number of events ");
   //  H_ang_reso->GetXaxis()->SetRangeUser(0,0.05);
   H_ang_reso->SetTitleSize(0.05,"XY");
   H_ang_reso->SetTitleOffset(1.0,"XY");
   H_ang_reso->SetLabelOffset(0.01,"XY");
   H_ang_reso->SetLabelSize(0.05,"XY");


  //  TH1F2ascii(H_ang_reso,"angreso","./","\t");

  // canv->SaveAs("name.pdf");
    
}


