//==========
//STL (standard C library)
#include <iostream>
//#include <ofstream>
//=============
//ROOT
#include <TLatex.h>
#include <TFile.h>
#include <TH1F.h>
#include <TH1D.h>
//graphic output:
#include <TCanvas.h>
//nice legend:
#include <TLegend.h>
//colors and stuff:
#include <TStyle.h>
#include "TMath.h"
#include "TGraph.h"
#include "TGraphPainter.h"
#include <sstream>
//
//This script visualises the relation of the number of telescopes in a shower 
// versus the angular resolution.
//
// --------------
// Author: Felicia Krauss - 17 October 2016
// --------------
// Modification history:
// 19 Oct 2016: Cosette Molijn (cmolijn): Add other histograms/containment radius
// 01 Nov 2016: cmolijn                 : Replace GetEntries() with Integral()
// 16 Nov 2016: cmolijn                 : Extend script with 2d histogram, add Ncuts and graphs

void TH1F2ascii(TH1F* hist,std::string filename, TString folder, TString separator);

//==============

// Don't define as cstring here, unclear why
//define default value for variable
//several variable separated by ","
void plotImpactvsAR(std::string FName="../../output/PsfStudies.MC.Impact.prod3.root")
{
  gROOT->SetStyle("Plain");

  //Define fonts:
  gStyle->SetStatFontSize(0.04); 
  gStyle->SetLabelFont(22,"X");
  gStyle->SetStatFont(22);
  gStyle->SetTitleFont(22,"xyz");
  gStyle->SetTitleFont(22,"h");
  gStyle->SetLegendFont(2);
    
  // * means pointer
  // convert string file name to c string, unclear why not before
  // Alternative: WRITE, RECREATE (=overwrite)

  // -----------------------
  // Read output MC File and define the histogram
  // -----------------------
  TFile* MCfile = new TFile (FName.c_str(),"READ");
    if(!MCfile)
      {
	std::cerr << "File not found: "<< FName << "Check if it exists." << std::endl;
      }
  // Define new histogram that points to histogram in file
  TH2F* H_ImpvsAR = (TH2F*)MCfile->Get("hImpactvsAR");
  H_ImpvsAR->Print("base");


  // -----------------------
  // Here we will project the 2D histogram onto 1D histograms, by making cuts on the 
  // number of telescopes
  // -----------------------
  // First bin of the histogram is the underflow bin, so start with b = 1
  // Get total number of x and y bins (Ntel cuts) and check this with bin centers
  int n_binx   = H_ImpvsAR->GetXaxis()->GetNbins();
  int n_biny   = H_ImpvsAR->GetYaxis()->GetNbins();
  // float Ntel_tot = H_->GetYaxis()->GetBinCenter(n_biny+1);

  std::cout << "Nbins X = " << n_binx  << std::endl;
  std::cout << "Number of Y (#Theta^2) bins:" << n_binx << std::endl;

  //Get total Number of Events:
  int tot_entr=H_ImpvsAR->Integral(1,n_binx + 1); // You want to count the overflow bin of course
  std::cout << "Total number of entries in simulation:" << tot_entr << std::endl;


  //============================
  // Define your variable parameters here
  //============================

    int Impcut_max = 6 ; // Define your max Ntel_cut: atm it is 52, since there are no events with 
                      // with more than 52 telescopes
                      // The minimum number of telescopes of an event is 3, so the lists with radii
                      // and so on will have Ntel_max - 2 elements

  //============================
  //============================

  //Define the lists with the radii and Ncut 
   
  Float_t L_r68[Impcut_max];
  Float_t L_r80[Impcut_max];
  Float_t L_r90[Impcut_max];
  Float_t L_Impcut[Impcut_max];
  Float_t L_entries[Impcut_max];
  Float_t L_ev_loss[Impcut_max];
  
  // 'el' will define the nth element for the lists to fill later in the loop
   Int_t el = 0; 

  for (Int_t b=2; b<=Impcut_max ; ++b ){
    // This for-loop is for making the impact parameter cuts. For each cut we calculate the r68 and r80
    // from the projected 1D histogram
  
     Int_t b_bin = b*100;
     // Project the 2d histogram onto a 1D
     TH1D* H_Impcut = H_ImpvsAR->ProjectionY("",200 , b_bin);
    
     //  H_Impcut->Draw("Hist");
     // The first bin in the histogram is the underflow. Since there is no underflow
     // here with the AR,
     // we must start with j = 1 instead of j=0.     

     // Calculate the number of entries and the total entry loss
     Float_t   n_entr  = H_Impcut->Integral(1,n_biny + 1);
     Float_t   ev_loss = (1-n_entr/tot_entr )*100; // percentage


     // The first bin in the histogram is the underflow. Since there is no underflow
     // here with the AR, we must start with j = 1 instead of j=0.     
     // Set the limits:
     double limit68 = 0.68*n_entr;
     double i68=0;
     int j68=1;

     double limit80 = 0.80*n_entr;
     double i80=0;
     int j80=1;

    double limit90 = 0.90*n_entr;
     double i90=0;
     int j90=1;

     // And count entries up and until the set limits
     while (i68 <= limit68)
       {
         i68+=H_Impcut->GetBinContent(j68);
         j68++;
       }

     while (i80 <= limit80)
       {
         i80+=H_Impcut->GetBinContent(j80);
         j80++;
       }

     while (i90 <= limit90)
       {
         i90+=H_Impcut->GetBinContent(j90);
         j90++;
       }


     //Get the r68 value of the histogram
     Float_t r68 = H_Impcut->GetBinCenter(j68);
     Float_t r80 = H_Impcut->GetBinCenter(j80);
     Float_t r90 = H_Impcut->GetBinCenter(j90);

     // Let's print some info:
     std::cout << "Impcut (via bin center) = "<< H_ImpvsAR->GetYaxis()->GetBinCenter(b_bin) << std::endl;
     std::cout << "Impcut = " << b_bin << std::endl;
     std::cout << "Number of entries = " << n_entr << " and event loss = "<< ev_loss << "%" << std::endl;
     std::cout << "r68, r80 & r90 = "<< r68 << " ; "<< r80 << " ; " << r90 << std::endl;

     // Finally add all the variables to the corresponding arrays
     L_r68[el]     = r68;
     L_r80[el]     = r80; 
     L_r90[el]     = r90;
     L_Impcut[el]    = b_bin; 
     L_entries[el] = n_entr;
     L_ev_loss[el] = ev_loss;

     ++el; // And +1 the nth element

   }

 const Int_t n_el = el;

 // -----------------------
 // PLOT FroM HERE
  TCanvas* canv = new TCanvas("c1","Impact parameter (b) vs Angular Resolution");
  canv->SetFillColor(0);
  canv->Divide(2,2);
  
  // Plot Impcut vs r68
  canv->cd(1);
  TGraph* gr_r68 = new TGraph(n_el,  L_Impcut,  L_r68);
  gr_r68-> SetTitle("Max. impact parameter vs AR r68 (deg^2); Impcut(m); r68 (deg^2)");
  gr_r68->SetMarkerStyle(20);
  gr_r68->SetMarkerColor(2);
  gr_r68->SetMarkerSize(1.);
  gr_r68->Draw("AP");
  gr_r68->GetYaxis()->SetTitleOffset(1.3);

  // Plot Impcut vs r80
  canv->cd(2);
  TGraph* gr_r80 = new TGraph(n_el,  L_Impcut,  L_r80);
  gr_r80-> SetTitle("Max. impact parameter vs AR r80 (deg^2); Impcut; r80 (deg^2)");
  gr_r80->SetMarkerStyle(20);
  gr_r80->SetMarkerColor(2);
  gr_r80->SetMarkerSize(1.);
  gr_r80->Draw("AP");
  gr_r80->GetYaxis()->SetTitleOffset(1.3);

  // Plot Impcut vs r90
   canv->cd(3);
  TGraph* gr_r90 = new TGraph(n_el,  L_Impcut,  L_r90);
  gr_r90-> SetTitle("Max. impact parameter vs AR r90 (deg^2); Impcut (m); r90 (deg^2)");
  gr_r90->SetMarkerStyle(20);
  gr_r90->SetMarkerColor(2);
  gr_r90->SetMarkerSize(1.);
  gr_r90->Draw("AP");
  gr_r90->GetYaxis()->SetTitleOffset(1.3);

  // Plot Impcut vs number of remaining events
   canv->cd(4);
  TGraph* gr_entries = new TGraph(n_el,  L_Impcut,  L_entries);
  gr_entries->SetTitle("Impcut vs number of entries/events; Impcut (m) ; # entries");
  gr_entries->SetMarkerStyle(20);
  gr_entries->SetMarkerColor(2);
  gr_entries->SetMarkerSize(1.);
  //  gr_entries->Draw("AP");
  gr_entries->GetYaxis()->SetTitleOffset(1.3);

  //Make new canvas, one is not enough
  //  TCanvas* canv2 = new TCanvas("c2","Impact parameter (b) vs Angular Resolution");
  //  canv2->SetFillColor(0);
  // canv2->Divide(2,2);

  // canv2->cd(1);
  TGraph* gr_loss = new TGraph(n_el,  L_Impcut,  L_ev_loss);
  gr_loss->SetTitle("Impcut vs event loss; Impcut (m) ; %");
  gr_loss->SetMarkerStyle(20);
  gr_loss->SetMarkerColor(2);
  gr_loss->SetMarkerSize(1.);
  gr_loss->Draw("AP");
  gr_loss->GetYaxis()->SetTitleOffset(0.8);


  //  TGraph2ascii(L_Ncut,L_ev_loss,"plots/gr_bin500_loss","./","\t");

  // canv->SaveAs("plots/EventDisplay_Ntel_AR_bin500.pdf"); // Form("h%d",b)
  // canv2->SaveAs("plots/EventDisplay_Ntel_AR_bin500_loss.pdf");
    
}


