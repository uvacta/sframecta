 //==========
//STL (standard C library)
#include <iostream>
//#include <ofstream>
//=============
//ROOT
#include <TLatex.h>
#include <TFile.h>
#include <TH1F.h>
#include <TH2F.h>
#include <TH1D.h>
//graphic output:
#include <TCanvas.h>
//nice legend:
#include <TLegend.h>
//colors and stuff:
#include <TStyle.h>
#include "TMath.h"
#include "TROOT.h"
#include "TGraph.h"
#include "TGraphPainter.h"
#include <sstream>
//
// This script calculates the error bar on r68 versus impact parameter. 
//
// --------------
// Author: Felicia Krauss - 17 October 2016
// --------------
// Modification history:
// 19 Oct 2016: Cosette Molijn (cmolijn): Add other histograms/containment radius
// 01 Nov 2016: cmolijn                 : Replace GetEntries() with Integral()
// 16 Nov 2016: cmolijn                 : Extend script with 2d histogram, add Ncuts and graphs
// --------------------
// February 2017: cmolijn

Double_t GetContainmentBinCenter(const TH1D* pInput, Double_t pRatio) {
  Double_t limit = pInput->Integral(0, pInput->GetNbinsX() + 1) * pRatio;
  Double_t sum = 0;
  for(Int_t bin = 1, end = pInput->GetNbinsX(); bin<end;++bin ) {
    sum += pInput->GetBinContent(bin);
    if(sum  >= limit)
      return pInput->GetXaxis()->GetBinCenter(bin);
  }
  return pInput->GetXaxis()->GetXmax();
}

Double_t CalculateLim(const TH1D* pInput, Double_t pRatio, Int_t sign){
  TH1D* hist1 = new TH1D("","", 10000, 0., 0.5);

 for(Int_t bin = 1; bin <= pInput->GetNbinsX(); ++bin ){    
    Double_t pois = pow(pInput->GetBinContent(bin), 0.5);

    if(sign == 0){
      Double_t new_cont = pInput->GetBinContent(bin) - pois;
      hist1->SetBinContent(bin, new_cont);
    }
    if(sign == 1){
      Double_t new_cont = pInput->GetBinContent(bin) + pois; 
      hist1->SetBinContent(bin, new_cont);
    }
    
  }
 // Now we have a new AR histogram: let's calculate the new max or min r68
 
 return GetContainmentBinCenter(hist1, pRatio);
}



TH1D * Mkplot_Imp(const TH2D* pInput, Double_t pRatio, Int_t sign, Double_t pMaxImpact, Double_t pImpactStep, 
		     const std::string & pName, const std::string & pTitle) {

  std::cout << "Impact =========== " << std::endl;
  pInput->Print("base");
  
  //Get 1D histogram - Impact parameter loop
  TH1D * hist = new TH1D(pName.c_str(), pTitle.c_str(), pMaxImpact/pImpactStep, 0, pMaxImpact);
  hist->SetBinErrorOption(TH1::kPoisson);
  for(Int_t bin = 1, end =  pMaxImpact/pImpactStep; bin<=end; ++bin) {

     const TH1D* projection = pInput->ProjectionX("proc", 0, pInput->GetYaxis()->FindBin(bin*pImpactStep)); 
     hist->SetBinContent(bin, GetContainmentBinCenter(projection, pRatio));
     
     Double_t minr = CalculateLim(projection, pRatio, 0);
     Double_t maxr = CalculateLim(projection, pRatio, 1);

     Double_t minerr = hist->GetBinContent(bin) - minr;
     Double_t maxerr = maxr - hist->GetBinContent(bin);
     
     if(sign == 0){
       hist->SetBinError(bin, minerr);
     }
     if(sign == 1){
       hist->SetBinError(bin, maxerr);
     }

     std::cout << "Impact parameter cut = " << hist->GetBinCenter(bin) << "; # events = " << projection->GetEntries()<< std::endl;
     std::cout << "stdev projection = " << projection->GetStdDev() << std::endl;
     std::cout << "r68 " << minr << "; " << maxr << "; "  <<  hist->GetBinContent(bin) << std::endl;
     std::cout << "errors (min, max): " << minerr << "; " << maxerr << std::endl;
     
     //hist->SetBinErrorLow(bin, minerr);

     //   std::cout << "error from hist = " << hist->GetBinError(bin) << std::endl;
     
     delete projection;

   }// End of impact paramaeter loop
 



  return hist;
}



void plotImpvsAR_dist(Double_t En_min=0.02, Double_t En_max=0.1, Double_t pMaxImpact=300, Double_t pImpactStep=10){


  gROOT->SetStyle("Plain");

  //Define fonts:
  gStyle->SetLabelFont(22,"X");
  gStyle->SetStatFont(22);
  gStyle->SetTitleFont(22,"xyz");
  gStyle->SetTitleFont(22,"h");
  gStyle->SetLegendFont(2);
  gStyle->SetOptStat(1000000001);

  
  // Declare 3D hist

  TH2D* H_2var_Imp = new TH2D( "2var_Ntel", ";#Theta^2; Max. Impact par", 
			   20000, 0., 1., 2000 ,0. , 2000  ) ;   


  TFile * inputFile = new TFile("../../output/PsfStudies.MC.Impact.prod3.root", "READ");
  if(!inputFile || inputFile->IsZombie()) return;
    TTree * tree = dynamic_cast<TTree *>(inputFile->Get("tree"));
    if(!tree) return;
      Double_t ARsq = 0;
      Double_t trueEn = 0;
      Double_t trueImp = 0;
      Int_t Ntelshower = 0;
      //again, pointer to the variable needed
      tree->SetBranchAddress("ThetaSq", &ARsq);
      tree->SetBranchAddress("TrueEnergy", &trueEn);
      tree->SetBranchAddress("TrueImpact", &trueImp);
      tree->SetBranchAddress("Ntelshower", &Ntelshower);

	
      //Loop over the tree with the set cuts on the entries: 
      // ======= Energy =======
      for(Int_t entry = 0, end = tree->GetEntries(); entry<end; ++entry ) {
        tree->GetEntry(entry);
	if( trueEn < En_min)
          continue; //we cut on the energy here
        if(trueEn > En_max)
          continue;


        H_2var_Imp->Fill(ARsq, trueImp);	
      }
      std::cout << " # entries = " << H_2var_Imp->GetEntries() << std::endl;
      Double_t entries_tot = H_2var_Imp->GetEntries();  
      Double_t stdev_tot   = H_2var_Imp->GetRMS();
      // The total number of entries of the Impact reco method

	
  // ============= Construct canvas =============
  // ============================================
  TCanvas* canv = new TCanvas("c1","Cut on max. Impact parameter vs Angular Resolution");
  canv->SetFillColor(0);
  canv->Divide(1,2);
  canv->cd(1);

  TH1D* hist0   = Mkplot_Imp(H_2var_Imp, 0.68, 0, pMaxImpact, pImpactStep,
  			    "","Impact method - error min - Max. Imp. cut vs r68 - SST - 0.02-0.1 TeV ; Max Imp.(m) ; r68 (deg^2)  ");
  TH1D* hist1   = Mkplot_Imp(H_2var_Imp, 0.68, 1, pMaxImpact, pImpactStep,
  			    "","Impact method - error max - Max. Imp. cut vs r68 - SST - 0.02-0.1 TeV ; Max Imp.(m) ; r68 (deg^2)  ");  

 
  
  hist0->SetMarkerStyle(20);
  hist0->SetMarkerColor(2);
  //  hist->SetMarkerSize(0.4);
  hist0->GetYaxis()->SetTitleOffset(1.3);
  hist0->Draw("X0E1");

  canv->cd(2);
  hist1->SetMarkerStyle(20);
  hist1->SetMarkerColor(2);
  //  hist->SetMarkerSize(0.4);
  hist1->GetYaxis()->SetTitleOffset(1.3);
  hist1->Draw("X0E1");
  
  //  canv->SaveAs("plots_Impact/ImpvsAR_SST_0305TeV.pdf");


} // End of main 




