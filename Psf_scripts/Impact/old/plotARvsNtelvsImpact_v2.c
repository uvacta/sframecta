//==========
//STL (standard C library)
#include <iostream>
//#include <ofstream>
//=============
//ROOT
#include <TLatex.h>
#include <TFile.h>
#include <TH1F.h>
#include <TH2F.h>
#include <TH1D.h>
//graphic output:
#include <TCanvas.h>
//nice legend:
#include <TLegend.h>
//colors and stuff:
#include <TStyle.h>
#include "TMath.h"
#include "TROOT.h"
#include "TGraph.h"
#include "TGraphPainter.h"
#include <sstream>
//
//This script visualises the relation of the number of telescopes in a shower 
// versus the angular resolution.
//
// --------------
// Author: Felicia Krauss - 17 October 2016
// --------------
// Modification history:
// 19 Oct 2016: Cosette Molijn (cmolijn): Add other histograms/containment radius
// 01 Nov 2016: cmolijn                 : Replace GetEntries() with Integral()
// 16 Nov 2016: cmolijn                 : Extend script with 2d histogram, add Ncuts and graphs

Double_t GetContainmentBinCenter(const TH1D* pInput, Double_t pRatio) {
  Double_t limit = pInput->Integral(0, pInput->GetNbinsX() + 1) * pRatio;
  Double_t sum = 0;
  for(Int_t bin = 0, end = pInput->GetNbinsX(); bin<end;++bin ) {
    sum += pInput->GetBinContent(bin);
    if(sum  >= limit)
      return pInput->GetXaxis()->GetBinCenter(bin);
  }
  return pInput->GetXaxis()->GetXmax();
}

//Double_t GetEntryloss(const TH1D* pInput, )


TH1F * MakeAngularPlot(const TH3F* pInput, Double_t pRatio, Double_t pMaxNtel, Double_t pNcut, Double_t pMaxImpact, Double_t pImpactStep, const std::string & pName, const std::string & pTitle) {
  pInput->Print("base");

  //Get 2D histogram - Ntel cut
 
  pInput->GetYaxis()->SetRange(pInput->GetYaxis()->FindBin(pNcut),pInput->GetNbinsY()+1);
  const TProfile2D* projection2d = pInput->Project3DProfile("zx UF OF"); //Number of entries is 'estimated', see decription of project3DProfile on internet
  projection2d->Print("base");
  //  projection2d->Print("all");
  
   //Get 1D histogram - Impact parameter loop
   TH1F * hist = new TH1F(pName.c_str(), pTitle.c_str(), pMaxImpact/pImpactStep, 0, pMaxImpact);
   for(Int_t bin = 1, end =  pMaxImpact/pImpactStep; bin<=end; ++bin) {

     const TH1D* projection = projection2d->ProjectionX("proc", 0, projection2d->GetYaxis()->FindBin(bin*pImpactStep));    
     hist->SetBinContent(bin, GetContainmentBinCenter(projection, pRatio));

     delete projection;

   }// End of impact paramaeter loop

  return hist;
}


void plotARvsNtelvsImpact_v2(Double_t pMaxNtel=55., Double_t pNcut=5., Double_t pMaxImpact = 1500.,
		     Double_t pImpactStep=50.){
 

  const std::string & pFileName_Impact="../../output/PsfStudies.MC.Impact.prod3.root";
  const std::string & pFileName_Event="../../output/PsfStudies.MC.EventDisplay.prod3.root";

   gROOT->SetStyle("Plain");

  //Define fonts:
  gStyle->SetLabelFont(22,"X");
  gStyle->SetStatFont(22);
  gStyle->SetTitleFont(22,"xyz");
  gStyle->SetTitleFont(22,"h");
  gStyle->SetLegendFont(2);
  gStyle->SetOptStat(0);


  //Read in two files (2 reco methods)
  // Impact (1):
  TFile* MCfile1 = new TFile (pFileName_Impact.c_str(),"READ");
  if(!MCfile1)
    {
      std::cerr << "File not found: "<< pFileName_Impact << "Check if it exists." << std::endl;
      return;
    }
  // Define new histogram that points to histogram in file
  TH3F* H_3V_1 = (TH3F*)MCfile1->Get("ARNtelImp");
  if(!H_3V_1) {
    std::cerr << "No suitable histogram found" << std::endl;
    return;
  }

  //EventDisplay (2):
  TFile* MCfile2 = new TFile (pFileName_Event.c_str(),"READ");
  if(!MCfile2)
    {
      std::cerr << "File not found: "<< pFileName_Event << "Check if it exists." << std::endl;
      return;
    }
  // Define new histogram that points to histogram in file
  TH3F* H_3V_2 = (TH3F*)MCfile2->Get("ARNtelImp");
  if(!H_3V_2) {
    std::cerr << "No suitable histogram found" << std::endl;
    return;
  }


  TCanvas* canv = new TCanvas("c1","Cut on # telescopes (minimum) vs Angular Resolution");
  canv->SetFillColor(0);
  canv->Divide(2,2);
  canv->cd(1);

  /* TH1F* hist = MakeAngularPlot(H_3V_1, 0.68, pMaxNtel,pNcut,pMaxImpact, pImpactStep, "Cont68-Impact","Impact r68 - Ncut = 10;  Max. impact par.; r68 "); */
  /* hist->SetMarkerStyle(20); */
  /* hist->SetMarkerColor(2); */
  /* hist->SetMarkerSize(1.); */
  //  hist->GetYaxis()->SetRangeUser(0.,0.006);
  // hist->SetTitleOffset(1.);
  //  hist->GetYaxis()->SetTitleOffset(1.4);
  // hist->Draw("PM");
  TH1F* hista = MakeAngularPlot(H_3V_2, 0.68, pMaxNtel,pNcut,pMaxImpact, pImpactStep,"Cont68","EventDisplay r68 - Ncut = 5;  Max. impact par.; r68  ");
  hista->SetMarkerStyle(20);
  hista->SetMarkerColor(8);
  hista->Draw("SAME PM");
  gPad->BuildLegend();
  

  /* canv->cd(2); */
  /* TH1F* hist2 = MakeAngularPlot(H_3V_1, 0.80, pMaxNtel,pNcut,pMaxImpact, pImpactStep,"Cont80","Impact r80; Max. impact par.; r80"); */
  /* hist2->SetMarkerStyle(20); */
  /* hist2->SetMarkerColor(2); */
  /* hist2->SetMarkerSize(1.); */
  /* //  hist2->GetYaxis()->SetRangeUser(0.,0.01); */
  /* hist2->SetTitleOffset(1.3); */
  /* hist2->Draw("PM"); */
  /* TH1F* hist2a = MakeAngularPlot(H_3V_2, 0.80, pMaxNtel,pNcut,pMaxImpact, pImpactStep,"Cont80 - Event","EventDisplay r80;Max. impact par.; r80 "); */
  /* hist2a->SetMarkerStyle(20); */
  /* hist2a->SetMarkerColor(8); */
  /* //  hist2a->Draw("SAME PM"); */
  /* gPad->BuildLegend(); */


  /* canv->cd(3); */
  /* TH1F* hist3 = MakeAngularPlot(H_3V_1, 0.90, pMaxNtel,pNcut,pMaxImpact, pImpactStep,"Cont90","Impact r90; Max. impact par.; r90"); */
  /* hist3->SetMarkerStyle(20); */
  /* hist3->SetMarkerSize(1.); */
  /* hist3->SetMarkerColor(2); */
  /* //  hist3->GetYaxis()->SetRangeUser(0.,0.03); */
  /* hist3->SetTitleOffset(1.3); */
  /* hist3->Draw("PM"); */
  /* TH1F* hist3a = MakeAngularPlot(H_3V_2, 0.90, pMaxNtel,pNcut,pMaxImpact, pImpactStep,"Cont90 - Event","EventDisplay r90; Max. impact par.; r90 "); */
  /* hist3a->SetMarkerStyle(20); */
  /* hist3a->SetMarkerColor(8); */
  /* // hist3a->Draw("SAME PM"); */
  /* gPad->BuildLegend(); */



} // End of main 




