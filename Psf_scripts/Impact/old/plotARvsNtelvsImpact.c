//==========
//STL (standard C library)
#include <iostream>
//#include <ofstream>
//=============
//ROOT
#include <TLatex.h>
#include <TFile.h>
#include <TH1F.h>
#include <TH1D.h>
//graphic output:
#include <TCanvas.h>
//nice legend:
#include <TLegend.h>
//colors and stuff:
#include <TStyle.h>
#include "TMath.h"
#include "TGraph.h"
#include "TGraphPainter.h"
#include <sstream>
//
//This script visualises the relation of the minimum number of telescopes in a shower versus the loss in percentage per energy bin.
//
// --------------
// Author: Felicia Krauss - 17 October 2016
// --------------
// Modification history:
// 19 Oct 2016: Cosette Molijn (cmolijn): Add other histograms/containment radius
// 01 Nov 2016: cmolijn                 : Replace GetEntries() with Integral()
// 16 Nov 2016: cmolijn                 : Extend script with 2d histogram, add Ncuts and graphs

//void TArray2ascii(TArray* array, std::string filename, TString folder, TString separator);

//==============

// Don't define as cstring here, unclear why
//define default value for variable
//several variable separated by ","
void plotARvsNtelvsImpact(std::string FName="../../output/PsfStudies.MC.Impact.prod3.root")
{
  gROOT->SetStyle("Plain");

  //Define fonts:
  gStyle->SetStatFontSize(0.04); 
  gStyle->SetLabelFont(22,"X");
  gStyle->SetStatFont(22);
  gStyle->SetTitleFont(22,"xyz");
  gStyle->SetTitleFont(22,"h");
  gStyle->SetLegendFont(2);
    
  // * means pointer
  // convert string file name to c string, unclear why not before
  // Alternative: WRITE, RECREATE (=overwrite)

  // -----------------------
  // Read output MC File and define the histogram
  // -----------------------
  TFile* MCfile = new TFile (FName.c_str(),"READ");
    if(!MCfile)
      {
	std::cerr << "File not found: "<< FName << "Check if it exists." << std::endl;
      }
  // Define new histogram that points to histogram in file
  TH3F* H_3par = (TH3F*)MCfile->Get("ARNtelImp");
  H_3par->GetYaxis()->Print("all");

  // First bin of the histogram is the underflow bin, so start with b = 1
  // Get total number of x and y bins (E cuts) and check this with bin centers
  Int_t n_binx   = H_3par->GetXaxis()->GetNbins(); // AR bins
  Int_t n_biny   = H_3par->GetYaxis()->GetNbins(); // Ntel bins
  Int_t n_binz   = H_3par->GetZaxis()->GetNbins(); // Impact parameter bins

  std::cout << "Number of X (#Theta^2) bins   = "<< n_binx  << std::endl;
  std::cout << "Number of Y (Ntel, # ) bins   = "<< n_biny  << std::endl;
  std::cout << "Number of Z (Impact parameter, m) bins= "<< n_binz  << std::endl;

  H_3par->Print("base");

  //Get total Number of Events:
  Int_t tot_entr=H_3par->GetEntries(); // You want to count the overflow bin of course
  std::cout << "Total number of entries in simulation:" << tot_entr << std::endl;


 // -----------------------
  // Here we will project the 3D histogram onto 2D histograms, by slicing along the X and Y axis
  // This way we get a 2D histogram for each energy bin. 
  // -----------------------

  //============================
  // Define your variable parameters here
  //============================

  Int_t b_max = 12; // In the loop we will multiply by 100, so we get to 1000 m, in 10 steps
  Int_t Ntel_max = 3; // Above a cut of 15 telescopes, the AR hardly improves anymore
                       // Minimum Ntel is 3 telescopes, so after the Ntel cut all the 
                       // lists will have (Ntel_max - 2) elements
  // The cut on telescopes will be applied last:
  // First we have a loop over the impact parameter, then we apply a loop (so over this one) over N telescopes, so : loop(Ntel (loop(ImpactPar) ) )

  std::cout << "bmax = " << b_max << std::endl;

  //-----------------------------------
  // LOOPS
  //-----------------------------------


  //Here we  start the Ntel loop:
  
  /* Double_t L_r68[n_binz]; */
  /* Double_t L_r80[n_binz]; */
  /* Double_t L_r90[n_binz]; */
  /* Double_t L_Impcut[n_binz]; */
  /* Double_t L_entries[n_binz]; */
  /* Double_t L_ev_loss[n_binz]; */
  Int_t elz = 0; 

  Int_t Ncut = 2.;
  while(Ncut <= Ntel_max){
    ++Ncut; 
  //  for(Int_t Ncut = 3 ; Ncut <= Ntel_max; ++Ncut){ 

    //============================
    // Define the lists with radii 

    Double_t Array_N [6][10]; // second is length of impact parameter cuts
    //  Double_t *ArrayN;
    // first column: 
    // 0 = Impar cut
    // 1 = r68
    // 2 = r80
    // 3 = r90
    // 4 = event loss
    // 5 = entries

    // 'el'  defines the nth element for the lists 
    Int_t el = 0; 
    std::cout << "Ncut = " << Ncut <<std::endl;


    // Project the 3d histogram onto a 2D
    H_3par->GetYaxis()->SetRange(Ncut,n_biny); // Select the energy bin
    TProfile2D* H_ARvsImp = H_3par->Project3DProfile("zx");    

    H_ARvsImp->Print("base");

    quit();

   //
   //Impact parameter loop
   //
    //    Int_t b = 0.;
    for(Int_t b=1; b <= b_max ; ++b ){
    //    while(b <= b_max){
    //      ++b; 
  
      // This for-loop is for making the impact parameter cuts. For each cut we calculate
      // the radii from the projected 1D histogram
  
      std::cout << "b, impcut max = " << b << "; " << b_max << std::endl; 
      std::cout << "Ncut , Ntel max = " << Ncut << "; " << Ntel_max << std::endl;
   
      Int_t b_bin = b*100;
      
     
      
      // Project the 2d histogram onto a 1D
      TH1D* H_Impcut = H_ARvsImp->ProjectionX("",1 , b_bin);    

      H_Impcut->Print("base");

      // Calculate the number of entries (of the 1D histogram) 
      // and the total entry (wrt initial3D histogram) loss
      Float_t   n_entr  = H_Impcut->Integral(1,n_binx + 1);  
      Float_t   ev_loss = (1-n_entr/tot_entr )*100; // percentage

      // Set the limits for the radii
      double limit68 = 0.68*n_entr;
      double i68=0;
      int j68=1;

      double limit80 = 0.80*n_entr;
      double i80=0;
      int j80=1;

      double limit90 = 0.90*n_entr;
      double i90=0;
      int j90=1;

      // And count entries up and until the set limits
      while (i68 <= limit68)
	{
	  i68+=H_Impcut->GetBinContent(j68);
	  j68++;
	}

      while (i80 <= limit80)
	{
	  i80+=H_Impcut->GetBinContent(j80);
	  j80++;
	}

      while (i90 <= limit90)
	{
	  i90+=H_Impcut->GetBinContent(j90);
	  j90++;
	}

      //Get the containment radii from the histogram
      Float_t r68 = H_Impcut->GetBinCenter(j68);
      Float_t r80 = H_Impcut->GetBinCenter(j80);
      Float_t r90 = H_Impcut->GetBinCenter(j90);


      // Let's print some info:
      std::cout << "Impcut (via bin center) = "<< H_ARvsImp->GetYaxis()->GetBinCenter(b_bin) << std::endl;
      std::cout << "Impcut = " << b_bin << std::endl;
      std::cout << "Number of entries = " << n_entr << " and event loss = "<< ev_loss << "%" << std::endl;
      std::cout << "r68, r80 & r90 = "<< r68 << " ; "<< r80 << " ; " << r90 << std::endl;
      
      // Finally add all the variables to the  array

      Array_N[0][el] = b_bin;
      Array_N[1][el] = r68;
      Array_N[2][el] = r80;
      Array_N[3][el] = r90;
      Array_N[4][el] = ev_loss;
      Array_N[5][el] = n_entr;
      
      ++el; // And +1 the nth element
      H_Impcut->Reset();


    }// end of impact parameter loop

    //  const Int_t n_el = el;
    
    // Here we make some graphs, plots (to save) and save the array to an ascii file

    TCanvas* canv = new TCanvas("c1","Cut on impact parameter (b) vs Angular Resolution");
    canv->SetFillColor(0);
    canv->Divide(2,2);

    canv->cd(1);
    TGraph* gr_r68 = new TGraph(el, Array_N[0] ,  Array_N[1]);
    gr_r68-> SetTitle("Max. impact parameter vs AR r68 (deg^2); Impcut(m); r68 (deg^2)");
    gr_r68->SetMarkerStyle(20);
    gr_r68->SetMarkerColor(2);
    gr_r68->SetMarkerSize(1.);
    gr_r68->Draw("AP");
    gr_r68->GetYaxis()->SetTitleOffset(1.3);

    // Plot Impcut vs r80
    canv->cd(2);
    TGraph* gr_r80 = new TGraph(el,  Array_N[0] ,  Array_N[2]);
    gr_r80-> SetTitle("Max. impact parameter vs AR r80 (deg^2); Impcut; r80 (deg^2)");
    gr_r80->SetMarkerStyle(20);
    gr_r80->SetMarkerColor(2);
    gr_r80->SetMarkerSize(1.);
    gr_r80->Draw("AP");
    gr_r80->GetYaxis()->SetTitleOffset(1.3);
  
    // Plot Impcut vs r90
    canv->cd(3);
    TGraph* gr_r90 = new TGraph(el,  Array_N[0] ,  Array_N[3]);
    gr_r90-> SetTitle("Max. impact parameter vs AR r90 (deg^2); Impcut (m); r90 (deg^2)");
    gr_r90->SetMarkerStyle(20);
    gr_r90->SetMarkerColor(2);
    gr_r90->SetMarkerSize(1.);
    gr_r90->Draw("AP");
    gr_r90->GetYaxis()->SetTitleOffset(1.3);
    
    // Plot Impcut vs number of remaining events
    canv->cd(4);
    TGraph* gr_entries = new TGraph(el,  Array_N[0] ,  Array_N[5]);
    gr_entries->SetTitle("Impcut vs number of entries/events; Impcut (m) ; # entries");
    gr_entries->SetMarkerStyle(20);
    gr_entries->SetMarkerColor(2);
    gr_entries->SetMarkerSize(1.);
    //   gr_entries->Draw("AP");
    gr_entries->GetYaxis()->SetTitleOffset(1.3);

    //Make new canvas, one is not enough
    //    TCanvas* canv2 = new TCanvas("c2","Impact parameter (b) vs Angular Resolution_&d");
    //   canv2->SetFillColor(0);

    TGraph* gr_loss = new TGraph(el,  Array_N[0] ,  Array_N[4]);
    gr_loss->SetTitle("Impcut vs event loss; Impcut (m) ; %");
    gr_loss->SetMarkerStyle(20);
    gr_loss->SetMarkerColor(2);
    gr_loss->SetMarkerSize(1.);
    gr_loss->Draw("AP");
    gr_loss->GetYaxis()->SetTitleOffset(0.8);

    //  ArrayN = Array_N;
    

    //  canv->SaveAs("plots/EventDisplay_"+std::to_string Ncut  +".pdf");
    //   canv2->SaveAs("plots/EventDisplay_Ntel_AR_bin5.pdf");


    //   TArray2ascii(ArrayN, "test_data" ,"plots/","\t");

    ++elz; 
    
    //   if (elz == 3){
    //    break;
    //   }

  }// end of Ntel loop

}
