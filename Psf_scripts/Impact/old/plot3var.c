//==========
//STL (standard C library)
#include <iostream>
//=============
//ROOT
#include <TLatex.h>
#include <TFile.h>
#include <TH1F.h>
#include <TH2F.h>
#include <TH1D.h>
#include <TH3D.h>
#include <TH3F.h>
//graphic output:
#include <TCanvas.h>
//nice legend:
#include <TLegend.h>
//colors and stuff:
#include <TStyle.h>
#include "TMath.h"
#include "TROOT.h"
#include "TGraph.h"
#include "TGraphPainter.h"
#include <sstream>
//
//This script visualises the relation of the number of telescopes in a shower 
// versus the angular resolution.
//
// --------------
// Author: Felicia Krauss - 17 October 2016
// --------------
// Modification history:
// 19 Oct 2016: Cosette Molijn (cmolijn): Add other histograms/containment radius
// 01 Nov 2016: cmolijn                 : Replace GetEntries() with Integral()
// 16 Nov 2016: cmolijn                 : Extend script with 2d histogram, add Ncuts and graphs

Double_t GetContainmentBinCenter(const TH1D* pInput, Double_t pRatio) {
  Double_t limit = pInput->Integral(0, pInput->GetNbinsX() + 1) * pRatio;
  Double_t sum = 0;
  for(Int_t bin = 1, end = pInput->GetNbinsX(); bin<end;++bin ) {
    sum += pInput->GetBinContent(bin);
    if(sum  >= limit)
      return pInput->GetXaxis()->GetBinCenter(bin);
  }
  return pInput->GetXaxis()->GetXmax();
}


void plot3var(Double_t En_min=0.0, Double_t En_max=300, Double_t pMaxImpact=2000, Double_t pImpactStep=50){


  gROOT->SetStyle("Plain");

  //Define fonts:
  gStyle->SetLabelFont(22,"X");
  gStyle->SetStatFont(22);
  gStyle->SetTitleFont(22,"xyz");
  gStyle->SetTitleFont(22,"h");
  gStyle->SetLegendFont(2);
  gStyle->SetOptStat(1000000001);

  
  // Declare 3D hist

  TH3D* H_3var = new TH3D( "H_3var", " 3variables;  #Theta^2 ; Max. Impact par ; Min. Ntel ", 
			  1000, 0., 0.1, 2000 ,0. , 2000.0 , 20, 0., 20.0 ) ;   
  
  TFile * inputFile = new TFile("../../output/PsfStudies.MC.Impact.prod3.root", "READ");
  if(!inputFile || inputFile->IsZombie()) return;
    TTree * tree = dynamic_cast<TTree *>(inputFile->Get("tree"));
    if(!tree) return;

     for(Int_t Ncut = 1, Nend=5; Ncut<=Nend; ++Ncut){
	for(Double_t Impcut = 0, ImpMax = pMaxImpact; Impcut<=ImpMax; Impcut += pImpactStep ) {

	  Double_t ARsq = 0;
	  Double_t trueEn = 0;
	  Double_t trueImp = 0;
	  Int_t Ntelshower = 0;
	  //again, pointer to the variable needed
	  tree->SetBranchAddress("ThetaSq", &ARsq);
	  tree->SetBranchAddress("TrueEnergy", &trueEn);
	  tree->SetBranchAddress("TrueImpact", &trueImp);
	  tree->SetBranchAddress("Ntelshower", &Ntelshower);

      //   for(Int_t Ncut = 1, Nend=20; Ncut<=Nend; ++Ncut){
      //	for(Double_t Impcut = 0, ImpMax = pMaxImpact; Impcut<=ImpMax; Impcut += pImpactStep ) {
	  
	  TH1D* hAR = new TH1D("hAR", "hAR; #Theta^2; # events", 50000, 0, 0.5);
	  
	  for(Int_t entry = 0, end = tree->GetEntries(); entry<end; ++entry ) {
	    tree->GetEntry(entry);
	    if( trueEn < En_min)
	      continue; 
	    if(trueEn > En_max)
	      continue;
	    if(Ntelshower<Ncut)
	      continue;
	    if(trueImp>Impcut)
	      continue;
	    
	    hAR->Fill(ARsq);	 
   
	  }
	  // hAR->Print("base");
	  Double_t r68 = GetContainmentBinCenter(hAR, 0.68);
	 
	  //  cout<< "Ncut; Impcut; r68 = " << Ncut << "; " << Impcut << "; " << r68 << std::endl;
	  H_3var->Fill(r68, Impcut, Ncut);

	  hAR->Clear(); 
	  //  std::cout<< "end of cycle Impcut = " << Impcut << std::endl; 
	}	 
     }

     H_3var->Print("base");
     //  H_3var->Draw("GLISO");
     Int_t r68_min  = H_3var->GetMinimumBin();
     // Double_t ntel_bin
     std::cout << "minimum bin = " <<  r68_min << std::endl;
     Double_t r68min=0; 
     Double_t Impcut_cut=0;
     Int_t Ntel_cut=0;
     
     //  r68min, Impcut_cut, Ntel_cut = H_3var->GetBinXYZ(r68_min, r68min, Impcut_cut, Ntel_cut);
     //  std::cout << "cuts are: " << r68min << 

  // ============= Construct canvas =============
  // ============================================
  /* TCanvas* canv = new TCanvas("c1","Cut on max. Impact parameter vs Angular Resolution"); */
  /* canv->SetFillColor(0); */

  /* hist->SetMarkerStyle(20); */
  /* hist->SetMarkerColor(2); */
  /* //  hist->SetMarkerSize(1.2); */
  /* hist->GetXaxis()->SetRangeUser(0,1500); */
  /* hist->GetYaxis()->SetTitleOffset(1.3); */
  /* hist->Draw("PM"); */




  
      // canv->SaveAs("plots_Impact/ImpvsAR.pdf");


} // End of main 




