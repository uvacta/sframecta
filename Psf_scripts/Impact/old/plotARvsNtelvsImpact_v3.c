//==========
//STL (standard C library)
#include <iostream>
//#include <ofstream>
//=============
//ROOT
#include <TLatex.h>
#include <TFile.h>
#include <TH1F.h>
#include <TH2F.h>
#include <TH1D.h>
//graphic output:
#include <TCanvas.h>
//nice legend:
#include <TLegend.h>
//colors and stuff:
#include <TStyle.h>
#include "TMath.h"
#include "TROOT.h"
#include "TGraph.h"
#include "TGraphPainter.h"
#include <sstream>
//
//This script visualises the relation of the number of telescopes in a shower 
// versus the angular resolution.
//
// --------------
// Author: Felicia Krauss - 17 October 2016
// --------------
// Modification history:
// 19 Oct 2016: Cosette Molijn (cmolijn): Add other histograms/containment radius
// 01 Nov 2016: cmolijn                 : Replace GetEntries() with Integral()
// 16 Nov 2016: cmolijn                 : Extend script with 2d histogram, add Ncuts and graphs

Double_t GetContainmentBinCenter(const TH1D* pInput, Double_t pRatio) {
  Double_t limit = pInput->Integral(0, pInput->GetNbinsX() + 1) * pRatio;
  Double_t sum = 0;
  for(Int_t bin = 0, end = pInput->GetNbinsX(); bin<end;++bin ) {
    sum += pInput->GetBinContent(bin);
    if(sum  >= limit)
      return pInput->GetXaxis()->GetBinCenter(bin);
  }
  return pInput->GetXaxis()->GetXmax();
}


TH1F * Mkplot_Ntel_Impact(const TH3F* pInput, Double_t pRatio, Int_t pMaxNtel, Int_t pMinNtel, Double_t pMaxImpact, Double_t pImpactStep, const std::string & pName, const std::string & pTitle) {

  std::cout << "Ntel_impact =========== " << std::endl;
  pInput->Print("base");
  
  //Get 2D histogram - Ntel cut
 
  pInput->GetYaxis()->SetRange(pMinNtel,pInput->GetNbinsY()+1);
  const TProfile2D* projection2d = pInput->Project3DProfile("zx UF OF"); //Number of entries is 'estimated', see
  //decription of project3DProfile on internet
  projection2d->Print("base");
  
  //Get 1D histogram - Impact parameter loop
  TH1F * hist = new TH1F(pName.c_str(), pTitle.c_str(), pMaxImpact/pImpactStep, 0, pMaxImpact);
  for(Int_t bin = 1, end =  pMaxImpact/pImpactStep; bin<=end; ++bin) {

     const TH1D* projection = projection2d->ProjectionX("proc", 0, projection2d->GetYaxis()->FindBin(bin*pImpactStep));    
     hist->SetBinContent(bin, GetContainmentBinCenter(projection, pRatio));

     delete projection;

   }// End of impact paramaeter loop
  hist->Print("base");
  return hist;
}


TH1F * Mkplot_Impact_Ntel(const TH3F* pInput, Double_t pRatio, Int_t pMaxNtel, Int_t pMinNtel, Double_t pMaxImpact, Double_t pImpactStep, const std::string & pName, const std::string & pTitle) {

  pInput->Print("base");

  //Get 2D histogram - Impact parameter cut
  pInput->GetZaxis()->SetRange(0,pInput->GetZaxis()->FindBin(pMaxImpact));
  const TProfile2D* projection2d = pInput->Project3DProfile("yx UF OF"); //Number of entries is 'estimated', see
  //decription of project3DProfile on internet
  projection2d->Print("base");
  
  //Get 1D histogram - Ntel loop
  TH1F * hist = new TH1F(pName.c_str(), pTitle.c_str(), pMaxNtel, 0, pMaxNtel);
  for(Int_t bin = pMinNtel, end =  pMaxNtel; bin<=end; ++bin) {

    const TH1D* projection = projection2d->ProjectionX("proc", projection2d->GetYaxis()->FindBin(bin),pMaxNtel+1);    
     hist->SetBinContent(bin, GetContainmentBinCenter(projection, pRatio));

     delete projection;

   }// End of Ntel loop
  hist->Print("base");
  
  
  return hist;
}



void plotARvsNtelvsImpact_v3(Int_t pMinNtel=5, Int_t pMaxNtel=55, Double_t pImpactStep=100, Double_t pMaxImpact=2000., Double_t En_min=200, Double_t En_max=210.){

  // Declare 3D hist
  TH3F* H_3var = new TH3F( "3var", ";#Theta^2; Ntel; Max. impact par", 
			   1000, 0., 0.5, pMaxNtel, 0, pMaxNtel, pMaxImpact ,0. , pMaxImpact  ) ;
  
  // void Loop() {
    TFile * inputFile = new TFile("../../output/PsfStudies.MC.Impact.prod3.root", "READ");
    if(!inputFile || inputFile->IsZombie()) return;
      TTree * tree = dynamic_cast<TTree *>(inputFile->Get("tree"));
      if(!tree) return;
        Double_t ARsq = 0;
        Double_t trueEn = 0;
        Double_t trueImp = 0;
        Int_t Ntelshower = 0;
        //again, pointer to the variable needed
	tree->SetBranchAddress("ThetaSq", &ARsq);
	tree->SetBranchAddress("TrueEnergy", &trueEn);
	tree->SetBranchAddress("TrueImpact", &trueImp);
	tree->SetBranchAddress("Ntelshower", &Ntelshower);

	//Loop over the tree with the set cuts on the entries
	for(Int_t entry = 0, end = tree->GetEntries(); entry<end; ++entry ) {
	  tree->GetEntry(entry);
	  
	  if ( trueEn < En_min) // (trueEn >= En_max))// && ( Ntelshower < pMinNtel) )
	    continue; //we cut on the energy here
	  if (trueEn > En_max)
	    continue;
	  
	  //This is only executed if the energy is above the min.cut and under the max. cut
	  //=============== Do fancy stuff here =========================
	  //
	  //Fill the 3D histogram:
	  H_3var->Fill(ARsq, Ntelshower, trueImp);	
	}
        std::cout << " # entries = " << H_3var->GetEntries() << std::endl;
	H_3var->Print("base"); 
	
	


  /* // ============= Construct canvas ============= */
  /* // ============================================ */
  /* TCanvas* canv = new TCanvas("c1","Cut on # telescopes (minimum) vs Angular Resolution"); */
  /* canv->SetFillColor(0); */
  /* canv->Divide(2,2); */
  /* canv->cd(1); */


  /* TH1F* hista = Mkplot_Impact_Ntel(H_3var, 0.68, pMaxNtel, pMinNtel, pMaxImpact, pImpactStep, */
  /* 				"Cont68_a","Impact r68; Ntel cut.; r68  "); */
  /* hista->SetMarkerStyle(20); */
  /* hista->SetMarkerColor(8); */
  /* hista->Draw("PM"); */
  /* hista->GetXaxis()->SetRangeUser(0,15); */
  /* //  gPad->BuildLegend(); */

  /* canv->cd(2); */
  /* TH1F* histb = Mkplot_Ntel_Impact(H_3var, 0.68, pMaxNtel, pMinNtel, pMaxImpact, pImpactStep, */
  /* 				"Cont68_b","Impact r68;  Max. impact par.; r68  "); */
  /* histb->SetMarkerStyle(20); */
  /* histb->SetMarkerColor(8); */
  /* histb->Draw("PM"); */
  /* //  histb->GetXaxis()SetRangeUser(0,15); */


} // End of main 




