
//==========
//STL (standard C library)
#include <iostream>
//#include <ofstream>
//=============
//ROOT
#include <TLatex.h>
#include <TFile.h>
#include <TH1F.h>
#include <TH2F.h>
#include <TH1D.h>
//graphic output:
#include <TCanvas.h>
//nice legend:
#include <TLegend.h>
//colors and stuff:
#include <TStyle.h>

#include "TMath.h"
#include "TROOT.h"
#include "TGraph.h"
#include "TGraphPainter.h"
#include <sstream>
#include <string>
#include "TRandom.h"
//
//
//
// --------------
// Author: Felicia Krauss - 17 October 2016C
// --------------
// Modification history:
// 19 Oct 2016: Cosette Molijn (cmolijn): Add other histograms/containment radius
// 01 Nov 2016: cmolijn                 : Replace GetEntries() with Integral()
// 16 Nov 2016: cmolijn                 : Extend script with 2d histogram, add Ncuts and graphs
// --------------------
// February 2017: cmolijn
// Mei 2017: cmolijn --                 : From ARsq histogram, fit function, from function, calculate bin
//                                        content of AR histogram, and fit a AR function





void FitGaus04(Double_t En_min=0.01, Double_t En_max=300){


  gROOT->SetStyle("Plain");
  gStyle->SetOptStat(1000000001);
  
  // Declare 1D hist
  // =======================
  TH1F* theta_hist = new TH1F( "Impact Method", "AR derived from thetaSq in degrees^2 ;#Theta; events", 
			       500, 0., 0.5) ;   
 TH1F* theta_hist_direct = new TH1F( "Impact Method2", "AR directly from TTree (degrees) ;#Theta; events", 
			       500, 0., 0.5) ;  
TH1F* thetaSq_hist = new TH1F( "thetaSq_hist", "thetaSq_hist;#Theta^2; events", 10000, 0., 0.1) ;


  TFile * inputFile = new TFile("../../output/PsfStudies.MC.Impact.prod3.root", "READ");
  if(!inputFile || inputFile->IsZombie()) return;
    TTree * tree = dynamic_cast<TTree *>(inputFile->Get("tree"));
    if(!tree) return;
      Double_t ARsq = 0;
      Double_t theta = 0;
      Double_t trueEn = 0;
      Double_t trueImp = 0;
      Int_t Ntelshower = 0;
      
      //again, pointer to the variable needed
      tree->SetBranchAddress("ThetaSq", &ARsq);
      tree->SetBranchAddress("Theta", &theta);
      tree->SetBranchAddress("TrueEnergy", &trueEn);
      tree->SetBranchAddress("TrueImpact", &trueImp);
      tree->SetBranchAddress("Ntelshower", &Ntelshower);

	
      //Loop over the tree with the set cuts on the entries: 
      // ======= Energy =======
      for(Int_t entry = 0, end = tree->GetEntries(); entry<end; ++entry ) {
        tree->GetEntry(entry);
	if( trueEn < En_min)
          continue; //we cut on the energy here
        if(trueEn > En_max)
          continue;
	if(trueImp > 700)
	  continue;
	if(Ntelshower < 2)
	  continue;
	
	Double_t AR = pow(ARsq, 0.5);
        theta_hist->Fill(AR);	
	theta_hist_direct->Fill(theta);
	thetaSq_hist->Fill(ARsq);
      }

      inputFile->Close();
      
      std::cout << " # entries AR = " << theta_hist->GetEntries() << std::endl;
      std::cout << " # entries theta = " << theta_hist_direct->GetEntries() << std::endl;  
      std::cout << " # entries ARsq = " << thetaSq_hist->GetEntries() << std::endl;

  //=========================================
  // Now let's plot the two AR histograms: one from theta = sqrt(thetaSq) and one from directly theta
  //=========================================


  // ============= Construct canvas =============
  // ============================================
   TCanvas* canv = new TCanvas("c1","Impact Method - PSF");
   //  canv->SetFillColor(0);
   canv->Divide(1,2);
   canv->cd(1);
 
   theta_hist->SetLineWidth(1);
   // theta_hist->GetXaxis()->SetRangeUser(0,0.004);
   theta_hist->GetYaxis()->SetTitleOffset(1.3);
   theta_hist->SetFillColor(3);
   theta_hist->Draw("");

   theta_hist_direct->SetLineWidth(2);
   theta_hist_direct->SetLineColor(4);
   theta_hist_direct->Draw("Same");


   // now first go from events to events/deg^2 (1/pi*(r_out^2 - r_in^2))
   canv->cd(2);
   TH1F* theta_perdeg2 = new TH1F("", "Theta versus events/deg^2; #Theta; Events/deg^2", 500, 0, 0.5);
      
   for(Int_t bin = 1, end = theta_hist->GetXaxis()->GetNbins(); bin<=end; ++bin){
        
     Double_t rLow = theta_hist->GetXaxis()->GetBinLowEdge(bin);
     Double_t rUp  = rLow + theta_hist->GetXaxis()->GetBinWidth(bin);
     Double_t pi   = 3.1415926;
     Double_t ev   = theta_hist->GetBinContent(bin);

     Double_t Ev_deg2 = ev /( pi*( pow(rUp,2) - pow(rLow,2) ) );
     theta_perdeg2->SetBinContent(bin, Ev_deg2);
     //      theta->SetBinContent(theta->GetXaxis()->FindBin(Ev_deg2),Ev_deg2);
     }
     theta_perdeg2->Draw("hist");

     //=============
     // Now let's turn to our former method: start with ThetaSq, fit a function, 
     // fill a new theta (in deg) histogram, according to that function. What I expect is that this will not
     // return the same histogram as the above ones: here we do not take into account the unit 
     // (that is, number of events per square degree), but we only look at the number of events that have
     // entered the bins. The bin widths are the same, but not anymore if we translate this to a 2D PSF: 
     // this means that the bin width is a radius^2 that changes...
     //=============












} // End of main 




