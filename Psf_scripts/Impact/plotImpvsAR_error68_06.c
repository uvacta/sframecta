//==========
//STL (standard C library)
#include <iostream>
//#include <ofstream>
//=============
//ROOT
#include <TLatex.h>
#include <TFile.h>
#include <TH1F.h>
#include <TH2F.h>
#include <TH1D.h>
//graphic output:
#include <TCanvas.h>
//nice legend:
#include <TLegend.h>
//colors and stuff:
#include <TStyle.h>
#include "TMath.h"
#include "TROOT.h"
#include "TGraph.h"
#include "TGraphPainter.h"
#include <sstream>
#include <string>
#include "TRandom.h"
//
// This script calculates r68 and corresponding event loss veresus impact parameter, for a given Ntel cut. 
//
// --------------
// Author: Felicia Krauss - 17 October 2016
// --------------
// Modification history:
// 19 Oct 2016: Cosette Molijn (cmolijn): Add other histograms/containment radius
// 01 Nov 2016: cmolijn                 : Replace GetEntries() with Integral()
// 16 Nov 2016: cmolijn                 : Extend script with 2d histogram, add Ncuts and graphs
// --------------------
// February 2017: cmolijn
// June 2017: cmolijn:


Double_t GetContainmentBinCenter(const TH1D* pInput, Double_t pRatio) {
  // Calculate the r68 (or other radius) of a th1 AR histogram

  Double_t limit = pInput->Integral(0, pInput->GetNbinsX() + 1) * pRatio;
  Double_t sum = 0;
  for(Int_t bin = 1, end = pInput->GetNbinsX(); bin<end;++bin ) {
    sum += pInput->GetBinContent(bin);
    if(sum  >= limit)
      return pInput->GetXaxis()->GetBinCenter(bin);
  }
  return pInput->GetXaxis()->GetXmax();
}


Double_t rad_rndm(const TH1D* pInput1, Double_t pRatio ){

  //Here we take a AR TH1D, draw random gaussian sqrt(Ni) as new delta N for each bin
  //and calculate a new r68 

  TH1D* hist1 = new TH1D("hist1","",pInput1->GetNbinsX(), pInput1->GetXaxis()->GetXmin(), pInput1->GetXaxis()->GetXmax());
  Int_t bin = 1;
  while(bin <= pInput1->GetNbinsX() ){

    //first error propagation: dn/da

        
    Double_t rLow = hist1->GetXaxis()->GetBinLowEdge(bin);
    Double_t rUp  = rLow + hist1->GetXaxis()->GetBinWidth(bin);
    Double_t pi   = 3.1415926;
    Double_t area_deg2 = pi*( pow(rUp,2) - pow(rLow,2) ) ;

    Double_t Ni = pInput1->GetBinContent(bin);
    Double_t sigma = pow(Ni, 0.5)/area_deg2;    
    Double_t delt_Ni = gRandom->Gaus(0.,sigma); 

    hist1->SetBinContent(bin, pInput1->GetBinContent(bin) + delt_Ni); 
    
    ++bin;
  } 

  // Now calculate the r68 for this new AR histogram  
  Double_t radi = GetContainmentBinCenter(hist1, pRatio);
  // std::cout << "new r68 = " << radi << std::endl;

  delete hist1;
  
  return radi;
}



Double_t GetSigmaRad(TH1D* phist_rad){
  // Calculate the sigma of a gaussian histogram

  phist_rad->Fit("gaus");
  TF1 *fitg = (TF1*)phist_rad->GetFunction("gaus");

  return fitg->GetParameter(2);
}




Double_t r68_ErrorLoop(const TH1D* pInput1, Double_t pRatio){
  //Here we read in the TH1D AR projection, and generate a r68 histogram.
 
  Int_t num = 10000.;  // how many times do we want to loop?
  TH1D* histrad = new TH1D("histrad","histrad",400, 0, 0.1); // r68 histogram: same binning as original AR hist?

  Int_t i = 0;
  while(i < num){    
    Double_t rad = rad_rndm(pInput1, pRatio);
    histrad->Fill(rad); // this is the r68 histogram;
    ++i;
  }

  // histrad->Draw("hist");
  // Now we want to find the uncertainty on the r68
  Double_t sigma = GetSigmaRad(histrad);

  delete histrad;

  return sigma; 
}


TH1D * todeg2(const TH1D* project){

    // The projection is a histogram with theta vs events; the new histogram is theta vs events/deg^2
    TH1D* AR_hist = new TH1D("AR_hist", "AR_hist; theta; num/arcmin^2", project->GetXaxis()->GetNbins(), project->GetXaxis()->GetXmin(), project->GetXaxis()->GetXmax()); 

    //Let's convert to events/deg^2 (the 'real' PSF)
    for(Int_t bin = 1, end = project->GetXaxis()->GetNbins(); bin<=end; ++bin){
        
        Double_t rLow = project->GetXaxis()->GetBinLowEdge(bin);
        Double_t rUp  = rLow + project->GetXaxis()->GetBinWidth(bin);
        Double_t pi   = 3.1415926;
        Double_t ev   = project->GetBinContent(bin);

        Double_t Ev_deg2 = ev /( pi*( pow(rUp,2) - pow(rLow,2) ) );
	Double_t Ev_arcmin2 = Ev_deg2/3600; // convert to ev/arcmin^2
        AR_hist->SetBinContent(bin, Ev_deg2);
       
     }
    return AR_hist;
}


TH1F * Mkplot_Imp(const TH2F* pInput, Double_t pRatio, Double_t pMaxImpact, Double_t pImpactStep, 
		     const std::string & pName, const std::string & pTitle) {
  // Here we make a histogram of the impact parameter versus containment radius (r68), with error bars

  std::cout << "========== Impact method =========== " << std::endl;
  
  // Make the final 1D histogram
  TH1F * hist = new TH1F(pName.c_str(), pTitle.c_str(), pMaxImpact/pImpactStep, 0, pMaxImpact);
  
  // Start Impact parameter loop
  for(Int_t bin = 1, end =  pMaxImpact/pImpactStep; bin<=end; ++bin) {
    
    // Make a 1D histogram from the 2D (select a slice by cut on the impact parameter)
    const TH1D* projection = pInput->ProjectionX("proc", 0, pInput->GetYaxis()->FindBin(bin*pImpactStep)); 

    TH1D* AR_hist = todeg2(projection);

     // Now we calculate the r68:
     hist->SetBinContent(bin, GetContainmentBinCenter(AR_hist, pRatio));
     
     //print some things
     std::cout << "r68 for this one = " << GetContainmentBinCenter(AR_hist, pRatio) << std::endl;
     
     // Get errors!
     Double_t uncert = r68_ErrorLoop(AR_hist, pRatio);
     std::cout << "error = " << uncert << std::endl;
     std::cout << "====== End =====" << std::endl;
     hist->SetBinError(bin, uncert);

     delete projection;
     delete AR_hist;

   }// End of impact paramaeter loop
  return hist;
}



TH1F * Mkplot_Imp_loss(const TH2F* pInput, Double_t pMaxImpact, Double_t total_entries,
                  Double_t pImpactStep, const std::string & pName, const std::string & pTitle) {

  // (---- To calculate the total number of entries in the input histogram (but then density style), 
  // we have to convert that also to events/deg2 ----)

  //  const TH1D* sliced = pInput->ProjectionX("2dsliced", 0, pInput->GetYaxis()->GetNbins());
  //  TH1D* sliced_deg2 = todeg2(sliced);
  //  Double_t tot_entries = sliced_deg2->Integral();


 // Make the final 1D histogram
  TH1F * hist = new TH1F(pName.c_str(), pTitle.c_str(), pMaxImpact/pImpactStep, 0, pMaxImpact);
  
  // Start Impact parameter loop
  for(Int_t bin = 1, end =  pMaxImpact/pImpactStep; bin<=end; ++bin) {
    
    // Make a 1D histogram from the 2D (select a slice by cut on the impact parameter)
    const TH1D* projection = pInput->ProjectionX("proc", 0, pInput->GetYaxis()->FindBin(bin*pImpactStep));
    
    // The projection is a histogram with theta vs events; the new histogram is theta vs events/deg^2
    TH1D* AR_hist1 = todeg2(projection);

    // Now we count and calculate the percentage loss
    Double_t entries = AR_hist1->Integral();

    Double_t loss = (1 - (entries/total_entries)) * 100;
    std::cout << "entries = " << entries <<  std::endl;
    std::cout << "entries tot = " << total_entries << std::endl;   
    std::cout << "loss = " << loss << "%" << std::endl;
    hist->SetBinContent(bin,loss );

    delete projection;
    delete  AR_hist1;

   }// End of impact paramaeter loop
  return hist;
}

//============

//Function to export histogram to ASCII File to plot with other programs


void TH1F2ascii(TH1F* hist,std::string filename, TString folder, TString separator)
{
  /**
   * \brief Export Single Histogram into ASCII file
   */
  Int_t i,j;
  Double_t xcenter, xwidth;
  Bool_t success=kFALSE;
  filename = folder + hist->GetName() + ".txt";
  ofstream file_out(filename);
  file_out << "# Output " << hist->ClassName() << ": " << hist->GetName() << " (" << hist->GetTitle() << ")\n";
  

  
  if (hist->GetDimension()==1)
    {
      file_out << "# BinCenter" << separator << "Content" << separator << "BinHalfWidth" << separator << "Error\n";
      for (i=1; i<=hist->GetNbinsX(); i++)
	file_out << hist->GetBinCenter(i) << separator << hist->GetBinContent(i) << separator << hist->GetBinWidth(i)/2 << separator << hist->GetBinError(i) << endl;
      if (i>1)
	success=kTRUE;
    }
    file_out.close();
    if (success == kTRUE)
      cout << "*** Histogram " << hist->GetName() << " written to " << filename << endl;
    //return success;

  
}


//===========


void plotImpvsAR_error68_06(Double_t En_min=0.0, Double_t En_max=300., Double_t pMaxImpact=2000, Double_t pImpactStep=50){
  
  // Declare 2D hist
  // =======================
  TH2F* H_2var_Imp = new TH2F( "2var_Ntel", ";#Theta; Max. Impact par", 
			   1000, 0., 0.25, 2000 ,0. , 2000  ) ;   

  TH2F* H_2var_count = new TH2F( "2var_count", ";#Theta; Max. Impact par", 
			   1000, 0., 0.25, 2000 ,0. , 2000  ) ; 

  TFile * inputFile = new TFile("../../output/PsfStudies.MC.Impact.prod3.root", "READ");
  if(!inputFile || inputFile->IsZombie()) return;
    TTree * tree = dynamic_cast<TTree *>(inputFile->Get("tree"));
    if(!tree) return;
      Double_t ARsq = 0;
      Double_t trueEn = 0;
      Double_t trueImp = 0;
      Int_t Ntelshower = 0;
      Double_t AR = 0;
      //again, pointer to the variable needed
      tree->SetBranchAddress("ThetaSq", &ARsq);
      tree->SetBranchAddress("TrueEnergy", &trueEn);
      tree->SetBranchAddress("TrueImpact", &trueImp);
      tree->SetBranchAddress("Ntelshower", &Ntelshower);
      tree->SetBranchAddress("Theta",&AR);
	
      //Loop over the tree with the set cuts on the entries: 
      // ======= Energy =======
      for(Int_t entry = 0, end = tree->GetEntries(); entry<end; ++entry ) {
        tree->GetEntry(entry);
	if( trueEn < En_min)
          continue; //we cut on the energy here
        if(trueEn > En_max)
          continue;

	// here count all the entries within this energy band - no cuts
	H_2var_count->Fill(AR,trueImp);
	
	if(Ntelshower < 2)
	  continue;

        H_2var_Imp->Fill(AR, trueImp);	
      }
      
      // The total number of entries of the Impact reco method
      const TH1D* sliced1 = H_2var_count->ProjectionX("2dsliced", 0, H_2var_count->GetYaxis()->GetNbins());


      TH1D* sliced_deg21 = todeg2(sliced1);      
      Double_t tot_entries = sliced_deg21->Integral();

      std::cout << "entries 2d hist Ncut = " << H_2var_Imp->GetEntries() << std::endl;
      std::cout << "entries 2d hist no cut = " << H_2var_count->GetEntries() << std::endl;
      std::cout << "entries 2d hist no cut (ev/deg^2) = " << tot_entries << std::endl;
      

  // ============= Construct canvas =============
  // ============================================

      // TCanvas* canv = new TCanvas("c1","Cut on max. Impact parameter vs Angular Resolution");
      //  canv->SetFillColor(0);
      // canv->Divide(1,2);
      // canv->cd(1);

  TH1F* hist = Mkplot_Imp(H_2var_Imp, 0.68, pMaxImpact, pImpactStep,"r68_hist",
      "Impact method - Max. Imp. cut vs r68 -5 GeV - 200  TeV - Ncut = 2; Max Imp.(m) ; r68 (deg)");
  TH1F* hist_loss = Mkplot_Imp_loss(H_2var_Imp, pMaxImpact, tot_entries, pImpactStep,"eventloss_hist",
       "Impact method - Max.Imp. cut vs event loss - 5 GeV - 200 TeV - Ncut = 2; Max Imp.(m) ; loss (%)");


  hist->SetMarkerStyle(20);
  hist->SetMarkerColor(2);
  hist->SetMarkerSize(1);
  hist->GetYaxis()->SetTitleOffset(1.3);
  // hist->GetYaxis()->SetRangeUser(0,0.025);
  hist->Draw("X0E1");

  //  canv->cd(2);
  hist_loss->SetMarkerStyle(20);
  hist_loss->SetMarkerColor(2);
  hist_loss->SetMarkerSize(1);
  hist_loss->GetYaxis()->SetTitleOffset(1.3);
  hist_loss->Draw("X0E1");
 
  //  TH1F2ascii(hist,"Imp_r68_hist","6july_100TeV/","\t");
  //  TH1F2ascii(hist_loss,"Imp_r68_hist","6july_100TeV/","\t");
  //  canv->SaveAs("6july_100TeV/ImpvsAR_100TeV_Ncut.pdf");


} // End of main 




