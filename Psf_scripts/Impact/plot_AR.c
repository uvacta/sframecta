//==========
//STL (standard C library)
#include <iostream>
//#include <ofstream>
//=============
//ROOT
#include <TLatex.h>
#include <TFile.h>
#include <TH1F.h>
#include <TH2F.h>
#include <TH1D.h>
//graphic output:
#include <TCanvas.h>
//nice legend:
#include <TLegend.h>
//colors and stuff:
#include <TStyle.h>
#include "TMath.h"
#include "TROOT.h"
#include "TGraph.h"
#include "TGraphPainter.h"
#include <sstream>
//
// This script visualises relation between a minimum cut in number of telescopes,
// and the loss - as a consequence of this cut - of events within each energy band (i.e. NOT 
// relative to total number of events over entire energy range)
//
// --------------
// Author: Felicia Krauss - 17 October 2016
// --------------
// Modification history:
// 19 Oct 2016: Cosette Molijn (cmolijn): Add other histograms/containment radius
// 01 Nov 2016: cmolijn                 : Replace GetEntries() with Integral()
// 16 Nov 2016: cmolijn                 : Extend script with 2d histogram, add Ncuts and graphs
// ......................
// February 2017 cmolijn -
// July 2017 cmolijn




//===========================
//Function to export histogram to ASCII File to plot with other programs


void TH1F2ascii(TH1F* hist,std::string filename, TString folder, TString separator)
{
  /**
   * \brief Export Single Histogram into ASCII file
   */
  Int_t i,j;
  Double_t xcenter, xwidth;
  Bool_t success=kFALSE;
  filename = folder + hist->GetName() + ".txt";
  ofstream file_out(filename);
  file_out << "# Output " << hist->ClassName() << ": " << hist->GetName() << " (" <<
 hist->GetTitle() << ")\n";
  

  
  if (hist->GetDimension()==1)
    {
      file_out << "# BinCenter" << separator << "Content" << separator << "BinHalfWidth" << separator << "Error\n";
      for (i=1; i<=hist->GetNbinsX(); i++)
        file_out << hist->GetBinCenter(i) << separator << hist->GetBinContent(i) << separator << hist->GetBinWidth(i)/2 << separator << hist->GetBinError(i) << endl;
      if (i>1)
        success=kTRUE;
    }
    file_out.close();
    if (success == kTRUE)
      cout << "*** Histogram " << hist->GetName() << " written to " << filename << endl;
    //return success;

  
}
  



void plot_AR(){

  gROOT->SetStyle("Plain");
  gStyle->SetOptStat(1000000001);  

  // Declare 2D hist
  TH1F* H_AR = new TH1F( "H_AR", "theta (deg); # events", 1000 , 0, 0.5   ) ;


  TFile * inputFile = new TFile("../../output/PsfStudies.MC.Impact.prod3.root", "READ");
  if(!inputFile || inputFile->IsZombie()) return;
    TTree * tree = dynamic_cast<TTree *>(inputFile->Get("tree"));
    if(!tree) return;
      Double_t ARsq = 0;
      Double_t trueEn = 0;
      Double_t trueImp = 0;
      Int_t Ntelshower = 0;
      Double_t AR = 0;
      //again, pointer to the variable needed
      tree->SetBranchAddress("ThetaSq", &ARsq);
      tree->SetBranchAddress("TrueEnergy", &trueEn);
      tree->SetBranchAddress("TrueImpact", &trueImp);
      tree->SetBranchAddress("Ntelshower", &Ntelshower);
      tree->SetBranchAddress("Theta",&AR);

 
      //Loop over the tree with the set cuts on the entries: 
      // ======= Energy + Ntel =======
      for(Int_t entry = 0, end = tree->GetEntries(); entry<end; ++entry ) {
        tree->GetEntry(entry);
	if(Ntelshower < 10)
	  continue;

        H_AR->Fill(AR);	  

      }

      H_AR->Draw("hist");

      TH1F2ascii(H_AR,"H_AR","entire_distributions10July/AR_range_Ncut10","\t");


} // End of main 

