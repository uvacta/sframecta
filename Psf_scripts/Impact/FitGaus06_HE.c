
//==========
//STL (standard C library)
#include <iostream>
//#include <ofstream>
//=============
//ROOT
#include <TLatex.h>
#include <TFile.h>
#include <TH1F.h>
#include <TH2F.h>
#include <TH1D.h>
//graphic output:
#include <TCanvas.h>
//nice legend:
#include <TLegend.h>
//colors and stuff:
#include <TStyle.h>
#include "TMath.h"
#include "TROOT.h"
#include "TGraph.h"
#include "TGraphPainter.h"
#include <sstream>
#include <string>
#include "TRandom.h"
//
// This script calculates error bars on r68: just to make plots etc. version 3 will continue this 
//  and apply upper/lower limits, and calculate this for a lot of impcut points or Ntel points e.g.
//
// --------------
// Author: Felicia Krauss - 17 October 2016C
// --------------
// Modification history:
// 19 Oct 2016: Cosette Molijn (cmolijn): Add other histograms/containment radius
// 01 Nov 2016: cmolijn                 : Replace GetEntries() with Integral()
// 16 Nov 2016: cmolijn                 : Extend script with 2d histogram, add Ncuts and graphs
// --------------------
// February 2017: cmolijn
// Mei 2017: cmolijn --                 : From ARsq histogram, fit function, from function, calculate bin
//                                        content of AR histogram, and fit a AR function


Double_t GetContainmentBinCenter(const TH1D* pInput, Double_t pRatio) {
  // Calculate the r68 (or other radius) of a th1 AR histogram

  Double_t limit = pInput->Integral(0, pInput->GetNbinsX() + 1) * pRatio;
  Double_t sum = 0;
  for(Int_t bin = 1, end = pInput->GetNbinsX(); bin<end;++bin ) {
    sum += pInput->GetBinContent(bin);
    if(sum  >= limit)
      return pInput->GetXaxis()->GetBinCenter(bin);
  }
  return pInput->GetXaxis()->GetXmax();
}



TH1D * todeg2(const TH1D* project){
  // convert a theta vs events histogram to a theta vs events/deg^2 histogram
  // =============
    // The projection is a histogram with theta vs events; the new histogram is theta vs events/deg^2
  const std::string & histName = project->GetName();
 
  TH1D* AR_hist = new TH1D(histName.c_str(), "AR_hist; theta; num/deg^2", project->GetXaxis()->GetNbins(), project->GetXaxis()->GetXmin(), project->GetXaxis()->GetXmax()); 

    //Let's convert to events/deg^2 (the 'real' PSF)
    for(Int_t bin = 1, end = project->GetXaxis()->GetNbins(); bin<=end; ++bin){
        
        Double_t rLow = project->GetXaxis()->GetBinLowEdge(bin);
        Double_t rUp  = rLow + project->GetXaxis()->GetBinWidth(bin);
        Double_t pi   = 3.1415926;
        Double_t ev   = project->GetBinContent(bin);

        Double_t Ev_deg2 = ev /( pi*( pow(rUp,2) - pow(rLow,2) ) );
        AR_hist->SetBinContent(bin, Ev_deg2);
       
     }
    return AR_hist;
}


void TH1D2ascii(TH1D* hist,std::string filename, TString folder, TString separator)
{
  /**
   * \brief Export Single Histogram into ASCII file
   */
  Int_t i,j;
  Double_t xcenter, xwidth;
  Bool_t success=kFALSE;
  filename = folder + hist->GetName() + ".txt";
  ofstream file_out(filename);
  file_out << "# Output " << hist->ClassName() << ": " << hist->GetName() << " (" << hist->GetTitle() << ")\n";
  

  
  if (hist->GetDimension()==1)
    {
      file_out << "# BinCenter" << separator << "Content" << separator << "BinHalfWidth" << separator << "Error\n";
      for (i=1; i<=hist->GetNbinsX(); i++)
	file_out << hist->GetBinCenter(i) << separator << hist->GetBinContent(i) << separator << hist->GetBinWidth(i)/2 << separator << hist->GetBinError(i) << endl;
      if (i>1)
	success=kTRUE;
    }
    file_out.close();
    if (success == kTRUE)
      cout << "*** Histogram " << hist->GetName() << " written to " << filename << endl;
    //return success;

  
}


void FitGaus06_HE(Double_t En_min=30.1264, Double_t En_max=100.0, Int_t Ncut = 15, Double_t Impcut=2000.){
  
  // Declare 2D hist
  // =======================
  TH1D* AR_hist = new TH1D( "AR_hist_impcut2000_HE_N15", ";#Theta; Events", 
			   200, 0., 0.25) ;   

  TFile * inputFile = new TFile("../../output/PsfStudies.MC.Impact.prod3.root", "READ");
  if(!inputFile || inputFile->IsZombie()) return;
    TTree * tree = dynamic_cast<TTree *>(inputFile->Get("tree"));
    if(!tree) return;
      Double_t ARsq = 0;
      Double_t trueEn = 0;
      Double_t trueImp = 0;
      Int_t Ntelshower = 0;
      Double_t AR = 0;
      //again, pointer to the variable needed
      tree->SetBranchAddress("ThetaSq", &ARsq);
      tree->SetBranchAddress("TrueEnergy", &trueEn);
      tree->SetBranchAddress("TrueImpact", &trueImp);
      tree->SetBranchAddress("Ntelshower", &Ntelshower);
      tree->SetBranchAddress("Theta",&AR);
	
      //Loop over the tree with the set cuts on the entries: 
      // ======= Energy =======
      for(Int_t entry = 0, end = tree->GetEntries(); entry<end; ++entry ) {
        tree->GetEntry(entry);
	if( trueEn < En_min)
          continue; //we cut on the energy here
        if(trueEn > En_max)
          continue;
	if(Ntelshower < Ncut)
	  continue;
	if(trueImp > Impcut)
	  continue;
        AR_hist->Fill(AR);	
      }

      Double_t r68 =  GetContainmentBinCenter(AR_hist, 0.68);


      // The total number of entries of the Impact reco method
      std::cout << "entries AR hist = " << AR_hist->GetEntries() << std::endl;
      std::cout << "r68 = " << r68 << std::endl; 


      // now let's convert to a theta vs events/deg^2 histogram:
      TH1D* AR_deg2 = todeg2(AR_hist);
      
      AR_deg2->Draw("hist");

      //=============== Make the fit =================
      TF1* f11  = new TF1("f11","[0]*exp(-(pow(x-[1],2)/[2]))", 0.0,0.01);
      TF1* f12  = new TF1("f12","[0]*exp(-(pow(x-[1],2))/[2])", 0.01,0.06);
      TF1* f13  = new TF1("f13","[0]*exp(-(pow(x-[1],2))/[2])", 0.025,0.06);
      TF1* total2 = new TF1("total2","[0]*exp(-(pow(x-[1],2))/[2]) + [3]*exp(-(pow(x-[4],2))/[5])",0,0.06);

      f11->SetParameters(2000000,0.0,0.005);
      AR_deg2->Fit(f11, "R M");
      f12->SetParameters(160000,0.01,0.01);
      AR_deg2->Fit(f12, "R M");
      //  f13->SetParameters(200000,0.03,0.05);
      // AR_deg2->Fit(f13, "R M");

      Double_t par2[9];
      f11->GetParameters(&par2[0]);
      f11->SetLineColor(2);
      f11->Draw("Same");
      f12->GetParameters(&par2[3]);
      f12->SetLineColor(1);
      f12->Draw("Same");
      //  f13->GetParameters(&par2[6]);
      // f13->SetLineColor(4);
      // f13->Draw("Same");

      total2->SetParameters(par2);
      AR_deg2->Fit(total2, "R M");
      total2->SetLineColor(8);
      total2->Draw("Same");

 
      TH1D2ascii(AR_deg2,"AR_hist_HE_N15","10sep/","\t");

} // End of main 




