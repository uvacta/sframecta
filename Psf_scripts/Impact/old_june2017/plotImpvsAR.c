//==========
//STL (standard C library)
#include <iostream>
//#include <ofstream>
//=============
//ROOT
#include <TLatex.h>
#include <TFile.h>
#include <TH1F.h>
#include <TH2F.h>
#include <TH1D.h>
//graphic output:
#include <TCanvas.h>
//nice legend:
#include <TLegend.h>
//colors and stuff:
#include <TStyle.h>
#include "TMath.h"
#include "TROOT.h"
#include "TGraph.h"
#include "TGraphPainter.h"
#include <sstream>
//
// This script visualises 3 containment radii of angular resolution 
// vs the maximum impact parameter, and the event loss for this maximum cut
// on the impact parameter. It is possible to do this for an energy range as well.  
//
// --------------
// Author: Felicia Krauss - 17 October 2016
// --------------
// Modification history:
// 19 Oct 2016: Cosette Molijn (cmolijn): Add other histograms/containment radius
// 01 Nov 2016: cmolijn                 : Replace GetEntries() with Integral()
// 16 Nov 2016: cmolijn                 : Extend script with 2d histogram, add Ncuts and graphs
// --------------------
// February 2017: cmolijn

Double_t GetContainmentBinCenter(const TH1D* pInput, Double_t pRatio) {
  Double_t limit = pInput->Integral(0, pInput->GetNbinsX() + 1) * pRatio;
  Double_t sum = 0;
  for(Int_t bin = 1, end = pInput->GetNbinsX(); bin<end;++bin ) {
    sum += pInput->GetBinContent(bin);
    if(sum  >= limit)
      return pInput->GetXaxis()->GetBinCenter(bin);
  }
  return pInput->GetXaxis()->GetXmax();
}


TH1F * Mkplot_Imp(const TH2F* pInput, Double_t pRatio, Double_t pMaxImpact, Double_t pImpactStep, 
		     const std::string & pName, const std::string & pTitle) {

  std::cout << "Impact =========== " << std::endl;
  pInput->Print("base");
  
  //Get 1D histogram - Impact parameter loop
  TH1F * hist = new TH1F(pName.c_str(), pTitle.c_str(), pMaxImpact/pImpactStep, 0, pMaxImpact);
  for(Int_t bin = 1, end =  pMaxImpact/pImpactStep; bin<=end; ++bin) {

     const TH1D* projection = pInput->ProjectionX("proc", 0, pInput->GetYaxis()->FindBin(bin*pImpactStep)); 
     hist->SetBinContent(bin, GetContainmentBinCenter(projection, pRatio));

     delete projection;

   }// End of impact paramaeter loop
  // hist->Print("base");
  return hist;
}


TH1F * Mkplot_Imp_loss(const TH2F* pInput, Double_t tot_entries, Double_t pMaxImpact, 
			  Double_t pImpactStep, const std::string & pName, const std::string & pTitle) {

  //Get 1D histogram - Impact parameter loop
  TH1F * hist = new TH1F(pName.c_str(), pTitle.c_str(), pMaxImpact/pImpactStep, 0, pMaxImpact);
  for(Int_t bin = 1, end =  pMaxImpact/pImpactStep; bin<=end; ++bin) {

     const TH1D* projection = pInput->ProjectionX("proc", 0, pInput->GetYaxis()->FindBin(bin*pImpactStep)); 
     Double_t entries = projection->GetEntries();
     Double_t loss = (1 - (entries/tot_entries)) * 100; 
     hist->SetBinContent(bin,loss );

     delete projection;

   }// End of impact paramaeter loop
  return hist;
}





void plotImpvsAR(Double_t En_min=0.3, Double_t En_max=0.5, Double_t pMaxImpact=300, Double_t pImpactStep=5){


  gROOT->SetStyle("Plain");

  //Define fonts:
  gStyle->SetLabelFont(22,"X");
  gStyle->SetStatFont(22);
  gStyle->SetTitleFont(22,"xyz");
  gStyle->SetTitleFont(22,"h");
  gStyle->SetLegendFont(2);
  gStyle->SetOptStat(1000000001);

  
  // Declare 3D hist

  TH2F* H_2var_Imp = new TH2F( "2var_Ntel", ";#Theta^2; Max. Impact par", 
			   10000, 0., 0.5, 2000 ,0. , 2000  ) ;   


  TFile * inputFile = new TFile("../../output/PsfStudies.MC.Impact.prod3.root", "READ");
  if(!inputFile || inputFile->IsZombie()) return;
    TTree * tree = dynamic_cast<TTree *>(inputFile->Get("tree"));
    if(!tree) return;
      Double_t ARsq = 0;
      Double_t trueEn = 0;
      Double_t trueImp = 0;
      Int_t Ntelshower = 0;
      //again, pointer to the variable needed
      tree->SetBranchAddress("ThetaSq", &ARsq);
      tree->SetBranchAddress("TrueEnergy", &trueEn);
      tree->SetBranchAddress("TrueImpact", &trueImp);
      tree->SetBranchAddress("Ntelshower", &Ntelshower);

	
      //Loop over the tree with the set cuts on the entries: 
      // ======= Energy =======
      for(Int_t entry = 0, end = tree->GetEntries(); entry<end; ++entry ) {
        tree->GetEntry(entry);
	if( trueEn < En_min)
          continue; //we cut on the energy here
        if(trueEn > En_max)
          continue;


        H_2var_Imp->Fill(ARsq, trueImp);	
      }
      std::cout << " # entries = " << H_2var_Imp->GetEntries() << std::endl;
      Double_t entries_tot = H_2var_Imp->GetEntries();  
      // The total number of entries of the Impact reco method

	
  // ============= Construct canvas =============
  // ============================================
  TCanvas* canv = new TCanvas("c1","Cut on max. Impact parameter vs Angular Resolution");
  canv->SetFillColor(0);
  canv->Divide(2,2);
  canv->cd(1);

  TH1F* hist   = Mkplot_Imp(H_2var_Imp, 0.68, pMaxImpact, pImpactStep,
  			    "","Impact method - Max. Imp. cut vs r68 - SST - 0.3-0.5 TeV ; Max Imp.(m) ; r68 (deg^2)  ");
  TH1F* hist80 = Mkplot_Imp(H_2var_Imp, 0.80, pMaxImpact, pImpactStep,
  			    "","Impact method - Max. Imp. cut vs r80  - SST -0.3-0.5 TeV; Max Imp.(m) ; r80 (deg^2)  ");
  TH1F* hist90 = Mkplot_Imp(H_2var_Imp, 0.90, pMaxImpact, pImpactStep,
  			    "","Impact method - Max. Imp. cut vs r90  - SST - 0.3-0.5 TeV ; Max Imp.(m) ; r90 (deg^2)  ");
  
  TH1F* histloss = Mkplot_Imp_loss(H_2var_Imp, entries_tot, pMaxImpact, pImpactStep,
  			    ""," Impact method - Max Imp. cut vs  Event loss - SST - 0.3-0.5 TeV; Max.Impact (m); %  ");


  hist->SetMarkerStyle(20);
  hist->SetMarkerColor(2);
  hist->SetMarkerSize(0.4);
  //  hist->GetXaxis()->SetRangeUser(0,700);
  //  hist->GetYaxis()->SetRangeUser(0.018,0.025);
  hist->GetYaxis()->SetTitleOffset(1.3);
  hist->Draw("PM");

  canv->cd(2);
  hist80->SetMarkerStyle(20);
  hist80->SetMarkerColor(2);
  hist80->SetMarkerSize(0.4);
  //  hist80->GetXaxis()->SetRangeUser(0,1500);
  hist80->GetYaxis()->SetTitleOffset(1.3);
  hist80->Draw("PM");

  canv->cd(3);
  hist90->SetMarkerStyle(20);
  hist90->SetMarkerColor(2);
  hist90->SetMarkerSize(0.4);
  //  hist90->GetXaxis()->SetRangeUser(0,1500);
  hist90->GetYaxis()->SetTitleOffset(1.3);
  hist90->Draw("PM");

  canv->cd(4);
  histloss->SetMarkerStyle(20);
  histloss->SetMarkerColor(2);
  histloss->SetMarkerSize(0.4);
  //  histloss->GetXaxis()->SetRangeUser(0,700);
  histloss->Draw("PM");


  
  canv->SaveAs("plots_Impact/ImpvsAR_SST_0305TeV.pdf");


} // End of main 




