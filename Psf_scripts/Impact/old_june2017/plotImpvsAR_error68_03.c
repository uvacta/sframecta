//==========
//STL (standard C library)
#include <iostream>
//#include <ofstream>
//=============
//ROOT
#include <TLatex.h>
#include <TFile.h>
#include <TH1F.h>
#include <TH2F.h>
#include <TH1D.h>
//graphic output:
#include <TCanvas.h>
//nice legend:
#include <TLegend.h>
//colors and stuff:
#include <TStyle.h>
#include "TMath.h"
#include "TROOT.h"
#include "TGraph.h"
#include "TGraphPainter.h"
#include <sstream>
#include <string>
#include "TRandom.h"
//
// This script calculates error bars on r68: just to make plots etc. version 3 will continue this 
//  and apply upper/lower limits, and calculate this for a lot of impcut points or Ntel points e.g.
//
// --------------
// Author: Felicia Krauss - 17 October 2016
// --------------
// Modification history:
// 19 Oct 2016: Cosette Molijn (cmolijn): Add other histograms/containment radius
// 01 Nov 2016: cmolijn                 : Replace GetEntries() with Integral()
// 16 Nov 2016: cmolijn                 : Extend script with 2d histogram, add Ncuts and graphs
// --------------------
// February 2017: cmolijn

Double_t GetContainmentBinCenter(const TH1D* pInput, Double_t pRatio) {
  Double_t limit = pInput->Integral(0, pInput->GetNbinsX() + 1) * pRatio;
  Double_t sum = 0;
  for(Int_t bin = 1, end = pInput->GetNbinsX(); bin<end;++bin ) {
    sum += pInput->GetBinContent(bin);
    if(sum  >= limit)
      return pInput->GetXaxis()->GetBinCenter(bin);
  }
  return pInput->GetXaxis()->GetXmax();
}


Double_t rad_rndm(const TH1D* pInput1, Double_t pRatio ){

  //Here we take a AR TH1D, draw random gaussian sqrt(Ni) as new delta N for each bin
  //and calculate a new r68 

  TH1D* hist1 = new TH1D("hist1","",pInput1->GetNbinsX(), pInput1->GetXaxis()->GetXmin(), pInput1->GetXaxis()->GetXmax());
  Int_t bin = 1;
  while(bin <= pInput1->GetNbinsX() ){

    Double_t Ni = pInput1->GetBinContent(bin);
    Double_t sigma = pow(Ni, 0.5);    
    Double_t delt_Ni = gRandom->Gaus(0.,sigma); 
   
    hist1->SetBinContent(bin, pInput1->GetBinContent(bin) + delt_Ni); 
    
    ++bin;
  } 
  // Now calculate the r68 for this new AR histogram  
  Double_t radi = GetContainmentBinCenter(hist1, pRatio);
  delete hist1;

  return radi;
}

Double_t GetSigmaRad(TH1D* phist_rad){

  phist_rad->Fit("gaus");
  TF1 *fitg = (TF1*)phist_rad->GetFunction("gaus");

  return fitg->GetParameter(2);
}


Double_t r68_ErrorLoop(const TH1D* pInput1, Double_t pRatio){
  
  //Here we read in the TH1D AR projection, and generate a r68 histogram.
  Int_t num = 10000.;  // how many times do we want to loop?
  TH1D* histrad = new TH1D("histrad","histrad",1000, 0, 0.05); // r68 histogram: same binning as original AR hist

  Int_t i = 0;
  while(i < num){    
    Double_t rad = rad_rndm(pInput1, pRatio);
    histrad->Fill(rad); // this is the r68 histogram;
    ++i;
  }
  // Now we want to find the uncertainty on the r68
  Double_t sigma = GetSigmaRad(histrad);

  delete histrad;

  return sigma; 
}



TH1F * Mkplot_Imp(const TH2F* pInput, Double_t pRatio, Double_t pMaxImpact, Double_t pImpactStep, 
		     const std::string & pName, const std::string & pTitle) {

  std::cout << "Impact =========== " << std::endl;
  pInput->Print("base");
  
  //Get 1D histogram - Impact parameter loop
  TH1F * hist = new TH1F(pName.c_str(), pTitle.c_str(), pMaxImpact/pImpactStep, 0, pMaxImpact);
  for(Int_t bin = 1, end =  pMaxImpact/pImpactStep; bin<=end; ++bin) {

     const TH1D* projection = pInput->ProjectionX("proc", 0, pInput->GetYaxis()->FindBin(bin*pImpactStep)); 
    
     hist->SetBinContent(bin, GetContainmentBinCenter(projection, pRatio));

     std::cout << "impcut = " << pInput->GetYaxis()->FindBin(bin*pImpactStep) << std::endl;
     std::cout << "r68 for this one = " << GetContainmentBinCenter(projection, pRatio) << std::endl;
     std::cout << "num events in this hist = " << projection->GetEntries() << std::endl;
     // Get errors!
    Double_t uncert = r68_ErrorLoop(projection, pRatio);
    std::cout << "error = " << uncert << std::endl;
    hist->SetBinError(bin, uncert);

     delete projection;

   }// End of impact paramaeter loop
  // hist->Print("base");
  return hist;
}

TH1F * Mkplot_Imp_loss(const TH2F* pInput, Double_t tot_entries, Double_t pMaxImpact, 
			  Double_t pImpactStep, const std::string & pName, const std::string & pTitle) {

  //Get 1D histogram - Impact parameter loop
  TH1F * hist = new TH1F(pName.c_str(), pTitle.c_str(), pMaxImpact/pImpactStep, 0, pMaxImpact);
  for(Int_t bin = 1, end =  pMaxImpact/pImpactStep; bin<=end; ++bin) {

     const TH1D* projection = pInput->ProjectionX("proc", 0, pInput->GetYaxis()->FindBin(bin*pImpactStep)); 
     Double_t entries = projection->GetEntries();
     Double_t loss = (1 - (entries/tot_entries)) * 100; 
     std::cout << "loss = " << loss << "%" << std::endl;
     hist->SetBinContent(bin,loss );

     delete projection;

   }// End of impact paramaeter loop
  return hist;
}


void plotImpvsAR_error68_03(Double_t En_min=0.1, Double_t En_max=1.0, Double_t pMaxImpact=800, Double_t pImpactStep=100){


  gROOT->SetStyle("Plain");
  gStyle->SetOptStat(1000000001);
  
  // Declare 2D hist
  // =======================
  TH2F* H_2var_Imp = new TH2F( "2var_Ntel", ";#Theta^2; Max. Impact par", 
			   10000, 0., 0.5, 2000 ,0. , 2000  ) ;   

  TFile * inputFile = new TFile("../../output/PsfStudies.MC.Impact.prod3.root", "READ");
  if(!inputFile || inputFile->IsZombie()) return;
    TTree * tree = dynamic_cast<TTree *>(inputFile->Get("tree"));
    if(!tree) return;
      Double_t ARsq = 0;
      Double_t trueEn = 0;
      Double_t trueImp = 0;
      Int_t Ntelshower = 0;
      //again, pointer to the variable needed
      tree->SetBranchAddress("ThetaSq", &ARsq);
      tree->SetBranchAddress("TrueEnergy", &trueEn);
      tree->SetBranchAddress("TrueImpact", &trueImp);
      tree->SetBranchAddress("Ntelshower", &Ntelshower);
	
      //Loop over the tree with the set cuts on the entries: 
      // ======= Energy =======
      for(Int_t entry = 0, end = tree->GetEntries(); entry<end; ++entry ) {
        tree->GetEntry(entry);
	if( trueEn < En_min)
          continue; //we cut on the energy here
        if(trueEn > En_max)
          continue;
	if(Ntelshower < 5)
	  continue;

        H_2var_Imp->Fill(ARsq, trueImp);	
      }
      std::cout << " # entries = " << H_2var_Imp->GetEntries() << std::endl;
      Double_t entries_tot = H_2var_Imp->GetEntries();  
      
      // The total number of entries of the Impact reco method

  // ============= Construct canvas =============
  // ============================================
  TCanvas* canv = new TCanvas("c1","Cut on max. Impact parameter vs Angular Resolution");
  canv->SetFillColor(0);
  canv->Divide(1,2);
  canv->cd(1);

  TH1F* hist = Mkplot_Imp(H_2var_Imp, 0.68, pMaxImpact, pImpactStep,"",
      "Impact method - Max. Imp. cut vs r68 -0.1-1 TeV - Ncut = 5 ; Max Imp.(m) ; r68 (deg^2)");
  TH1F* hist_loss = Mkplot_Imp_loss(H_2var_Imp, entries_tot, pMaxImpact, pImpactStep,"",
      "Impact method - Max.Imp. cut vs event loss-0.1-1 TeV - Ncut = 15; Max Imp.(m) ; loss (%)");

  hist->SetMarkerStyle(20);
  hist->SetMarkerColor(2);
  hist->SetMarkerSize(1);
  hist->GetYaxis()->SetTitleOffset(1.3);
  hist->Draw("X0E1");

  canv->cd(2);
  hist_loss->SetMarkerStyle(20);
  hist_loss->SetMarkerColor(2);
  hist_loss->SetMarkerSize(1);
  hist_loss->GetYaxis()->SetTitleOffset(1.3);
  hist_loss->Draw("PM");


 
  
  // canv->SaveAs("plots_Impact_LST/ImpvsAR_LST_Ncut5_00201.pdf");


} // End of main 




