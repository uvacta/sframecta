
//==========
//STL (standard C library)
#include <iostream>
//#include <ofstream>
//=============
//ROOT
#include <TLatex.h>
#include <TFile.h>
#include <TH1F.h>
#include <TH2F.h>
#include <TH1D.h>
//graphic output:
#include <TCanvas.h>
//nice legend:
#include <TLegend.h>
//colors and stuff:
#include <TStyle.h>
#include "TMath.h"
#include "TROOT.h"
#include "TGraph.h"
#include "TGraphPainter.h"
#include <sstream>
#include <string>
#include "TRandom.h"
//
// This script calculates error bars on r68: just to make plots etc. version 3 will continue this 
//  and apply upper/lower limits, and calculate this for a lot of impcut points or Ntel points e.g.
//
// --------------
// Author: Felicia Krauss - 17 October 2016
// --------------
// Modification history:
// 19 Oct 2016: Cosette Molijn (cmolijn): Add other histograms/containment radius
// 01 Nov 2016: cmolijn                 : Replace GetEntries() with Integral()
// 16 Nov 2016: cmolijn                 : Extend script with 2d histogram, add Ncuts and graphs
// --------------------
// February 2017: cmolijn





void FitGaus02(Double_t En_min=1, Double_t En_max=5){


  gROOT->SetStyle("Plain");
  gStyle->SetOptStat(1000000001);
  
  // Declare 1D hist
  // =======================
  TH1F* theta_hist = new TH1F( "theta_hist", "PSF Impact method - 1-5 TeV - Ncut = 7;#Theta; events/deg^2", 
			       1000, 0., 0.1) ;   

  TFile * inputFile = new TFile("../../output/PsfStudies.MC.Impact.prod3.root", "READ");
  if(!inputFile || inputFile->IsZombie()) return;
    TTree * tree = dynamic_cast<TTree *>(inputFile->Get("tree"));
    if(!tree) return;
      Double_t ARsq = 0;
      Double_t trueEn = 0;
      Double_t trueImp = 0;
      Int_t Ntelshower = 0;
      //again, pointer to the variable needed
      tree->SetBranchAddress("ThetaSq", &ARsq);
      tree->SetBranchAddress("TrueEnergy", &trueEn);
      tree->SetBranchAddress("TrueImpact", &trueImp);
      tree->SetBranchAddress("Ntelshower", &Ntelshower);
	
      //Loop over the tree with the set cuts on the entries: 
      // ======= Energy =======
      for(Int_t entry = 0, end = tree->GetEntries(); entry<end; ++entry ) {
        tree->GetEntry(entry);
	if( trueEn < En_min)
          continue; //we cut on the energy here
        if(trueEn > En_max)
          continue;
	if(trueImp > 800)
	  continue;
	if(Ntelshower < 7)
	  continue;
	
	Double_t AR = pow(ARsq, 0.5);
        theta_hist->Fill(AR);	
      }

      inputFile->Close();
      
      std::cout << " # entries = " << theta_hist->GetEntries() << std::endl;
      Double_t entries_tot = theta_hist->GetEntries();  
      theta_hist->Draw("Hist");
    
      // now first go from events to events/deg^2 (1/pi*(r_out^2 - r_in^2))

      //  TH1F* theta = new TH1F("", "Theta versus events/deg^2; #Theta; Events/deg^2", 5000, 0, 0.1);
      
      for(Int_t bin = 1, end = theta_hist->GetXaxis()->GetNbins(); bin<=end; ++bin){
	
     	Double_t rLow = theta_hist->GetXaxis()->GetBinLowEdge(bin);
     	Double_t rUp  = rLow + theta_hist->GetXaxis()->GetBinWidth(bin);
     	Double_t pi   = 3.1415926;
     	Double_t ev   = theta_hist->GetBinContent(bin);

     	Double_t Ev_deg2 = ev /( pi*( pow(rUp,2) - pow(rLow,2) ) );
     	theta_hist->SetBinContent(bin, Ev_deg2);
	//	theta->SetBinContent(theta->GetXaxis()->FindBin(Ev_deg2),Ev_deg2);
        }
        theta_hist->Draw("hist");


      //=========================================
      //Now, let's fit a function to the data
      //=========================================
  /*     TF1* f1  = new TF1("f1","[0]*exp(-x/[1])", 0,0.0025); */
  /*     TF1* f2  = new TF1("f2","[0]*exp(-x/[1])", 0.0025,0.0075); */
  /*     TF1* f3  = new TF1("f3","[0]*exp(-x/[1])", 0.0075,0.025); */

  /*     TF1* total = new TF1("mstotal","[0]*exp(-x/[1]) + [2]*exp(-x/[3]) + [4]*exp(-x/[5])",0,0.025); */

  /*     f1->SetParameters(6000,1); */
  /*     f2->SetParameters(1000,1); */
  /*     f3->SetParameters(600,1); */

  /*     theta_hist->Fit(f1, "R"); */
  /*     theta_hist->Fit(f2, "R+"); */
  /*     theta_hist->Fit(f3, "R+"); */

  /*     Double_t par[6]; */

  /*     f1->GetParameters(&par[0]); */
  /*     f2->GetParameters(&par[2]); */
  /*     f3->GetParameters(&par[4]); */

  /*     total->SetParameters(par); */
  /*     theta_hist->Fit(total, "R+"); */


  /* // ============= Construct canvas ============= */
  /* // ============================================ */
  /*  TCanvas* canv = new TCanvas("c1","Impact Method - PSF"); */
  /*  //  canv->SetFillColor(0); */
  /*  //  canv->Divide(1,2); */
  /*  //  canv->cd(1); */
 
  /* theta_hist->SetLineWidth(3); */
  /* // theta_hist->GetXaxis()->SetRangeUser(0,0.05); */
  /* theta_hist->GetYaxis()->SetTitleOffset(1.3); */
  /* theta_hist->Draw("Hist"); */

  /* f1->SetLineColor(2); */
  /* f2->SetLineColor(4); */
  /* f3->SetLineColor(8); */
  /* total->SetLineColor(5); */

  /* f1->Draw("Same"); */
  /* f2->Draw("Same"); */
  /* f3->Draw("Same"); */
  /* total->Draw("Same"); */
  
  /* // canv->SaveAs("plots_Impact_LST/ImpvsAR_LST_Ncut5_00201.pdf"); */








  //================
   /* FILE *fp = fopen("function.txt","w"); */
   /* TFitResultPtr r = theta_hist->Fit(total,"S"); */   
   /* // TMatrixDSym cov = r->GetCovarianceMatrix(); */
   /* const Double_t* pars = r->GetParams(); */
   /* r->Print("V"); */
   /* std:: cout << "N px  =  " << total->GetNpx << std::endl; */
   
   /* total->Print("all"); */

   //  fprintf(fp, "%d", r);
   //  fp->Write();
  
  // fclose(fp);
  //================



} // End of main 




