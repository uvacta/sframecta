//==========
//STL (standard C library)
#include <iostream>
//=============
//ROOT
#include <TLatex.h>
#include <TFile.h>
#include <TH1F.h>
#include <TH2F.h>
#include <TH1D.h>
#include <TH3D.h>
#include <TH3F.h>
#include "TProfile.h"
#include "TTree.h"
//graphic output:
#include <TCanvas.h>
//nice legend:
#include <TLegend.h>
//colors and stuff:
#include <TStyle.h>
#include "TMath.h"
#include "TROOT.h"
#include "TGraph.h"
#include "TGraphPainter.h"
#include <sstream>
//
//This script visualises the relation of the number of telescopes in a shower 
// versus the angular resolution.
//
// --------------
// Author: Felicia Krauss - 17 October 2016
// --------------
// Modification history:
// 19 Oct 2016: Cosette Molijn (cmolijn): Add other histograms/containment radius
// 01 Nov 2016: cmolijn                 : Replace GetEntries() with Integral()
// 16 Nov 2016: cmolijn                 : Extend script with 2d histogram, add Ncuts and graphs

void gen_tree(Double_t En_min=0.02, Double_t En_max=0.1, Double_t Imp_min = 0.,
	      Double_t Imp_max = 300.){


  TFile * inputFile = new TFile("../../output/PsfStudies.MC.Impact.prod3.root", "READ");
  if(!inputFile || inputFile->IsZombie()) return;
    TTree * tree = dynamic_cast<TTree *>(inputFile->Get("tree"));
    if(!tree) return;

    Double_t trueEn_cut = 0;
    Double_t ARsq_cut   = 0;
    Double_t trueImp_cut= 0;
    
    TFile * File_cut = new TFile("3var_LST_Aseries.root", "UPDATE");
    TTree * tree_cut = new TTree("tree_LST_300", "tree_LST_300"); 
    tree_cut->Branch("trueEn_cut", &trueEn_cut);
    tree_cut->Branch("ARsq_cut", &ARsq_cut);
    tree_cut->Branch("trueImp_cut", &trueImp_cut);

    gROOT->cd();

    Double_t trueEn = 0;
    Double_t ARsq   = 0;
    Double_t trueImp= 0; 
    //again, pointer to the variable needed
    tree->SetBranchAddress("ThetaSq", &ARsq);
    tree->SetBranchAddress("TrueEnergy", &trueEn);
    tree->SetBranchAddress("TrueImpact", &trueImp);

    for(Int_t entry = 0, end = tree->GetEntries(); entry<end; ++entry ) {
       tree->GetEntry(entry);
       if( trueEn < En_min)
         continue; 
       if(trueEn > En_max)
         continue;
       if(trueImp < Imp_min)
	 continue;
       if(trueImp > Imp_max)
	 continue;
       if(ARsq > 0.5)
	 continue;
       ARsq_cut    = ARsq;
       trueEn_cut  = trueEn;
       trueImp_cut = trueImp;
       tree_cut->Fill();
    }
    File_cut->Write();
    File_cut->Close();



} // End of main 




