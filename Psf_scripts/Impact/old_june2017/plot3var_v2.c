//==========
//STL (standard C library)
#include <iostream>
//=============
//ROOT
#include <TLatex.h>
#include <TFile.h>
#include <TH1F.h>
#include <TH2F.h>
#include <TH1D.h>
#include <TH3D.h>
#include <TH3F.h>
#include "TProfile.h"
#include "TTree.h"
//graphic output:
#include <TCanvas.h>
//nice legend:
#include <TLegend.h>
//colors and stuff:
#include <TStyle.h>
#include "TMath.h"
#include "TROOT.h"
#include "TGraph.h"
#include "TGraphPainter.h"
#include <sstream>
//
//This script visualises the relation of the number of telescopes in a shower 
// versus the angular resolution.
//
// --------------
// Author: Felicia Krauss - 17 October 2016
// --------------
// Modification history:
// 19 Oct 2016: Cosette Molijn (cmolijn): Add other histograms/containment radius
// 01 Nov 2016: cmolijn                 : Replace GetEntries() with Integral()
// 16 Nov 2016: cmolijn                 : Extend script with 2d histogram, add Ncuts and graphs

Double_t GetContainmentBinCenter(const TH1D* pInput, Double_t pRatio) {
  Double_t limit = pInput->Integral(0, pInput->GetNbinsX() + 1) * pRatio;
  Double_t sum = 0;
  for(Int_t bin = 1, end = pInput->GetNbinsX(); bin<end;++bin ) {
    sum += pInput->GetBinContent(bin);
    if(sum  >= limit)
      return pInput->GetXaxis()->GetBinCenter(bin);
  }
  return pInput->GetXaxis()->GetXmax();
}


void plot3var_v2(Double_t En_min=0.02, Double_t En_max=0.1, Double_t pMaxImpact=800, Double_t pImpactStep=50){

  gROOT->SetStyle("Plain");
  
  TFile * inputFile = new TFile("../../output/PsfStudies.MC.Impact.prod3.root", "READ");
  if(!inputFile || inputFile->IsZombie()) return;
    TTree * tree = dynamic_cast<TTree *>(inputFile->Get("tree"));
    if(!tree) return;

    Double_t ARsq_cut    = 0;
    Double_t trueImp_cut = 0;
    Int_t    Ntel_cut    = 0;
    Double_t r68         = 0;
    
    TFile * File_cut = new TFile("3var.root", "UPDATE");
    TTree * tree_cut = new TTree("tree_cut_LSTRange02", "tree_cut_LSTRange02"); 
    tree_cut->Branch("ARsq_cut", &ARsq_cut); 
    tree_cut->Branch("trueImp_cut", &trueImp_cut);
    tree_cut->Branch("Ntel_cut", &Ntel_cut);
    tree_cut->Branch("r68", &r68);

    gROOT->cd();



     for(Int_t Ncut = 1, Nend=10; Ncut<=Nend; ++Ncut){
	for(Double_t Impcut = 50, ImpMax = pMaxImpact; Impcut<=ImpMax; Impcut += pImpactStep ) {

	  Double_t ARsq = 0;
	  Double_t trueEn = 0;
	  Double_t trueImp = 0;
	  Int_t Ntelshower = 0;
	  //again, pointer to the variable needed
	  tree->SetBranchAddress("ThetaSq", &ARsq);
	  tree->SetBranchAddress("TrueEnergy", &trueEn);
	  tree->SetBranchAddress("TrueImpact", &trueImp);
	  tree->SetBranchAddress("Ntelshower", &Ntelshower);

	  TH1D* hAR = new TH1D("hAR", "hAR; #Theta^2; # events", 10000, 0, 0.5);
	  
	  for(Int_t entry = 0, end = tree->GetEntries(); entry<end; ++entry ) {
	    tree->GetEntry(entry);
	    if( trueEn < En_min)
	      continue; 
	    if(trueEn > En_max)
	      continue;
	    if(Ntelshower<Ncut)
	      continue;
	    if(trueImp>Impcut)
	      continue;
	    
	    hAR->Fill(ARsq);	 
   
	  }
	  // hAR->Print("base");
	  Double_t r68_cal = GetContainmentBinCenter(hAR, 0.68);
	  trueImp_cut = Impcut;
	  Ntel_cut    = Ncut;
	  r68         = r68_cal;
	  tree_cut->Fill();
	 
	  hAR->Clear(); 
	}	 
	std::cout << "Ncut = " << Ncut << std::endl;
     }

     File_cut->Write();
     File_cut->Close();



} // End of main 




