//==========
//STL (standard C library)
#include <iostream>
//#include <ofstream>
//=============
//ROOT
#include <TLatex.h>
#include <TFile.h>
#include <TH1F.h>
#include <TH2F.h>
#include <TH1D.h>
//graphic output:
#include <TCanvas.h>
//nice legend:
#include <TLegend.h>
//colors and stuff:
#include <TStyle.h>
#include "TMath.h"
#include "TROOT.h"
#include "TGraph.h"
#include "TGraphPainter.h"
#include <sstream>
#include "TRandom.h"
//
// This script calculates error bars on r68: just to make plots etc. version 3 will continue this 
//  and apply upper/lower limits, and calculate this for a lot of impcut points or Ntel points e.g.
//
// --------------
// Author: Felicia Krauss - 17 October 2016
// --------------
// Modification history:
// 19 Oct 2016: Cosette Molijn (cmolijn): Add other histograms/containment radius
// 01 Nov 2016: cmolijn                 : Replace GetEntries() with Integral()
// 16 Nov 2016: cmolijn                 : Extend script with 2d histogram, add Ncuts and graphs
// --------------------
// February 2017: cmolijn

Double_t GetContainmentBinCenter(const TH1D* pInput, Double_t pRatio) {
  Double_t limit = pInput->Integral(0, pInput->GetNbinsX() + 1) * pRatio;
  Double_t sum = 0;
  for(Int_t bin = 1, end = pInput->GetNbinsX(); bin<end;++bin ) {
    sum += pInput->GetBinContent(bin);
    if(sum  >= limit)
      return pInput->GetXaxis()->GetBinCenter(bin);
  }
  return pInput->GetXaxis()->GetXmax();
}


Double_t r68_rndm(const TH1D* pInput1, Double_t pRatio ){

  //Here we take a AR TH1D, draw random gaussian sqrt(Ni) as new delta N for each bin
  //and calculate a new r68 

  TH1D* hist1 = new TH1D("hist1","",pInput1->GetNbinsX(), pInput1->GetXaxis()->GetXmin(), pInput1->GetXaxis()->GetXmax());
  Int_t bin = 1;
  while(bin <= pInput1->GetNbinsX() ){

    Double_t Ni = pInput1->GetBinContent(bin);
    Double_t sigma = pow(Ni, 0.5);    
    Double_t delt_Ni = gRandom->Gaus(0.,sigma); 
    if(bin == 5){
      // std::cout << "bin 5: sigma, delt_Ni = " << sigma << "; " << delt_Ni << std::endl;
    }
    hist1->SetBinContent(bin, pInput1->GetBinContent(bin) + delt_Ni); 
    // Adjust this later for sign (upper or lower error/limit)

    ++bin;
  } 
  // Now calculate the r68 for this new AR histogram  
  Double_t radi = GetContainmentBinCenter(hist1, pRatio);
  delete hist1;

  return radi;
}


TH1D* r68_ErrorLoop(const TH1D* pInput1, Double_t pRatio){
  
  //Here we read in the TH1D AR projection, and generate a r68 histogram.
  Int_t num = 10000.;  // how many times do we want to loop?
  TH1D* histr68 = new TH1D("histr68","histr68",2000, 0, 0.05); // r68 histogram: same binning as original AR hist

  Int_t i = 0;
  while(i < num){    
    Double_t r68 = r68_rndm(pInput1, pRatio);
    histr68->Fill(r68);
    ++i;
  }
  Double_t r68_input = GetContainmentBinCenter(pInput1,pRatio);
  std::cout << "true r68 = " << r68_input << std::endl;

  return histr68; // this is the r68 histogram;  later we want to fit a gaussian 
}


TH1F * Mkplot_Imp(const TH2F* pInput, Double_t pRatio, Double_t pMaxImpact, Double_t pImpactStep, 
		     const std::string & pName, const std::string & pTitle) {

  std::cout << "Impact =========== " << std::endl;
  pInput->Print("base");
  
  //Get 1D histogram - Impact parameter loop
  TH1F * hist = new TH1F(pName.c_str(), pTitle.c_str(), pMaxImpact/pImpactStep, 0, pMaxImpact);
  for(Int_t bin = 1, end =  pMaxImpact/pImpactStep; bin<=end; ++bin) {

     const TH1D* projection = pInput->ProjectionX("proc", 0, pInput->GetYaxis()->FindBin(bin*pImpactStep)); 
    
     //   Double_t r68 = 

     // do smtg to get the errorsss

     hist->SetBinContent(bin, GetContainmentBinCenter(projection, pRatio));

     TH1D* dist = r68_ErrorLoop(projection, pRatio, 0);
     dist->SetTitle("r68 distribution fake MC method - minus-sigma - Impact cut = 150 m - 0.02-0.1 TeV; r68 (deg^2); #events");
     dist->Print("base");
     dist->Draw("Hist");

     dist->Fit("gaus");
     TF1 *fitg = (TF1*)dist->GetFunction("gaus");
     fitg->Draw("Same");


     delete projection;

   }// End of impact paramaeter loop

  return hist;
}



void plotImpvsAR_error68_02(Double_t En_min=0.1, Double_t En_max=1, Double_t pMaxImpact=150, Double_t pImpactStep=150){


  gROOT->SetStyle("Plain");

  //Define fonts:
  gStyle->SetLabelFont(22,"X");
  gStyle->SetStatFont(22);
  gStyle->SetTitleFont(22,"xyz");
  gStyle->SetTitleFont(22,"h");
  gStyle->SetLegendFont(2);
  gStyle->SetOptStat(1000000001);

  
  // Declare 2D hist

  TH2F* H_2var_Imp = new TH2F( "2var_Ntel", ";#Theta^2; Max. Impact par", 
			   20000, 0., 0.5, 2000 ,0. , 2000  ) ;   


  TFile * inputFile = new TFile("../../output/PsfStudies.MC.Impact.prod3.root", "READ");
  if(!inputFile || inputFile->IsZombie()) return;
    TTree * tree = dynamic_cast<TTree *>(inputFile->Get("tree"));
    if(!tree) return;
      Double_t ARsq = 0;
      Double_t trueEn = 0;
      Double_t trueImp = 0;
      Int_t Ntelshower = 0;
      //again, pointer to the variable needed
      tree->SetBranchAddress("ThetaSq", &ARsq);
      tree->SetBranchAddress("TrueEnergy", &trueEn);
      tree->SetBranchAddress("TrueImpact", &trueImp);
      tree->SetBranchAddress("Ntelshower", &Ntelshower);

	
      //Loop over the tree with the set cuts on the entries: 
      // ======= Energy =======
      for(Int_t entry = 0, end = tree->GetEntries(); entry<end; ++entry ) {
        tree->GetEntry(entry);
	if( trueEn < En_min)
          continue; //we cut on the energy here
        if(trueEn > En_max)
          continue;


        H_2var_Imp->Fill(ARsq, trueImp);	
      }
      std::cout << " # entries = " << H_2var_Imp->GetEntries() << std::endl;
      Double_t entries_tot = H_2var_Imp->GetEntries();  
      // The total number of entries of the Impact reco method


      


	
  // ============= Construct canvas =============
  // ============================================
  /* TCanvas* canv = new TCanvas("c1","Cut on max. Impact parameter vs Angular Resolution"); */
  /* canv->SetFillColor(0); */
  /* canv->Divide(2,2); */
  /* canv->cd(1); */

  TH1F* hist   = Mkplot_Imp(H_2var_Imp, 0.68, pMaxImpact, pImpactStep,
  			    "","Impact method - Max. Imp. cut vs r68 - SST - 0.3-0.5 TeV ; Max Imp.(m) ; r68 (deg^2)  ");


  /* hist->SetMarkerStyle(20); */
  /* hist->SetMarkerColor(2); */
  /* hist->SetMarkerSize(0.4); */
  /* //  hist->GetXaxis()->SetRangeUser(0,700); */
  /* //  hist->GetYaxis()->SetRangeUser(0.018,0.025); */
  /* hist->GetYaxis()->SetTitleOffset(1.3); */
  /* hist->Draw("PM"); */

 
  
  // canv->SaveAs("plots_Impact/ImpvsAR_SST_0305TeV.pdf");


} // End of main 




