
//==========
//STL (standard C library)
#include <iostream>
//#include <ofstream>
//=============
//ROOT
#include <TLatex.h>
#include <TFile.h>
#include <TH1F.h>
#include <TH2F.h>
#include <TH1D.h>
//graphic output:
#include <TCanvas.h>
//nice legend:
#include <TLegend.h>
//colors and stuff:
#include <TStyle.h>
#include "TMath.h"
#include "TROOT.h"
#include "TGraph.h"
#include "TGraphPainter.h"
#include <sstream>
#include <string>
#include "TRandom.h"
//
// This script calculates error bars on r68: just to make plots etc. version 3 will continue this 
//  and apply upper/lower limits, and calculate this for a lot of impcut points or Ntel points e.g.
//
// --------------
// Author: Felicia Krauss - 17 October 2016C
// --------------
// Modification history:
// 19 Oct 2016: Cosette Molijn (cmolijn): Add other histograms/containment radius
// 01 Nov 2016: cmolijn                 : Replace GetEntries() with Integral()
// 16 Nov 2016: cmolijn                 : Extend script with 2d histogram, add Ncuts and graphs
// --------------------
// February 2017: cmolijn
// Mei 2017: cmolijn --                 : From ARsq histogram, fit function, from function, calculate bin
//                                        content of AR histogram, and fit a AR function





void FitGaus03(Double_t En_min=50, Double_t En_max=300){


  gROOT->SetStyle("Plain");
  gStyle->SetOptStat(1000000001);
  
  // Declare 1D hist
  // =======================
  TH1F* theta_hist = new TH1F( "theta_hist", "PSF Impact method ->50 TeV - Ncut = 10 - Impcut = 700m ;#Theta^2; events", 
			       10000, 0., 0.1) ;   

  TFile * inputFile = new TFile("../../output/PsfStudies.MC.Impact.prod3.root", "READ");
  if(!inputFile || inputFile->IsZombie()) return;
    TTree * tree = dynamic_cast<TTree *>(inputFile->Get("tree"));
    if(!tree) return;
      Double_t ARsq = 0;
      Double_t trueEn = 0;
      Double_t trueImp = 0;
      Int_t Ntelshower = 0;
      //again, pointer to the variable needed
      tree->SetBranchAddress("ThetaSq", &ARsq);
      tree->SetBranchAddress("TrueEnergy", &trueEn);
      tree->SetBranchAddress("TrueImpact", &trueImp);
      tree->SetBranchAddress("Ntelshower", &Ntelshower);
	
      //Loop over the tree with the set cuts on the entries: 
      // ======= Energy =======
      for(Int_t entry = 0, end = tree->GetEntries(); entry<end; ++entry ) {
        tree->GetEntry(entry);
	if( trueEn < En_min)
          continue; //we cut on the energy here
        if(trueEn > En_max)
          continue;
	if(trueImp > 700)
	  continue;
	if(Ntelshower < 10)
	  continue;
	
	Double_t AR = pow(ARsq, 0.5);
        theta_hist->Fill(ARsq);	
      }

      inputFile->Close();
      
      std::cout << " # entries = " << theta_hist->GetEntries() << std::endl;
      Double_t entries_tot = theta_hist->GetEntries();  
    

      //=========================================
      //Now, let's fit a function to the data
      //=========================================
      TF1* f1  = new TF1("f1","[0]*exp(-x/[1])", 0,0.003);
      //   TF1* f2  = new TF1("f2","[0]*exp(-x/[1])", 0.00015,0.0006);
      //  TF1* f3  = new TF1("f3","[0]*exp(-x/[1])", 0.0006,0.001);

      TF1* total = new TF1("mstotal","[0]*exp(-x/[1])",0,0.003);
      // TF1* total = new TF1("mstotal","[0]*exp(-x/[1]) + [2]*exp(-x/[3]) + [4]*exp(-x/[5]) ",0,0.002);

      f1->SetParameters(100,0.05);
      //    f2->SetParameters(40,0.5);
      //    f3->SetParameters(20,1);

      theta_hist->Fit(f1, "R");
      //   theta_hist->Fit(f2, "R+");
      //   theta_hist->Fit(f3, "R+");

      Double_t par[6];

      f1->GetParameters(&par[0]);
      //    f2->GetParameters(&par[2]);
      //    f3->GetParameters(&par[4]);

      total->SetParameters(par);
      theta_hist->Fit(total, "R+");


  // ============= Construct canvas =============
  // ============================================
   TCanvas* canv = new TCanvas("c1","Impact Method - PSF");
   //  canv->SetFillColor(0);
   canv->Divide(1,2);
   canv->cd(1);
 
  theta_hist->SetLineWidth(3);
  theta_hist->GetXaxis()->SetRangeUser(0,0.004);
  theta_hist->GetYaxis()->SetTitleOffset(1.3);
  theta_hist->Draw("Hist");

  f1->SetLineColor(2);
  //  f2->SetLineColor(4);
  //  f3->SetLineColor(8);
  total->SetLineColor(5);

  f1->Draw("Same");
  // f2->Draw("Same");
  // f3->Draw("Same");
  total->Draw("Same");

  std::cout << "========= Now over to theta fit =========" << std::endl;

  //=========================================
  // Now, we make a new histogram from this fitted function, total, and make a new histogram, events vs degrees
  //=========================================

  TH1F* hist2 = new TH1F("hist2", "Events vs degrees ->50 TeV - Ncut = 10 -Impcut = 700m ; #theta; events", 20000,0, 0.2);
  for(Int_t bin = 1, end = hist2->GetNbinsX(); bin<= end; bin++){
    
    Double_t ev = total->Eval( pow(hist2->GetXaxis()->GetBinCenter(bin),2) );
    // Double_t ev = pow(evSq,0.5);
    hist2->SetBinContent(bin,ev);
  }
  canv->cd(2);
  hist2->Draw("Hist");
  
   //=========================================
   // And again fit a function to this new histogram
   //=========================================
   TF1* f11  = new TF1("f11","[0]*exp(-pow(x,2)/[1])", 0,0.05);
   TF1* f22  = new TF1("f22","[0]*exp(-pow(x-[1],2)/[2])", 0.01,0.02);
   //  TF1* f33  = new TF1("f33","[0]*exp(-pow(x-[1],2)/[2])", 0.15,0.3);

   TF1* total2 = new TF1("mstotal2","[0]*exp(-pow(x,2)/[1])"
  			 ,0,0.05);

   f11->SetParameters(50,0.005);
   // f22->SetParameters(10,0.01,0.02);
   //  f33->SetParameters(5,0.13,0.5);

   hist2->Fit(f11, "R M");
   // hist2->Fit(f22, "R+ M");
   //  hist2->Fit(f33, "R+ M");

   Double_t par2[5];

   f11->GetParameters(&par2[0]);
   f22->GetParameters(&par2[2]);
   //  f33->GetParameters(&par2[5]);

   total2->SetParameters(par2);
   hist2->Fit(total2, "R+M");

   // ======= Draw it!

  hist2->SetLineWidth(3);
  hist2->GetXaxis()->SetRangeUser(0,0.05);
  // hist2->GetYaxis()->SetRangeUser(0,50);
  hist2->GetYaxis()->SetTitleOffset(1.3);
  hist2->Draw("Hist");

  f11->SetLineColor(2);
  f22->SetLineColor(4);
  // f33->SetLineColor(8);
  total2->SetLineColor(5);

  canv->cd(2);
  f11->Draw("Same");
  // f22->Draw("Same");
  //  f33->Draw("Same");
  // total2->Draw("Same");




} // End of main 




