//==========
//STL (standard C library)
#include <iostream>
//#include <ofstream>
//=============
//ROOT
#include <TLatex.h>
#include <TFile.h>
#include <TH1F.h>
#include <TH2F.h>
#include <TH1D.h>
//graphic output:
#include <TCanvas.h>
//nice legend:
#include <TLegend.h>
//colors and stuff:
#include <TStyle.h>
#include "TMath.h"
#include "TROOT.h"
#include "TGraph.h"
#include "TGraphPainter.h"
#include <sstream>
#include <fstream>
//
// This script visualises relation between a minimum cut in number of telescopes,
// and the loss - as a consequence of this cut - of events within each energy band (i.e. NOT 
// relative to total number of events over entire energy range)
//
// --------------
// Author: Felicia Krauss - 17 October 2016
// --------------
// Modification history:
// 19 Oct 2016: Cosette Molijn (cmolijn): Add other histograms/containment radius
// 01 Nov 2016: cmolijn                 : Replace GetEntries() with Integral()
// 16 Nov 2016: cmolijn                 : Extend script with 2d histogram, add Ncuts and graphs
// ......................
// February 2017 cmolijn -
// July 2017 cmolijn - this script applies the cuts, and writes the following data to a .txt file:
// - Ebins (up and low)
// - r68 (deg)
// - r68 error (deg)
// - initial # events
// - remaining # events after cuts
// - event loss (%)



//===========================
// I. All  functions to calculate r68 and its errors:
//===========================

Double_t GetContainmentBinCenter(const TH1D* pInput, Double_t pRatio) {
  // Calculate the r68 (or other radius) of a th1 AR histogram

  Double_t limit = pInput->Integral(0, pInput->GetNbinsX() + 1) * pRatio;
  Double_t sum = 0;
  for(Int_t bin = 1, end = pInput->GetNbinsX(); bin<end;++bin ) {
    sum += pInput->GetBinContent(bin);
    if(sum  >= limit)
      return pInput->GetXaxis()->GetBinCenter(bin);
  }
  return pInput->GetXaxis()->GetXmax();
}


Double_t rad_rndm(const TH1D* pInput1, Double_t pRatio ){

  //Here we take a AR TH1D, draw random gaussian sqrt(Ni) as new delta N for each bin
  //and calculate a new r68 

  TH1D* hist1 = new TH1D("hist1","",pInput1->GetNbinsX(), pInput1->GetXaxis()->GetXmin(), pInput1->GetXaxis()->GetXmax());
  Int_t bin = 1;
  while(bin <= pInput1->GetNbinsX() ){

    //first error propagation: dn/da

        
    //  Double_t rLow = hist1->GetXaxis()->GetBinLowEdge(bin);
    //   Double_t rUp  = rLow + hist1->GetXaxis()->GetBinWidth(bin);
    //   Double_t pi   = 3.1415926;
    //   Double_t area_deg2 = pi*( pow(rUp,2) - pow(rLow,2) ) ;

    Double_t Ni = pInput1->GetBinContent(bin);
    Double_t sigma = pow(Ni, 0.5);
      ///pow(area_deg2,0.5);    
    Double_t delt_Ni = gRandom->Gaus(0.,sigma); 
   
    hist1->SetBinContent(bin, pInput1->GetBinContent(bin) + delt_Ni); 
    
    ++bin;
  } 
  // std::cout << "test 2" << std::endl;
  // Now calculate the r68 for this new AR histogram  
  Double_t radi = GetContainmentBinCenter(hist1, pRatio);
  // std::cout << "new r68 = " << radi << std::endl;

  delete hist1;
  
  return radi;
}



Double_t GetSigmaRad(TH1D* phist_rad){
  // Calculate the sigma of a gaussian histogram

  Double_t bin_width = phist_rad->GetBinWidth(3);
  Double_t low_lim = phist_rad->GetBinCenter(phist_rad->FindFirstBinAbove(0,1));
  Double_t up_lim = phist_rad->GetBinCenter(phist_rad->FindLastBinAbove(0,1));
  
  Int_t nbins = (up_lim - low_lim)/bin_width;
  std::cout << "prints = " << up_lim << low_lim << nbins << phist_rad->GetEntries()<< std::endl;
  Int_t low_bin = phist_rad->FindFirstBinAbove(0,1);
  Int_t up_bin = phist_rad->FindLastBinAbove(0,1);


  TH1F* fithist = new TH1F("fithist", "fithist; r68; events", nbins, low_lim, up_lim);

  Int_t bin_fit = 1;
  for(Int_t bin = low_bin , last=up_bin; bin<=last;  ++bin){
    fithist->SetBinContent(bin_fit,phist_rad->GetBinContent(bin));
    ++bin_fit;

  }


  fithist->Fit("gaus");
  TF1 *fitg = (TF1*)fithist->GetFunction("gaus");
  Double_t sig = fitg->GetParameter(2);

  delete fithist;
  // delete fitg;

  return sig;
}


Double_t r68_ErrorLoop(const TH1D* pInput1, Double_t pRatio){
  //Here we read in the TH1D AR projection, and generate a r68 histogram.
 
  Int_t num = 10000.;  // how many times do we want to loop?
  TH1D* histrad = new TH1D("histrad","histrad",800, 0, 0.2); // r68 histogram: same binning as original AR hist?

  Int_t i = 0;
  while(i < num){    
    Double_t rad = rad_rndm(pInput1, pRatio);
    //   std::cout << "rad = " << rad << std::endl;
    histrad->Fill(rad); // this is the r68 histogram;
    ++i;
  }
  
  histrad->Draw("hist");
  // Now we want to find the uncertainty on the r68
  std::cout << "entries histrad = " << histrad->GetEntries() << std::endl; 
  Double_t sigma = GetSigmaRad(histrad);
 std::cout << "here? sigma = "<< sigma << std::endl;
  delete histrad;

  return sigma; 
}

//===========================
// II. Function to convert a histrogram of events to events/deg^2
//===========================

TH1D * todeg2(const TH1D* project){

    // The projection is a histogram with theta vs events; the new histogram is theta vs events/deg^2
    TH1D* AR_hist = new TH1D("AR_hist", "AR_hist; theta; num/arcmin^2", project->GetXaxis()->GetNbins(), project->GetXaxis()->GetXmin(), project->GetXaxis()->GetXmax()); 

    //Let's convert to events/deg^2 (the 'real' PSF)
    for(Int_t bin = 1, end = project->GetXaxis()->GetNbins(); bin<=end; ++bin){
        
        Double_t rLow = project->GetXaxis()->GetBinLowEdge(bin);
        Double_t rUp  = rLow + project->GetXaxis()->GetBinWidth(bin);
        Double_t pi   = 3.1415926;
        Double_t ev   = project->GetBinContent(bin);

        Double_t Ev_deg2 = ev /( pi*( pow(rUp,2) - pow(rLow,2) ) );
	Double_t Ev_arcmin2 = Ev_deg2/3600; // convert to ev/arcmin^2
        AR_hist->SetBinContent(bin, Ev_deg2);
       
     }
    return AR_hist;
}



//===========================
// III. Function to export histogram to ASCII File to plot with other programs
//===========================

void TH1F2ascii(TH1F* hist,std::string filename, TString folder, TString separator)
{
  /**
   * \brief Export Single Histogram into ASCII file
   */
  Int_t i,j;
  Double_t xcenter, xwidth;
  Bool_t success=kFALSE;
  filename = folder + hist->GetName() + ".txt";
  ofstream file_out(filename);
  file_out << "# Output " << hist->ClassName() << ": " << hist->GetName() << " (" <<
 hist->GetTitle() << ")\n";
  

  
  if (hist->GetDimension()==1)
    {
      file_out << "# BinCenter" << separator << "Content" << separator << "BinHalfWidth" << separator << "Error\n";
      for (i=1; i<=hist->GetNbinsX(); i++)
        file_out << hist->GetBinCenter(i) << separator << hist->GetBinContent(i) << separator << hist->GetBinWidth(i)/2 << separator << hist->GetBinError(i) << endl;
      if (i>1)
        success=kTRUE;
    }
    file_out.close();
    if (success == kTRUE)
      cout << "*** Histogram " << hist->GetName() << " written to " << filename << endl;
    //return success;

  
}
  
//===========================
// MAIN PROGRAM
//===========================


void apply_cuts( Int_t Ncut = 5, Double_t pMaxImpact=800,
		Double_t pRatio = 0.68){ 

  // start the loop over the energy bins here
  // First: open the file we want to write the data to:

  ofstream f ;
  f.open("file.txt");

  // define the energy bins:

  Double_t Energy[20] = { 0.0501,    0.0748,    0.1115,    0.1664,    0.2482,    0.3702,
          0.5522,    0.8237,    1.2288,    1.833 ,    2.7343,    4.0787,
          6.0843,    9.076 ,   13.5388,   20.1959,   30.1264,   44.9398,
         67.0372,  100. };


  // start the energy loop:
  for(Int_t i = 0; i < 19; ++i){

    Double_t En_min = Energy[i];
    Double_t En_max = Energy[i+1];

  // Declare 2D hist
  TH1D* H_AR_En = new TH1D( "H_AR_En", "theta (deg); # events", 1000 , 0, 0.5   ) ;
  TH1D* H_AR_all = new TH1D( "H_AR_all", "theta (deg); # events", 1000 , 0, 0.5   ) ;


  TFile * inputFile = new TFile("../../output/PsfStudies.MC.Impact.prod3.root", "READ");
  if(!inputFile || inputFile->IsZombie()) return;
    TTree * tree = dynamic_cast<TTree *>(inputFile->Get("tree"));
    if(!tree) return;
      Double_t trueEn = 0;
      Double_t trueImp = 0;
      Int_t Ntelshower = 0;
      Double_t AR = 0;
      //again, pointer to the variable needed
      tree->SetBranchAddress("TrueEnergy", &trueEn);
      tree->SetBranchAddress("TrueImpact", &trueImp);
      tree->SetBranchAddress("Ntelshower", &Ntelshower);
      tree->SetBranchAddress("Theta",&AR);

 
      //Loop over the tree with the set cuts on the entries: 
      // ======= Energy + Ntel =======
      for(Int_t entry = 0, end = tree->GetEntries(); entry<end; ++entry ) {
        tree->GetEntry(entry);
	if( trueEn < En_min)
          continue;
        if(trueEn > En_max)
          continue;
	H_AR_En->Fill(AR); //we only cut on the energy here

	if(Ntelshower < Ncut )
	  continue;
        if(trueImp > pMaxImpact)
	  continue;

        H_AR_all->Fill(AR);	  
      }
      // So now we have two AR histograms. 
      // Let's get the data already we do not have to calculate:
      Double_t tot_entries = H_AR_En->GetEntries();
      Double_t cut_entries = H_AR_all->GetEntries(); 

      Double_t loss = (1 - (cut_entries/tot_entries)) * 100;
      // get errors of loss:
      Double_t frac = cut_entries/tot_entries;
      Double_t en_err = pow(cut_entries,0.5);
      Double_t tot_err = pow(tot_entries,0.5);
      Double_t fr1 = en_err/cut_entries;
      Double_t fr2 = tot_err/tot_entries;
      Double_t sum = pow(fr1,2) + pow(fr2,2);
      Double_t frac_err = loss*pow(sum,0.5);
     

      std::cout << "entries total = " << tot_entries <<  std::endl;
      std::cout << "entries after cut = " << cut_entries << std::endl;   
      std::cout << "loss = " << loss << "%" << std::endl; 

      // So now we want to calculate the r68 of this AR histogram
      // TH1D* AR_hist = todeg2(H_AR_all);
      Double_t r68 = GetContainmentBinCenter(H_AR_all, pRatio);
      Double_t uncert = r68_ErrorLoop(H_AR_all, pRatio);

      std::cout << En_min << " " << En_max <<  " "  << r68 <<  " "  << uncert <<  " "  << tot_entries <<   " "  << cut_entries<<  " "  << loss <<  " "  << Ncut <<  " "  << pMaxImpact << std::endl;


      // Now we write the data to a file

      f << En_min << " " << En_max <<  " "  << r68 <<  " "  << uncert <<  " "  << tot_entries <<   " "  << cut_entries<<  " "  << loss <<  " "  << Ncut <<  " "  << pMaxImpact << std::endl;

  }
      f.close();
      
 
} // End of main 




