//==========
//STL (standard C library)
#include <iostream>
//#include <ofstream>
//=============
//ROOT
#include <TLatex.h>
#include <TFile.h>
#include <TH1F.h>
#include <TH2F.h>
#include <TH1D.h>
//graphic output:
#include <TCanvas.h>
//nice legend:
#include <TLegend.h>
//colors and stuff:
#include <TStyle.h>
#include "TMath.h"
#include "TROOT.h"
#include "TGraph.h"
#include "TGraphPainter.h"
#include <sstream>
#include <string>
#include "TRandom.h"
//
// This script calculates r68 and corresponding event loss veresus impact parameter, for a given Ntel cut. 
//
// --------------
// Author: Felicia Krauss - 17 October 2016
// --------------
// Modification history:
// 19 Oct 2016: Cosette Molijn (cmolijn): Add other histograms/containment radius
// 01 Nov 2016: cmolijn                 : Replace GetEntries() with Integral()
// 16 Nov 2016: cmolijn                 : Extend script with 2d histogram, add Ncuts and graphs
// --------------------
// February 2017: cmolijn
// June 2017: cmolijn:


Double_t GetContainmentBinCenter(const TH1D* pInput, Double_t pRatio) {
  // Calculate the r68 (or other radius) of a th1 AR histogram

  Double_t limit = pInput->Integral(0, pInput->GetNbinsX() + 1) * pRatio;
  Double_t sum = 0;
  for(Int_t bin = 1, end = pInput->GetNbinsX(); bin<end;++bin ) {
    sum += pInput->GetBinContent(bin);
    if(sum  >= limit)
      return pInput->GetXaxis()->GetBinCenter(bin);
  }
  return pInput->GetXaxis()->GetXmax();
}


Double_t rad_rndm(const TH1D* pInput1, Double_t pRatio ){

  //Here we take a AR TH1D, draw random gaussian, sqrt(Ni) as new delta N for each bin
  //and calculate a new r68 

  TH1D* hist1 = new TH1D("hist1","",pInput1->GetNbinsX(), pInput1->GetXaxis()->GetXmin(), pInput1->GetXaxis()->GetXmax());
  Int_t bin = 1;
  while(bin <= pInput1->GetNbinsX() ){

    //first error propagation: dn/da

        
    Double_t rLow = hist1->GetXaxis()->GetBinLowEdge(bin);
    Double_t rUp  = rLow + hist1->GetXaxis()->GetBinWidth(bin);
    Double_t pi   = 3.1415926;
    Double_t area_deg2 = pi*( pow(rUp,2) - pow(rLow,2) ) ;

    Double_t Ni = pInput1->GetBinContent(bin);
    Double_t sigma = pow(Ni, 0.5)/pow(area_deg2,0.5);    
    Double_t delt_Ni = gRandom->Gaus(0.,sigma); 

    hist1->SetBinContent(bin, pInput1->GetBinContent(bin) + delt_Ni); 
    
    ++bin;
  } 

  // Now calculate the r68 for this new AR histogram  
  Double_t radi = GetContainmentBinCenter(hist1, pRatio);
  std::cout << "new r68 = " << radi << std::endl;

  delete hist1;
  
  return radi;
}


TF1* GetSigmaRad(TH1D* phist_rad){
  // Calculate the sigma of a gaussian histogram

  Double_t bin_width = phist_rad->GetBinWidth(3);
  Double_t low_lim = phist_rad->GetBinCenter(phist_rad->FindFirstBinAbove(0,1));
  Double_t up_lim = phist_rad->GetBinCenter(phist_rad->FindLastBinAbove(0,1));
  
  Int_t nbins = (up_lim - low_lim)/bin_width;

  Int_t low_bin = phist_rad->FindFirstBinAbove(0,1);
  Int_t up_bin = phist_rad->FindLastBinAbove(0,1);

  std::cout<< "nbins = " << nbins << std::endl;

 TH1F* fithist = new TH1F("fithist", "fithist; r68; events", nbins, low_lim, up_lim);

  Int_t bin_fit = 1;
  for(Int_t bin = low_bin , last=up_bin; bin<=last;  ++bin){
    fithist->SetBinContent(bin_fit,phist_rad->GetBinContent(bin));
    ++bin_fit;

  }

  std::cout << "lower and upper limit for the r68 fit = " << low_lim << "; "<< up_lim << std::endl;

  fithist->Fit("gaus");
  TF1 *fitg = (TF1*)fithist->GetFunction("gaus");

  return fitg;
}


TH1D* histrad(const TH1D* pInput1, Double_t pRatio){
  //Here we read in the TH1D AR projection, and generate a r68 histogram.
 
  Int_t num = 10000.;  // how many times do we want to loop?
  TH1D* histrad = new TH1D("histr68","histr68",400, 0, 0.1); // r68 histogram: same binning as original AR hist?

  Int_t i = 0;
  while(i < num){    
    Double_t rad = rad_rndm(pInput1, pRatio);
    histrad->Fill(rad); // this is the r68 histogram;
    ++i;
  }

  //TF1* fit = GetSigmaRad(histrad);
  return histrad; 

}


TH1D * todeg2(const TH1D* project){

    // The projection is a histogram with theta vs events; the new histogram is theta vs events/deg^2
    TH1D* AR_hist = new TH1D("AR_hist", "AR_hist; theta; num/arcmin^2", project->GetXaxis()->GetNbins(), project->GetXaxis()->GetXmin(), project->GetXaxis()->GetXmax()); 

    //Let's convert to events/deg^2 (the 'real' PSF)
    for(Int_t bin = 1, end = project->GetXaxis()->GetNbins(); bin<=end; ++bin){
        
        Double_t rLow = project->GetXaxis()->GetBinLowEdge(bin);
        Double_t rUp  = rLow + project->GetXaxis()->GetBinWidth(bin);
        Double_t pi   = 3.1415926;
        Double_t ev   = project->GetBinContent(bin);

        Double_t Ev_deg2 = ev /( pi*( pow(rUp,2) - pow(rLow,2) ) );
	Double_t Ev_arcmin2 = Ev_deg2/3600; // convert to ev/arcmin^2
        AR_hist->SetBinContent(bin, Ev_deg2);
       
     }
    return AR_hist;
}




//===========


void test10july(Double_t En_min=0.300, Double_t En_max=0.560, Double_t pMaxImpact=2000, Double_t pImpactStep=50){
  
  // Declare 2D hist
  // =======================
  TH2F* H_2var_Imp = new TH2F( "2var_Ntel", ";#Theta; Max. Impact par", 
			   1000, 0., 0.25, 2000 ,0. , 2000  ) ;   

  TH2F* H_2var_count = new TH2F( "2var_count", ";#Theta; Max. Impact par", 
			   1000, 0., 0.25, 2000 ,0. , 2000  ) ; 

  TFile * inputFile = new TFile("../../output/PsfStudies.MC.Impact.prod3.root", "READ");
  if(!inputFile || inputFile->IsZombie()) return;
    TTree * tree = dynamic_cast<TTree *>(inputFile->Get("tree"));
    if(!tree) return;
      Double_t ARsq = 0;
      Double_t trueEn = 0;
      Double_t trueImp = 0;
      Int_t Ntelshower = 0;
      Double_t AR = 0;
      //again, pointer to the variable needed
      tree->SetBranchAddress("ThetaSq", &ARsq);
      tree->SetBranchAddress("TrueEnergy", &trueEn);
      tree->SetBranchAddress("TrueImpact", &trueImp);
      tree->SetBranchAddress("Ntelshower", &Ntelshower);
      tree->SetBranchAddress("Theta",&AR);
	
      //Loop over the tree with the set cuts on the entries: 
      // ======= Energy =======
      for(Int_t entry = 0, end = tree->GetEntries(); entry<end; ++entry ) {
        tree->GetEntry(entry);
	if( trueEn < En_min)
          continue; //we cut on the energy here
        if(trueEn > En_max)
          continue;

	// here count all the entries within this energy band - no cuts
	H_2var_count->Fill(AR,trueImp);
	
	if(Ntelshower < 2)
	  continue;

        H_2var_Imp->Fill(AR, trueImp);	
      }
      
      // The total number of entries of the Impact reco method
      TH1D* sliced1 = H_2var_count->ProjectionX("2dsliced", 0, H_2var_count->GetYaxis()->GetNbins());


      TH1D* sliced_deg21 = todeg2(sliced1);    
      
      Double_t r68 = GetContainmentBinCenter(sliced_deg21, 0.68);
      std::cout << "r68 = " << r68 << std::endl;

      TH1D* hist_radials = histrad(sliced_deg21, 0.68);


      TF1* fit = GetSigmaRad(hist_radials);

      hist_radials->Draw("hist");
      fit->SetLineColor(4);
      fit->Draw("same");
      
      std::cout << "sigma of r68 histogram = " << fit->GetParameter(2) << std::endl;
  
      //      Double_t tot_entries = sliced_deg21->Integral();

      //     std::cout << "entries 2d hist Ncut = " << H_2var_Imp->GetEntries() << std::endl;
      //      std::cout << "entries 2d hist no cut = " << H_2var_count->GetEntries() << std::endl;
      //      std::cout << "entries 2d hist no cut (ev/deg^2) = " << tot_entries << std::endl;
      
      // sliced1->Draw("hist");



} // End of main 




