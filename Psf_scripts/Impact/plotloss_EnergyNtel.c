//==========
//STL (standard C library)
#include <iostream>
//#include <ofstream>
//=============
//ROOT
#include <TLatex.h>
#include <TFile.h>
#include <TH1F.h>
#include <TH2F.h>
#include <TH1D.h>
//graphic output:
#include <TCanvas.h>
//nice legend:
#include <TLegend.h>
//colors and stuff:
#include <TStyle.h>
#include "TMath.h"
#include "TROOT.h"
#include "TGraph.h"
#include "TGraphPainter.h"
#include <sstream>
//
// This script visualises relation between a minimum cut in number of telescopes,
// and the loss - as a consequence of this cut - of events within each energy band (i.e. NOT 
// relative to total number of events over entire energy range)
//
// --------------
// Author: Felicia Krauss - 17 October 2016
// --------------
// Modification history:
// 19 Oct 2016: Cosette Molijn (cmolijn): Add other histograms/containment radius
// 01 Nov 2016: cmolijn                 : Replace GetEntries() with Integral()
// 16 Nov 2016: cmolijn                 : Extend script with 2d histogram, add Ncuts and graphs
// ......................
// February 2017 cmolijn - 

Double_t GetContainmentBinCenter(const TH1D* pInput, Double_t pRatio) {
  Double_t limit = pInput->Integral(0, pInput->GetNbinsX() + 1) * pRatio;
  Double_t sum = 0;
  for(Int_t bin = 0, end = pInput->GetNbinsX(); bin<end;++bin ) {
    sum += pInput->GetBinContent(bin);
    if(sum  >= limit)
      return pInput->GetXaxis()->GetBinCenter(bin);
  }
  return pInput->GetXaxis()->GetXmax();
}


TH1F * Mkplot(const TH2F* pInput, Double_t pRatio, Double_t pMaxImpact, Double_t pImpactStep, const std::string & pName, const std::string & pTitle) {

  std::cout << "Impact =========== " << std::endl;
  pInput->Print("base");
  
  //Get 1D histogram - Impact parameter loop
  TH1F * hist = new TH1F(pName.c_str(), pTitle.c_str(), pMaxImpact/pImpactStep, 0, pMaxImpact);
  for(Int_t bin = 1, end =  pMaxImpact/pImpactStep; bin<=end; ++bin) {

     const TH1D* projection = pInput->ProjectionX("proc", 0, pInput->GetYaxis()->FindBin(bin*pImpactStep)); 
     hist->SetBinContent(bin, GetContainmentBinCenter(projection, pRatio));

     delete projection;

   }// End of impact paramaeter loop
  // hist->Print("base");
  return hist;
}


TH1F * Mkplot_loss(const TH2F* pInput, Double_t pRatio, Double_t pMaxImpact, Double_t pImpactStep, const std::string & pName, const std::string & pTitle) {

  //Get 1D histogram - Impact parameter loop
  TH1F * hist = new TH1F(pName.c_str(), pTitle.c_str(), pMaxImpact/pImpactStep, 0, pMaxImpact);
  for(Int_t bin = 1, end =  pMaxImpact/pImpactStep; bin<=end; ++bin) {

     const TH1D* projection = pInput->ProjectionX("proc", 0, pInput->GetYaxis()->FindBin(bin*pImpactStep)); 
     Double_t entries = projection->GetEntries();
     Double_t loss = (1 - (entries/tot_entries)) * 100; 
     hist->SetBinContent(bin,loss );

     delete projection;

   }// End of impact paramaeter loop
  return hist;
}

//===========================
//Function to export histogram to ASCII File to plot with other programs


void TH1F2ascii(TH1F* hist,std::string filename, TString folder, TString separator)
{
  /**
   * \brief Export Single Histogram into ASCII file
   */
  Int_t i,j;
  Double_t xcenter, xwidth;
  Bool_t success=kFALSE;
  filename = folder + hist->GetName() + ".txt";
  ofstream file_out(filename);
  file_out << "# Output " << hist->ClassName() << ": " << hist->GetName() << " (" << hist->GetTitle() << ")\n";
  

  
  if (hist->GetDimension()==1)
    {
      file_out << "# BinCenter" << separator << "Content" << separator << "BinHalfWidth" << separator << "Error\n";
      for (i=1; i<=hist->GetNbinsX(); i++)
        file_out << hist->GetBinCenter(i) << separator << hist->GetBinContent(i) << separator << hist->GetBinWidth(i)/2 << separator << hist->GetBinError(i) << endl;
      if (i>1)
        success=kTRUE;
    }
    file_out.close();
    if (success == kTRUE)
      cout << "*** Histogram " << hist->GetName() << " written to " << filename << endl;
    //return success;

  
}




void plotloss_EnergyNtel(Double_t En_min=0., Double_t En_max=300.){


  gROOT->SetStyle("Plain");

  //Define fonts:
  gStyle->SetLabelFont(22,"X");
  gStyle->SetStatFont(22);
  gStyle->SetTitleFont(22,"xyz");
  gStyle->SetTitleFont(22,"h");
  gStyle->SetLegendFont(2);
  gStyle->SetOptStat(1000000001);
  
  //==========
  // first make an array for the energy bins
  Double_t En_bins [25] = {  5.01187234e-02, 7.07945784e-02, 1.00000000e-01,1.41253754e-01, 1.99526231e-01, 2.81838293e-01, 3.98107171e-01, 5.62341325e-01, 7.94328235e-01, 1.12201845e+00, 1.58489319e+00, 2.23872114e+00, 3.16227766e+00, 4.46683592e+00, 6.30957344e+00, 8.91250938e+00, 1.25892541e+01, 1.77827941e+01, 2.51188643e+01, 3.54813389e+01, 5.01187234e+01, 7.07945784e+01, 1.00000000e+02, 1.41253754e+02, 1.99526231e+02};
 
  //==========

  // Declare 2D hist
  TH1F* H_En = new TH1F( "H_En", "; Energy",  24, En_bins  ) ;
  TH1F* H_En_cut = new TH1F( "H_En_cut", "; Energy",  24, En_bins  ) ;   

  TFile * inputFile = new TFile("../../output/PsfStudies.MC.Impact.prod3.root", "READ");
  if(!inputFile || inputFile->IsZombie()) return;
    TTree * tree = dynamic_cast<TTree *>(inputFile->Get("tree"));
    if(!tree) return;
      Double_t ARsq = 0;
      Double_t trueEn = 0;
      Double_t trueImp = 0;
      Int_t Ntelshower = 0;
      //again, pointer to the variable needed
      tree->SetBranchAddress("ThetaSq", &ARsq);
      tree->SetBranchAddress("TrueEnergy", &trueEn);
      tree->SetBranchAddress("TrueImpact", &trueImp);
      tree->SetBranchAddress("Ntelshower", &Ntelshower);

 
      //Loop over the tree with the set cuts on the entries: 
      // ======= Energy + Ntel =======
      for(Int_t entry = 0, end = tree->GetEntries(); entry<end; ++entry ) {
        tree->GetEntry(entry);
        // Here we fill the H_En: no cuts on telescopes
        H_En->Fill(trueEn);	  


        if( trueEn < En_min)
          continue; //we cut on the energy here
	if(trueEn > En_max)
	  continue;
	if(Ntelshower < 15)
	  continue;

        H_En_cut->Fill(trueEn);	
      }
      TH1F* H_En_loss = new TH1F( "Energy_loss_Ncut15", "Impact method - Event loss vs energy, Ntel cut = 15; Energy (TeV); Event loss (%)", 24, En_bins  ) ;         
      for(Int_t i=1, end = 24; i<=end; ++i ){
	Double_t n1   = H_En->GetBinContent(i);
	Double_t n2   = H_En_cut->GetBinContent(i);
	std::cout << "n1, n2 = " << n1 << "; " << n2 << std::endl;
	std::cout << "Energy = " << H_En->GetBinCenter(i) << std::endl;
	if(n1>0){	  
	  Double_t loss = (1-n2/n1) * 100;
	  H_En_loss->SetBinContent(i,loss);
	}else if(n1 == 0 ){
	  Double_t loss = 0.0;
	  H_En_loss->SetBinContent(i,loss);
	}else {
	  Double_t loss = 100.0;
	  H_En_loss->SetBinContent(i,loss);
	}
      }     
      H_En_loss->Print("base");

  // ============= Construct canvas =============
  // ============================================
  TCanvas* canv = new TCanvas("c1","Energy vs event loss (%)");
  canv->SetFillColor(0);
  
  H_En_loss->SetMarkerColor(2);
  H_En_loss->SetMarkerStyle(20);
  H_En_loss->SetMarkerColor(2);
  canv->SetLogx();
  H_En_loss->Draw("PM");
  
  canv->SaveAs("data_EnergyLoss/Loss_Energy_Ncut15.pdf");
  TH1F2ascii(H_En_loss,"H_En_loss","data_EnergyLoss/","\t");


} // End of main 




